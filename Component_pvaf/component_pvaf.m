function [pvaf, goodComps]=component_pvaf(ALLEEG, range,chanind, min_pvaf)
%% this function calculates the percent of variance accounted for (pvaf)
%  of every component present in the ALLEEG structure. 
%  Example for N170:
%  [pvaf_all, goodComps_all]=component_pvaf(ALLEEG, [150 190], [23 17], 5);
%% Initial Variables
global ALLEEG
pvaf = NaN(length(ALLEEG), ALLEEG(1).nbchan);
goodComps=NaN(length(ALLEEG), ALLEEG(1).nbchan);
% take all channels if no channels are specified
if isempty(chanind)
    chanind=1:ALLEEG(1).nbchan;
end
%% loop through subjects in ALLEEG
for k = 1:length(ALLEEG)
    EEG_tmp = pop_loadset('filepath', ALLEEG(k).filepath, 'filename', ALLEEG(k).filename);

    % Settings
    % time range in ms
    %range = [200 800];
    % converted into pnts
    [~,minpnt]=min(abs(EEG_tmp.times-range(1)));
    [~,maxpnt]=min(abs(EEG_tmp.times-range(2)));
    pntrange = minpnt:maxpnt;
    % min pvaf (%) for component of interest
    %min_pvaf=5;


    comp = EEG_tmp.icaact(:,pntrange,:);
    winv = EEG_tmp.icawinv(chanind,:);

    % Calculate pvaf for each component
    for i = 1:length(EEG_tmp.icawinv(1,:))
        [~, pvaf(k,i)] = compvar(EEG_tmp.data(chanind,pntrange,:), comp, winv, [i]);
    end
    
    ncomps=length(find(pvaf(k,:)>=min_pvaf));
    pvaf_tmp=pvaf;
    pvaf_tmp(isnan(pvaf_tmp))=-Inf;
    [sorted_pvaf, inds]=sort(pvaf_tmp(k,:),'descend');
    goodComps(k,1:ncomps) = inds(1:ncomps);

     
end

% plot the first N components of each subject that account for most
% variance
N=3;
figure;
for i = 1:length(ALLEEG)
    cnt=1;
    for k = 1:21:21*N
        h=subplot(N,length(ALLEEG),k+(i-1));
        try
            topoplot(ALLEEG(i).icawinv(:,goodComps(i,cnt)), ALLEEG(i).chanlocs)
        catch
            delete(h)
        end
        if k ==1
            title(sprintf('%s',ALLEEG(i).subject))
        end
        cnt=cnt+1;
    end
end
% CSD maps of those
for i = 1:length(ALLEEG(1).chanlocs)
    chans(i) = {ALLEEG(1).chanlocs(i).labels};
end
chans=chans.';
% pause because it somehow needs it :?
pause(1)
[M] = ExtractMontage ('10-5-System_Mastoids_EGI129.csd', chans);
% MapMontage(M)
pause(1)
[G,H] = GetGH(M); %default: 4, otherwise e.g.: GetGH(M, 3)


figure;
for i = 1:length(ALLEEG)
    cnt=1;
    for k = 1:21:21*N
        h=subplot(N,length(ALLEEG),k+(i-1));
        try
            CSD_tmp = CSD(ALLEEG(i).icawinv(:,goodComps(i,cnt)),G,H);
            topoplot(CSD_tmp, ALLEEG(i).chanlocs)
        catch
            delete(h)
        end
        if k ==1
            title(sprintf('%s',ALLEEG(i).subject))
        end
        cnt=cnt+1;
    end
end


end