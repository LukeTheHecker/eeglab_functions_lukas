function [pvaf, goodComps]=component_pvaf_conddiff(STUDY, ALLEEG, range,chanind,conds, min_pvaf, lorExp)
%% this function calculates the percent of variance accounted for (pvaf)
%  of every component present in the ALLEEG structure. 
%  Example for N170:
%  [pvaf_all, goodComps_all]=component_pvaf_conddiff(STUDY, ALLEEG, [150 190], [23 17],[2 4], 5,0);

%% Initial Variables
global STUDY
global ALLEEG

% unpack condition names
if length(STUDY.design.variable(1).value{1})==2
    condname1 = {STUDY.design.variable(1).value{1,1}{1,1} STUDY.design.variable(1).value{1,1}{1,2}};
    condname2 = {STUDY.design.variable(1).value{1,2}{1,1} STUDY.design.variable(1).value{1,2}{1,2}};
    fprintf('contrasting condition %s + %s and %s + %s', condname1{1}, condname1{2}, condname2{1}, condname2{2})

elseif length(STUDY.design.variable(1).value{1})==1
    condname1 = {STUDY.design.variable(1).value{conds(1)}};
    condname2 = {STUDY.design.variable(1).value{conds(2)}};
    fprintf('contrasting condition %s and %s', condname1{1}, condname2{1})
elseif length(STUDY.design.variable(1).value)==4
    condname1 = {STUDY.design.variable(1).value{conds(1)}};
    condname2 = {STUDY.design.variable(1).value{conds(2)}};
    fprintf('contrasting condition %s and %s', condname1{1}, condname2{1})

end

pvaf1 = NaN(length(ALLEEG), ALLEEG(1).nbchan);
pvaf2 = NaN(length(ALLEEG), ALLEEG(1).nbchan);
pvaf = NaN(length(ALLEEG), ALLEEG(1).nbchan);
goodComps=NaN(length(ALLEEG), ALLEEG(1).nbchan);
% take all channels if no channels are specified
if isempty(chanind)
    chanind=1:ALLEEG(1).nbchan;
end

%% loop through subjects in ALLEEG
for k = 1:length(ALLEEG)
    
    % load dataset of subject 'k' and create two subsets for the two
    % conditions
    EEG_tmp = pop_loadset('filepath', ALLEEG(k).filepath, 'filename', ALLEEG(k).filename);
    EEG_tmp_1 = pop_epoch(EEG_tmp, {condname1{:}}, [-0.4 1]);
    
    data1=mean(EEG_tmp_1.data,3);
    EEG_tmp_2 = pop_epoch(EEG_tmp, {condname2{:}}, [-0.4 1]);
    data2=mean(EEG_tmp_2.data,3);

    % time range in ms converted into pnts
    [~,minpnt]=min(abs(EEG_tmp_1.times-range(1)));
    [~,maxpnt]=min(abs(EEG_tmp_1.times-range(2)));
    pntrange = minpnt:maxpnt;

    % Get ica activation matrix (comp) and inv. weight matrix (winv)
    comp1 = mean(EEG_tmp_1.icaact(:,pntrange,:),3);
    comp2 = mean(EEG_tmp_2.icaact(:,pntrange,:),3);    
    winv = EEG_tmp_2.icawinv(chanind,:);
    
    % perform difference of the ERPs of the respective conditions & their
    % ica activation matrix
    dataDiff = data1-data2;
    compDiff = comp1-comp2;
    
    % Calculate pvaf diff for each component
    for i = 1:length(EEG_tmp_1.icawinv(1,:))
        [~, pvaf(k,i)] = compvar(dataDiff(chanind,pntrange), compDiff, winv, [i]);
        % Old approach: [~, pvaf2(k,i)] = compvar(data2(chanind,pntrange,:), comp2, winv, [i]);
    end
%     pvaf(k,:) = pvaf1(k,:)-pvaf2(k,:);
    % find the component indices that account for at least the given amount
    % of variance (e.g. 5%)
    ncomps=length(find(abs(pvaf(k,:))>=min_pvaf));
    pvaf_tmp=pvaf;
    pvaf_tmp(isnan(pvaf_tmp))=0;
    [sorted_pvaf, inds]=sort(abs(pvaf_tmp(k,:)),'descend');
    goodComps(k,1:ncomps) = inds(1:ncomps);

    % Loreta export
    if lorExp ==1
        eeglab2loreta( EEG_tmp.chanlocs,EEG_tmp.icawinv, 'compnum', goodComps(k,1:ncomps),'filecomp', [ALLEEG(k).subject '_comp_']);
    end
     
end

% plot the first N components of each subject that account for most
% variance
N=3;
figure;
for i = 1:length(ALLEEG)
    cnt=1;
    for k = 1:length(ALLEEG):length(ALLEEG)*N
        h=subplot(N,length(ALLEEG),k+(i-1));
        try
            topoplot(ALLEEG(i).icawinv(:,goodComps(i,cnt)), ALLEEG(i).chanlocs)
            if k ==1
                title({sprintf('%s',ALLEEG(i).subject),...
                    sprintf('%d',round(pvaf(i,goodComps(i,cnt))))},...
                    'Position', [0 1.3 0], 'VerticalAlignment','top',...
                    'HorizontalAlignment', 'center')
            else
                title(sprintf('%d',round(pvaf(i,goodComps(i,cnt)))),...
                    'Position', [0 1.3 0], 'VerticalAlignment','top',...
                    'HorizontalAlignment', 'center')
            end
        catch
            delete(h)
        end
        cnt=cnt+1;
    end
end
% CSD maps of those
for i = 1:length(ALLEEG(1).chanlocs)
    chans(i) = {ALLEEG(1).chanlocs(i).labels};
end
chans=chans.';
% pause because it somehow needs it :?
pause(1)
[M] = ExtractMontage ('10-5-System_Mastoids_EGI129.csd', chans);
% MapMontage(M)
pause(1)
[G,H] = GetGH(M); %default: 4, otherwise e.g.: GetGH(M, 3)


figure;
for i = 1:length(ALLEEG)
    cnt=1;
    for k = 1:length(ALLEEG):length(ALLEEG)*N
        h=subplot(N,length(ALLEEG),k+(i-1));
        try
            CSD_tmp = CSD(ALLEEG(i).icawinv(:,goodComps(i,cnt)),G,H);
            topoplot(CSD_tmp, ALLEEG(i).chanlocs)
            if k ==1
                title({sprintf('%s',ALLEEG(i).subject),...
                    sprintf('%d',round(pvaf(i,goodComps(i,cnt))))},...
                    'Position', [0 1.3 0], 'VerticalAlignment','top',...
                    'HorizontalAlignment', 'center')
            else
                title(sprintf('%d',round(pvaf(i,goodComps(i,cnt)))),...
                    'Position', [0 1.3 0], 'VerticalAlignment','top',...
                    'HorizontalAlignment', 'center')
            end
        catch
            delete(h)
        end
        if k ==1
            title(sprintf('%s',ALLEEG(i).subject))
        end
        cnt=cnt+1;
    end
end




end