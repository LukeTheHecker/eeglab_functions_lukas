function [pvaf, goodComps]=component_pvaf_conddiff(STUDY, ALLEEG, range,chanind,conds, min_pvaf)
%% this function calculates the percent of variance accounted for (pvaf)
%  of every component present in the ALLEEG structure. 
%  Example for N170:
%  [pvaf_all, goodComps_all]=component_pvaf_conddiff(STUDY, ALLEEG, [150 190], [23 17],[2 4], 5);

%% Initial Variables
global STUDY
global ALLEEG

fprintf('contrasting condition %s and %s', STUDY.design.variable(1).value{conds(1)}, STUDY.design.variable(1).value{conds(2)})

pvaf1 = NaN(length(ALLEEG), ALLEEG(1).nbchan);
pvaf2 = NaN(length(ALLEEG), ALLEEG(1).nbchan);
pvaf = NaN(length(ALLEEG), ALLEEG(1).nbchan);
goodComps=NaN(length(ALLEEG), ALLEEG(1).nbchan);
% take all channels if no channels are specified
if isempty(chanind)
    chanind=1:ALLEEG(1).nbchan;
end

%% loop through subjects in ALLEEG
for k = 1:length(ALLEEG)
    EEG_tmp = pop_loadset('filepath', ALLEEG(k).filepath, 'filename', ALLEEG(k).filename);
    EEG_tmp_1 = pop_epoch(EEG_tmp, {STUDY.design.variable(1).value{conds(1)}}, [-0.4 1]);
    data1=EEG_tmp_1.data;
    EEG_tmp_2 = pop_epoch(EEG_tmp, {STUDY.design.variable(1).value{conds(2)}}, [-0.4 1]);
    data2=EEG_tmp_2.data;

    % time range in ms converted into pnts
    [~,minpnt]=min(abs(ALLEEG(k).times-range(1)));
    [~,maxpnt]=min(abs(ALLEEG(k).times-range(2)));
    pntrange = minpnt:maxpnt;
    % min pvaf (%) for component of interest
    %min_pvaf=5;


    comp1 = EEG_tmp_1.icaact(:,pntrange,:);
    comp2 = EEG_tmp_2.icaact(:,pntrange,:);    
    winv = EEG_tmp_2.icawinv(chanind,:);

    % Calculate pvaf diff for each component
    for i = 1:length(EEG_tmp_1.icawinv(1,:))
        [~, pvaf1(k,i)] = compvar(data1(chanind,pntrange,:), comp1, winv, [i]);
        [~, pvaf2(k,i)] = compvar(data2(chanind,pntrange,:), comp2, winv, [i]);
    end
    pvaf(k,:) = pvaf1(k,:)-pvaf2(k,:);
    ncomps=length(find(abs(pvaf(k,:))>=min_pvaf));
    pvaf_tmp=pvaf;
    pvaf_tmp(isnan(pvaf_tmp))=0;
    [sorted_pvaf, inds]=sort(abs(pvaf_tmp(k,:)),'descend');
    goodComps(k,1:ncomps) = inds(1:ncomps);

     
end

% plot the first N components of each subject that account for most
% variance
N=3;
figure;
for i = 1:length(ALLEEG)
    cnt=1;
    for k = 1:21:21*N
        h=subplot(N,length(ALLEEG),k+(i-1));
        try
            topoplot(ALLEEG(i).icawinv(:,goodComps(i,cnt)), ALLEEG(i).chanlocs)
        catch
            delete(h)
        end
        if k ==1
            title(sprintf('%s',ALLEEG(i).subject))
        end
        cnt=cnt+1;
    end
end
% CSD maps of those
for i = 1:length(ALLEEG(1).chanlocs)
    chans(i) = {ALLEEG(1).chanlocs(i).labels};
end
chans=chans.';
% pause because it somehow needs it :?
pause(1)
[M] = ExtractMontage ('10-5-System_Mastoids_EGI129.csd', chans);
% MapMontage(M)
pause(1)
[G,H] = GetGH(M); %default: 4, otherwise e.g.: GetGH(M, 3)


figure;
for i = 1:length(ALLEEG)
    cnt=1;
    for k = 1:21:21*N
        h=subplot(N,length(ALLEEG),k+(i-1));
        try
            CSD_tmp = CSD(ALLEEG(i).icawinv(:,goodComps(i,cnt)),G,H);
            topoplot(CSD_tmp, ALLEEG(i).chanlocs)
        catch
            delete(h)
        end
        if k ==1
            title(sprintf('%s',ALLEEG(i).subject))
        end
        cnt=cnt+1;
    end
end


end