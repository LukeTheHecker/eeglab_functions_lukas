[STUDY erpdata erptimes pgroup pcond pinter] = std_erpplot(STUDY, ALLEEG);


[STUDY ALLEEG customRes] = std_precomp(STUDY, ALLEEG, { ALLEEG(1).chanlocs.labels }, 'interp', ...
               'on', 'erp', 'on', 'spec', 'off', 'ersp', 'off', 'erspparams', ...
               { 'cycles' [ 3 0.5 ], 'alpha', 0.01, 'padratio' 1 });

[STUDY erpdata erptimes] = std_erpplot(STUDY,ALLEEG,'channels',{ 'Cz'});
std_plotcurve(erptimes, erpdata, 'plotconditions', 'together', ...
    'plotstderr', 'on', 'figure', 'on', 'colors',{'r'; 'k'}); 

% Defaults
width = 5;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 2;        % LineWidth
msz = 8;       % MarkerSize


%isequal('amb_reversal',STUDY.design.variable(1).value{1})
amb_stab=permute(cell2mat(erpdata(2)),[3 2 1]);
amb_rev=permute(cell2mat(erpdata(1)),[3 2 1]);
unamb_stab=permute(cell2mat(erpdata(4)),[3 2 1]);
unamb_rev=permute(cell2mat(erpdata(3)),[3 2 1]);

% stabrev combined, respectively:
% amb_SR = permute(cell2mat(erpdata(1)),[3 2 1]);
% unamb_SR = permute(cell2mat(erpdata(2)),[3 2 1]);

% if any(size(unamb)~=size(amb))
%     unamb=unamb(1:20,:,:);
% end
% for i = 1:length(amb_SR(:,1,1))
%     for j = 1:351
%         amb_mast(i,:,j) = amb_stab(i,:,j)-mean([amb_stab(i,17,j), amb_stab(i,22,j)]);
%         unamb_mast(i,:,j) = unamb_stab(i,:,j)-mean([unamb_stab(i,17,j), unamb_stab(i,22,j)]);
%     end
% end
tmp=[];
tmp(:,:,1) = squeeze(mean(unamb_rev,1));
tmp(:,:,2) = squeeze(mean(amb_rev,1));
fig=figure;
% pos = get(gcf, 'Position');
% set(gcf, 'Position', [pos(1) pos(2) width*300, height*300]); %<- Set size
% set(gca, 'FontSize', fsz, 'LineWidth', alw); %<- Set properties
plottopo(tmp, 'chanlocs', ALLEEG(1).chanlocs,'limits',[-60 800 0 0],...
    'colors',{{'k' 'linewidth' 1.5 } {'r' 'linewidth' 1.5 }},'ydir',1,...
    'showleg','on')
set(fig, 'PaperPositionMode', 'auto')
print('/Applications/eeglab14_1_2b/Lattice_U_A_plottopo','-dpng','-r600');

fig=figure;
pos = get(gcf, 'Position');
set(gcf, 'Position', [pos(1) pos(2) width*300, height*300]); %<- Set size
set(gca, 'FontSize', fsz, 'LineWidth', alw); %<- Set properties
plottopo(tmp(:,:,1)-tmp(:,:,2), 'chanlocs', ALLEEG(1).chanlocs,'limits',[-400 1000 0 0],...
    'colors',{'k' 'linewidth' 1.5 },'ydir',1,...
    'showleg','on')
set(fig, 'PaperPositionMode', 'auto')



% Plot Cz only. with 95% Confidence Interval

var1 = amb_rev;
var2 = amb_stab;

for i = 1:length(CI_amb(1,:))
    tmp = var1(:,14,i);
    SEM = std(tmp)/sqrt(length(tmp));
    ts = tinv([0.025  0.975],length(tmp)-1);
    CI_amb_stab_pos(1,i) = mean(tmp)+max(ts)*SEM;
    CI_amb_stab_neg(1,i) = mean(tmp)+min(ts)*SEM;
end
for i = 1:length(CI_unamb(1,:))
    tmp = var2(:,14,i);
    SEM = std(tmp)/sqrt(length(tmp));
    ts = tinv([0.025  0.975],length(tmp)-1);
    CI_amb_rev_pos(1,i) = mean(tmp)+max(ts)*SEM;
    CI_amb_rev_neg(1,i) = mean(tmp)+min(ts)*SEM;
end

avg1=squeeze(mean(var1(:,14,:),1));
avg2=squeeze(mean(var2(:,14,:),1));

% Running Permutation Test on U vs. A
sig_arr = NaN(1,351);
for i=1:length(sig_arr(1,:))
    [p] = pairedPerm(var1(:,14,i),var2(:,14,i), 10000);
    if p<0.05
        sig_arr(1,i)=1;
    end
end

tmp_Astab_smooth = movmean(avg1,5);
tmp_Arev_smooth = movmean(avg2,5);
CI_amb_stab_pos_smooth = movmean(CI_amb_stab_pos,5);
CI_amb_stab_neg_smooth = movmean(CI_amb_stab_neg,5);
CI_amb_rev_pos_smooth = movmean(CI_amb_rev_pos,5);
CI_amb_rev_neg_smooth = movmean(CI_amb_rev_neg,5);

figure;
% draw Unamb and Amb Line
plot(erptimes,tmp_Astab_smooth,'k','LineWidth',lw)
hold on
plot(erptimes,tmp_Arev_smooth,'r','LineWidth',lw)
xlim([-60 800])
hold on
% Fill Confidence intervals
x2 = [erptimes, fliplr(erptimes)];
inBetween = [CI_amb_rev_pos_smooth, fliplr(CI_amb_rev_neg_smooth)];
fill(x2, inBetween, [0.5 0 0])
hold off
alpha(.5)
hold on
x2 = [erptimes, fliplr(erptimes)];
inBetween = [CI_amb_stab_pos_smooth, fliplr(CI_amb_stab_neg_smooth)];
fill(x2, inBetween, [0.3 0.3 0.3])
hold off
alpha(.5)
hold on
% significance line
scatter(erptimes,sig_arr*-1,'o', 'MarkerEdgeColor', 'r', 'MarkerFaceColor', 'r')
hold on
% horizontal line
plot([min(erptimes) max(erptimes)],[0 0],'k')
hold on
% vertical line
plot([0 0],[-4 4],'k')
ylabel('Amplitude in \muV','FontSize', 14)
xlabel('Time in ms','FontSize', 14')
xt = get(gca, 'XTick');
set(gca, 'FontSize', 14)
% save figure
print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/ERP-Ambiguity-Effect/Lat/stabrev/Lattice_Arev_Astab_Cz_smooth','-dpng','-r300');


% var1=unamb_SR;
% var2=amb_SR;
% 
% % create Global field power
% gfp=std(squeeze(mean(var1-var2,1))); 
% % gfp_mast=std(squeeze(mean(unamb_mast-amb_mast,1))); 
% % CSD stuff
% for i = 1:length(ALLEEG(1).chanlocs)
%     chans(i) = {ALLEEG(1).chanlocs(i).labels};
% end
% chans=chans.';
% pause(0.1)
% [M] = ExtractMontage ('10-5-System_Mastoids_EGI129.csd', chans);
% % MapMontage(M)
% [G,H] = GetGH(M); %default: 4, otherwise e.g.: GetGH(M, 3)
% 
% % Defaults for this blog post
% width = 3;     % Width in inches
% height = 3;    % Height in inches
% alw = 0.75;    % AxesLineWidth
% fsz = 11;      % Fontsize
% lw = 2;        % LineWidth
% msz = 8;       % MarkerSize
% 
% % figure;
% % plot(erptimes.',gfp.', 'k','linewidth', 2)
% % title('U-A gfp')
% % running average with sliding window of width 3
% M = movmean(gfp,3);
% figure(1);
% pos = get(gcf, 'Position');
% set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
% set(gca, 'FontSize', fsz, 'LineWidth', alw); %<- Set properties
% plot(erptimes,M, 'k','linewidth', lw)
% xlim([-60 800])
% hold on
% plot(erptimes(1,tROI(1,1):tROI(5,2)),M(1,tROI(1,1):tROI(5,2)),'k','linewidth',3)
% ylabel('GFP [\muV]', 'FontSize',13)
% xlabel('Time [ms]',  'FontSize',13)
% xt = get(gca, 'XTick');
% set(gca, 'FontSize', 13)
% hold on
% 
% % Show temporal regions of interest (Lattices)
% tROI=[find(erptimes==-60),find(erptimes==84);...
%     find(erptimes==84),find(erptimes==200);...
%     find(erptimes==200),find(erptimes==316);...
%     find(erptimes==316),find(erptimes==612);...
%     find(erptimes==612),find(erptimes==800)];
% for i = 1:length(tROI)
%     area(erptimes(1,tROI(i,1):tROI(i,2)),M(1,tROI(i,1):tROI(i,2)))
% end
% % save to eeglab path
% print('/Applications/eeglab14_1_2b/improvedExample','-dpng','-r300');
% 
% for i=1:length(tROI)
%     tmp_range = tROI(i,1):tROI(i,2);
%     a_crit=0.05;
%     timerange=[erptimes(tROI(i,1)) erptimes(tROI(i,2))];
%     elecs=zeros(1,ALLEEG(1).nbchan);
%     for k=1:length(elecs)
%         %[p,d,cohd]=permutationTest(mean(var1(:,i,t1:t2),3),mean(var2(:,i,t1:t2),3),2000);
%         [p,~] = signrank(mean(var1(:,k,tmp_range),3),mean(var2(:,k,tmp_range),3));
%         if p<a_crit
%             elecs(k)=1;
%         end
%     end
%     [~,low] = min(abs(erptimes-timerange(1)));
%     [~,high] = min(abs(erptimes-timerange(2)));
%     find(erptimes==timerange(1))
%     figure;
%     subplot(1,3,1)
%     tmp=squeeze(mean(var1-var2,1));
%     tmp = tmp(:,low:high);
%     topoplot(mean(tmp,2), ALLEEG(1).chanlocs,'emarker2' ,{[find(elecs==1)],'*','k',4})
%     cbh=colorbar;
%     cbh.FontSize = 13;
%     ylabel(cbh, '[\muV]')
%     title(sprintf('U-A Topo at %d-%d ms', timerange(1), timerange(2)))
%     subplot(1,3,2)
%     CSD_tmp = CSD(tmp,G,H);
%     topoplot(mean(CSD_tmp,2), ALLEEG(1).chanlocs,'emarker2' ,{[find(elecs==1)],'*','k',4})
%     cbh=colorbar;
%     cbh.FontSize = 13;
%     ylabel(cbh, '[\muV / cm^2]')
%     title(sprintf('U-A CSD at %d-%d ms', timerange(1), timerange(2)))
%     subplot(1,3,3)
%     headplot(mean(CSD_tmp,2),'idk.spl','electrodes','off','view','top')
%     title(sprintf('U-A CSD at %d-%d ms', timerange(1), timerange(2)))
%     print(sprintf('/Applications/eeglab14_1_2b/Lattice_U-A_%d_%dms',erptimes(tROI(i,1)),erptimes(tROI(i,2))),'-dpng','-r300')
% end

eeglab2loreta(ALLEEG(1).chanlocs, squeeze(mean(var1,1))-squeeze(mean(var2,1)), 'exporterp','on','labelonly','on','filecomp', 'Lattices_U-A')
