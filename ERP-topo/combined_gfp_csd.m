[STUDY erpdata erptimes pgroup pcond pinter] = std_erpplot(STUDY, ALLEEG);
% enter time limits -400 1000, no stats

[STUDY ALLEEG customRes] = std_precomp(STUDY, ALLEEG, { ALLEEG(1).chanlocs.labels }, 'interp', ...
               'on', 'erp', 'on', 'spec', 'off', 'ersp', 'off', 'erspparams', ...
               { 'cycles' [ 3 0.5 ], 'alpha', 0.01, 'padratio' 1 });

% Defaults
width = 5;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 2;        % LineWidth
msz = 8;       % MarkerSize

% %isequal('amb_reversal',STUDY.design.variable(1).value{1})
% amb_stab=permute(cell2mat(erpdata(2)),[3 2 1]);
% amb_rev=permute(cell2mat(erpdata(1)),[3 2 1]);
% unamb_stab=permute(cell2mat(erpdata(4)),[3 2 1]);
% unamb_rev=permute(cell2mat(erpdata(3)),[3 2 1]);

% stabrev combined, respectively:
amb_SR = permute(cell2mat(erpdata(1)),[3 2 1]);
unamb_SR = permute(cell2mat(erpdata(2)),[3 2 1]);


% topographic ERP plot
tmp=[];
tmp(:,:,1) = squeeze(mean(unamb_SR,1));
tmp(:,:,2) = squeeze(mean(amb_SR,1));

var1=unamb_SR;
var2=amb_SR;

% create Global field power
gfp=std(squeeze(mean(var1-var2,1))); 
gfp_smooth = gfp;   %movmean(gfp,10);

% gfp_mast=std(squeeze(mean(unamb_mast-amb_mast,1))); 
% CSD stuff
for i = 1:length(ALLEEG(1).chanlocs)
    chans(i) = {ALLEEG(1).chanlocs(i).labels};
end
chans=chans.';
pause(1)
[M] = ExtractMontage ('10-5-System_Mastoids_EGI129.csd', chans);
% MapMontage(M)
pause(1)
[G,H] = GetGH(M); %default: 4, otherwise e.g.: GetGH(M, 3)

% Auto tROI: Searches the [ntROI] most prominent minima in the gfp and 
% creates the tROI array. Boarders: [40 800] ms
limits = [-400 1000];
ntROI = 3;
tmprange=find(erptimes==limits(1)):find(erptimes==limits(2));
[~, prominence]=islocalmin(gfp(tmprange));
[sprom, indices]=sort(prominence,'descend');
% get rid of indices to close to boarders
tmp=indices(indices>30);
sindices = sort(tmp(1:ntROI),'ascend');
erptimes(sindices(1:ntROI)+find(erptimes==limits(1))-1)

tROI = [];
for i = 1:ntROI+1
    if i == 1
        tROI = [tROI; find(erptimes==limits(1)), find(erptimes==erptimes(sindices(i)+find(erptimes==40)-1))];
    elseif i == ntROI+1
        tROI = [tROI; find(erptimes==erptimes(sindices(i-1)+find(erptimes==40)-1)), find(erptimes==limits(2))];
    else
        tROI = [tROI; find(erptimes==erptimes(sindices(i-1)+find(erptimes==40)-1)), find(erptimes==erptimes(sindices(i)+find(erptimes==40)-1))];
    end
end

% Temporal regions of interest (Smileys)
% tROI=[find(erptimes==-60),find(erptimes==72);...
%     find(erptimes==72),find(erptimes==220);...
%     find(erptimes==220),find(erptimes==356);...
%     find(erptimes==356),find(erptimes==588);...
%     find(erptimes==588),find(erptimes==800)];
% Temporal regions of interest (Lattices)
% tROI=[find(erptimes==-60),find(erptimes==84);...
%     find(erptimes==84),find(erptimes==200);...
%     find(erptimes==200),find(erptimes==316);...
%     find(erptimes==316),find(erptimes==612);...
%     find(erptimes==612),find(erptimes==800)];

% Global field power with running average with sliding window of width "3"

figure;
subplot(3,length(tROI),[9 12])
pos = get(gcf, 'Position');
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
set(gca, 'FontSize', fsz, 'LineWidth', alw); %<- Set properties
% plot the gfp
plot(erptimes,gfp_smooth, 'k','linewidth', lw)
xlim([limits(1) limits(2)])
ylim([0 max(gfp_smooth)*2])
hold on
% plot the gfp
%plot(erptimes(1,tROI(1,1):tROI(length(tROI),2)),gfp_smooth(1,tROI(1,1):tROI(length(tROI),2)),'k','linewidth',3)
ylabel('GFP [\muV]', 'FontSize',18)
xlabel('Time [ms]',  'FontSize',18)
xt = get(gca, 'XTick');
set(gca, 'FontSize', 18,'Layer','top')
hold on
% Fill tROI with colors
romanNum=["I", "II", "III", "IV", "V"];
for i = 1:length(tROI)
    % fill area of tROI
    h = area(erptimes(1,tROI(i,1):tROI(i,2)),gfp_smooth(1,tROI(i,1):tROI(i,2)));
    if mod(i,2)==1
        h.FaceColor=[0.8 0.8 0.8];
    else
        h.FaceColor=[0.6 0.6 0.6];
    end
    hold on
    % add vertical lines
    plot([erptimes(tROI(i,1)) erptimes(tROI(i,1))],[0 max(gfp_smooth)*2], 'k--', 'LineWidth', 1)
    hold on
    plot([erptimes(tROI(i,2)) erptimes(tROI(i,2))],[0 max(gfp_smooth)*2], 'k--', 'LineWidth', 1)
    % add tROI number
    txt = char(romanNum(i));
    txt2 = num2str(erptimes(tROI(i,1)));
    text((erptimes(tROI(i,1))+erptimes(tROI(i,2)))/2,...
        (max(gfp_smooth)+max(gfp_smooth)*2)/2,txt, 'FontSize', 20,...
        'HorizontalAlignment','center')
    text(erptimes(tROI(i,1)), max(gfp_smooth)*2.125, txt2, 'HorizontalAlignment','center', 'FontSize', 18)
end

% CAR Scalp maps & CSD maps within tROI
for i=1:length(tROI)
    tmp_range = tROI(i,1):tROI(i,2);
    a_crit=0.01;
    timerange=[erptimes(tROI(i,1)) erptimes(tROI(i,2))];
    elecs=zeros(1,ALLEEG(1).nbchan);
    for k=1:length(elecs)
        [p,d]=pairedPerm(mean(var1(:,k,tmp_range),3),mean(var2(:,k,tmp_range),3),10000);
        %[p,~] = signrank(mean(var1(:,k,tmp_range),3),mean(var2(:,k,tmp_range),3));
        if p<a_crit
            elecs(k)=1;
        end
    end
    [~,low] = min(abs(erptimes-timerange(1)));
    [~,high] = min(abs(erptimes-timerange(2)));
    find(erptimes==timerange(1))
    
    subplot(3,length(tROI),i)
    tmp=squeeze(mean(var1-var2,1));
    tmp = tmp(:,low:high);
    topoplot(mean(tmp,2), ALLEEG(1).chanlocs,'emarker2' ,{[find(elecs==1)],'p','k',6})%,'maplimits',[-10 10])
    cbh=colorbar;
    cbh.FontSize = 18;
    set(cbh, 'location', 'southoutside')
    ylabel(cbh, '[\muV]')
    % Column title
    if i<6
        title(char(romanNum(i)), 'FontSize', 18,...
            'HorizontalAlignment','center');

    end
    % Row title
    if i == 1
        text(-1, 0,'Scalp Map', 'FontSize', 18,'HorizontalAlignment','center');
    end
    % CSD Plot
    subplot(3,length(tROI),i+length(tROI))
    tmp=squeeze(mean(var1-var2,1));
    tmp = tmp(:,low:high);
    CSD_tmp = CSD(tmp,G,H);
    topoplot(mean(CSD_tmp,2), ALLEEG(1).chanlocs,'emarker2' ,{[find(elecs==1)],'p','k',6})%,'maplimits',[-10 10])
    cbh=colorbar;
    cbh.FontSize = 18;
    set(cbh, 'location', 'southoutside')
    ylabel(cbh, '[\muV / cm^2]')
    if i == 1
        text(-1, 0,'CSD', 'FontSize', 18,'HorizontalAlignment','center');
    end
end
% save to eeglab path
print('/Applications/eeglab14_1_2b/Lattices_U-A_GFP_CSD_RelativeScale','-dpng','-r300');

eeglab2loreta(ALLEEG(1).chanlocs, squeeze(mean(var1,1))-squeeze(mean(var2,1)), 'exporterp','on','labelonly','on','filecomp', 'Lattices_U-A')
% Export diff-erp
tmp_lor=squeeze(mean(var1-var2,1));
eeglab2loreta( ALLEEG(1).chanlocs,tmp_lor, 'exporterp','on','filecomp', 'Lat_wholetime_scalpMap');

% Export single-subject loreta for each condition
% MISTAKE HERE: var1 = unamb, not amb!!!
for i = 1%:length(ALLEEG)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(var1(i,:,:)), 'exporterp','on','filecomp', sprintf('%s_Lat_unamb',ALLEEG(i).subject));
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(var2(i,:,:)), 'exporterp','on','filecomp', sprintf('%s_Lat_amb',ALLEEG(i).subject));
end
%% All trials concatenated

