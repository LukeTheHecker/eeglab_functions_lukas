STUDY = pop_specparams(STUDY, 'subtractsubjectmean','off','freqrange',[1 120] );
[STUDY specdata specfreqs pgroup pcond pinter] = std_specplot(STUDY,ALLEEG,'channels',{'Fp1' 'Fp2' 'F7' 'F3' 'Fz' 'F4' 'F8' 'FC5' 'FC1' 'FC2' 'FC6' 'T7' 'C3' 'Cz' 'C4' 'T8' 'TP9' 'CP5' 'CP1' 'CP2' 'CP6' 'TP10' 'P7' 'P3' 'Pz' 'P4' 'P8' 'PO9' 'O1' 'Oz' 'O2' 'PO10'});


amb=permute(cell2mat(specdata(1)),[3 2 1]);
unamb=permute(cell2mat(specdata(2)),[3 2 1]);
if any(size(unamb)~=size(amb))
    unamb=unamb(1:20,:,:);
end
% create Global field power
gfp=std(squeeze(mean(unamb-amb,1))); 
 
% CSD stuff
for i = 1:length(ALLEEG(1).chanlocs)
    chans(i) = {ALLEEG(1).chanlocs(i).labels};
end
chans=chans.';
[M] = ExtractMontage ('10-5-System_Mastoids_EGI129.csd', chans);
% MapMontage(M)
[G,H] = GetGH(M); %default: 4, otherwise e.g.: GetGH(M, 3)


figure;
plot(specfreqs.',gfp.')

freqrange=[30 50];
[~,low] = min(abs(specfreqs-freqrange(1)));
[~,high] = min(abs(specfreqs-freqrange(2)));
figure;
subplot(1,3,1)
tmp=squeeze(mean(unamb-amb,1));
tmp = tmp(:,low:high);
topoplot(mean(tmp,2), ALLEEG(1).chanlocs)
colorbar;
title(sprintf('U-A Topo at %d-%d hz', freqrange(1), freqrange(2)))
subplot(1,3,2)
CSD_tmp = CSD(tmp,G,H);
topoplot(mean(CSD_tmp,2), ALLEEG(1).chanlocs)
colorbar;
title(sprintf('U-A CSD at %d-%d hz', freqrange(1), freqrange(2)))
subplot(1,3,3)
headplot(mean(CSD_tmp,2),'idk.spl')
colorbar;
title(sprintf('U-A CSD at %d-%d hz', freqrange(1), freqrange(2)))



