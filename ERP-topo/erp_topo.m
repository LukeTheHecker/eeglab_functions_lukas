[STUDY erpdata erptimes pgroup pcond pinter] = std_erpplot(STUDY, ALLEEG);


[STUDY ALLEEG customRes] = std_precomp(STUDY, ALLEEG, { ALLEEG(1).chanlocs.labels }, 'interp', ...
               'on', 'erp', 'on', 'spec', 'off', 'ersp', 'off', 'erspparams', ...
               { 'cycles' [ 3 0.5 ], 'alpha', 0.01, 'padratio' 1 });

% Defaults
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 2;        % LineWidth
msz = 8;       % MarkerSize

% %isequal('amb_reversal',STUDY.design.variable(1).value{1})
% amb_stab=permute(cell2mat(erpdata(2)),[3 2 1]);
% amb_rev=permute(cell2mat(erpdata(1)),[3 2 1]);
% unamb_stab=permute(cell2mat(erpdata(4)),[3 2 1]);
% unamb_rev=permute(cell2mat(erpdata(3)),[3 2 1]);

% stabrev combined, respectively:
amb_SR = permute(cell2mat(erpdata(1)),[3 2 1]);
unamb_SR = permute(cell2mat(erpdata(2)),[3 2 1]);

% if any(size(unamb)~=size(amb))
%     unamb=unamb(1:20,:,:);
% end
% for i = 1:length(amb_SR(:,1,1))
%     for j = 1:351
%         amb_mast(i,:,j) = amb_stab(i,:,j)-mean([amb_stab(i,17,j), amb_stab(i,22,j)]);
%         unamb_mast(i,:,j) = unamb_stab(i,:,j)-mean([unamb_stab(i,17,j), unamb_stab(i,22,j)]);
%     end
% end
tst(:,:,1) = squeeze(mean(unamb_SR,1));
tst(:,:,2) = squeeze(mean(amb_SR,1));
figure;
pos = get(gcf, 'Position');
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
set(gca, 'FontSize', fsz, 'LineWidth', alw); %<- Set properties
plottopo(tst, 'chanlocs', ALLEEG(1).chanlocs,'limits',[-60 800 0 0],...
    'colors',{{'k' 'linewidth' 1.5 } {'r' 'linewidth' 1.5 }},'ydir',1,...
    'showleg','on')
print('/Applications/eeglab14_1_2b/Smiley_U_A_plottopo','-dpng','-r300');

var1=unamb_SR;
var2=amb_SR;

% create Global field power
gfp=std(squeeze(mean(var1-var2,1))); 
% gfp_mast=std(squeeze(mean(unamb_mast-amb_mast,1))); 
% CSD stuff
for i = 1:length(ALLEEG(1).chanlocs)
    chans(i) = {ALLEEG(1).chanlocs(i).labels};
end
chans=chans.';
pause(0.1)
[M] = ExtractMontage ('10-5-System_Mastoids_EGI129.csd', chans);
% MapMontage(M)
[G,H] = GetGH(M); %default: 4, otherwise e.g.: GetGH(M, 3)


% figure;
% plot(erptimes.',gfp.', 'k','linewidth', 2)
% title('U-A gfp')
% running average with sliding window of width 3
M = movmean(gfp,3);
figure;
pos = get(gcf, 'Position');
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
set(gca, 'FontSize', fsz, 'LineWidth', alw); %<- Set properties
plot(erptimes,M, 'k','linewidth', lw)
xlim([-60 800])
hold on
plot(erptimes(1,tROI(1,1):tROI(5,2)),M(1,tROI(1,1):tROI(5,2)),'k','linewidth',3)
ylabel('GFP [\muV]', 'FontSize',13)
xlabel('Time [ms]',  'FontSize',13)
xt = get(gca, 'XTick');
set(gca, 'FontSize', 13)
hold on

% Show temporal regions of interest (Smileys)
tROI=[find(erptimes==-60),find(erptimes==72);...
    find(erptimes==72),find(erptimes==220);...
    find(erptimes==220),find(erptimes==356);...
    find(erptimes==356),find(erptimes==588);...
    find(erptimes==588),find(erptimes==800)];
for i = 1:length(tROI)
    area(erptimes(1,tROI(i,1):tROI(i,2)),M(1,tROI(i,1):tROI(i,2)))
end
% save to eeglab path
print('/Applications/eeglab14_1_2b/Smiley_U-A_GFP','-dpng','-r300');

for i=1:length(tROI)
    tmp_range = tROI(i,1):tROI(i,2);
    a_crit=0.05;
    timerange=[erptimes(tROI(i,1)) erptimes(tROI(i,2))];
    elecs=zeros(1,ALLEEG(1).nbchan);
    for k=1:length(elecs)
        %[p,d,cohd]=permutationTest(mean(var1(:,i,t1:t2),3),mean(var2(:,i,t1:t2),3),2000);
        [p,~] = signrank(mean(var1(:,k,tmp_range),3),mean(var2(:,k,tmp_range),3));
        if p<a_crit
            elecs(k)=1;
        end
    end
    [~,low] = min(abs(erptimes-timerange(1)));
    [~,high] = min(abs(erptimes-timerange(2)));
    find(erptimes==timerange(1))
    figure;
    subplot(1,3,1)
    tmp=squeeze(mean(var1-var2,1));
    tmp = tmp(:,low:high);
    topoplot(mean(tmp,2), ALLEEG(1).chanlocs,'emarker2' ,{[find(elecs==1)],'*','k',4})
    cbh=colorbar;
    cbh.FontSize = 13;
    ylabel(cbh, '[\muV]')
    title(sprintf('U-A Topo at %d-%d ms', timerange(1), timerange(2)))
    subplot(1,3,2)
    CSD_tmp = CSD(tmp,G,H);
    topoplot(mean(CSD_tmp,2), ALLEEG(1).chanlocs,'emarker2' ,{[find(elecs==1)],'*','k',4})
    cbh=colorbar;
    cbh.FontSize = 13;
    ylabel(cbh, '[\muV / cm^2]')
    title(sprintf('U-A CSD at %d-%d ms', timerange(1), timerange(2)))
    subplot(1,3,3)
    headplot(mean(CSD_tmp,2),'idk.spl','electrodes','off','view','top')
    title(sprintf('U-A CSD at %d-%d ms', timerange(1), timerange(2)))
    print(sprintf('/Applications/eeglab14_1_2b/Smiley_U-A_%d_%dms',erptimes(tROI(i,1)),erptimes(tROI(i,2))),'-dpng','-r300')
end

eeglab2loreta(ALLEEG(1).chanlocs, squeeze(mean(var1,1))-squeeze(mean(var2,1)), 'exporterp','on','labelonly','on','filecomp', 'Lattices_U-A')
