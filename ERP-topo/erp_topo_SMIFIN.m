[STUDY erpdata erptimes pgroup pcond pinter] = std_erpplot(STUDY, ALLEEG);


[STUDY ALLEEG customRes] = std_precomp(STUDY, ALLEEG, { ALLEEG(1).chanlocs.labels }, 'interp', ...
               'on', 'erp', 'on', 'spec', 'off', 'ersp', 'off', 'erspparams', ...
               { 'cycles' [ 3 0.5 ], 'alpha', 0.01, 'padratio' 1 });

% Defaults
width = 5;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 2;        % LineWidth
msz = 8;       % MarkerSize

% %isequal('amb_reversal',STUDY.design.variable(1).value{1})
% amb_stab=permute(cell2mat(erpdata(2)),[3 2 1]);
% amb_rev=permute(cell2mat(erpdata(1)),[3 2 1]);
% unamb_stab=permute(cell2mat(erpdata(4)),[3 2 1]);
% unamb_rev=permute(cell2mat(erpdata(3)),[3 2 1]);

% stabrev combined, respectively:
amb_SR = permute(cell2mat(erpdata(1)),[3 2 1]);
unamb_SR = permute(cell2mat(erpdata(2)),[3 2 1]);

% if any(size(unamb)~=size(amb))
%     unamb=unamb(1:20,:,:);
% end
% for i = 1:length(amb_SR(:,1,1))
%     for j = 1:351
%         amb_mast(i,:,j) = amb_stab(i,:,j)-mean([amb_stab(i,17,j), amb_stab(i,22,j)]);
%         unamb_mast(i,:,j) = unamb_stab(i,:,j)-mean([unamb_stab(i,17,j), unamb_stab(i,22,j)]);
%     end
% end

% topographic ERP plot
tmp=[];
tmp(:,:,1) = squeeze(mean(unamb_SR,1));
tmp(:,:,2) = squeeze(mean(amb_SR,1));
fig=figure;
pos = get(gcf, 'Position');
set(gcf, 'Position', [pos(1) pos(2) width*300, height*300]); %<- Set size
set(gca, 'FontSize', fsz, 'LineWidth', alw); %<- Set properties
plottopo(tmp, 'chanlocs', ALLEEG(1).chanlocs,'limits',[-400 1000 0 0],...
    'colors',{{'k' 'linewidth' 1.5 } {'r' 'linewidth' 1.5 }},'ydir',1,...
    'showleg','on')
set(fig, 'PaperPositionMode', 'auto')
print('/Applications/eeglab14_1_2b/Smiley_U_A_plottopo','-dpng','-r600');
% diff plot
tmp=[];
tmp(:,:,1) = (squeeze(mean(unamb_SR,1))-squeeze(mean(amb_SR,1)))*(1);
fig=figure;
pos = get(gcf, 'Position');
set(gcf, 'Position', [pos(1) pos(2) width*300, height*300]); %<- Set size
set(gca, 'FontSize', fsz, 'LineWidth', alw); %<- Set properties
plottopo(tmp, 'chanlocs', ALLEEG(1).chanlocs,'limits',[-400 1000 0 0],...
    'colors',{{'k' 'linewidth' 1.5 } },'ydir',1,...
    'showleg','on')
set(fig, 'PaperPositionMode', 'auto')

print('/Applications/eeglab14_1_2b/Smiley_U_A_plottopo','-dpng','-r600');


% Plot Cz only. with 95% Confidence Interval
CI_amb = zeros(1,351);
CI_unamb = zeros(1,351);
for i = 1:length(CI_amb(1,:))
    tmp = amb_SR(:,14,i);
    SEM = std(tmp)/sqrt(length(tmp));
    ts = tinv([0.025  0.975],length(tmp)-1);
    CI_amb_pos(1,i) = mean(tmp)+max(ts)*SEM;
    CI_amb_neg(1,i) = mean(tmp)+min(ts)*SEM;
end
for i = 1:length(CI_unamb(1,:))
    tmp = unamb_SR(:,14,i);
    SEM = std(tmp)/sqrt(length(tmp));
    ts = tinv([0.025  0.975],length(tmp)-1);
    CI_unamb_pos(1,i) = mean(tmp)+max(ts)*SEM;
    CI_unamb_neg(1,i) = mean(tmp)+min(ts)*SEM;
end

tmp_U=squeeze(mean(unamb_SR(:,14,:),1));
tmp_A=squeeze(mean(amb_SR(:,14,:),1));

% Running Permutation Test on U vs. A
sig_arr = NaN(1,351);
p = NaN(1,351);
sig_arr_wilc = NaN(1,351);
p_wilc = NaN(1,351);

for i=1:length(sig_arr(1,:))
    [p(i)] = pairedPerm(unamb_SR(:,14,i),amb_SR(:,14,i), 10000);
    [p_wilc(i)] = signrank(unamb_SR(:,14,i),amb_SR(:,14,i));
    if p(1,i)<0.05
        sig_arr(1,i)=1;
    end
    if p_wilc(1,i)<0.05
        sig_arr_wilc(1,i)=1;
    end
end


figure;
% draw Unamb and Amb Line
plot(erptimes,tmp_U,'k','LineWidth',lw)
hold on
plot(erptimes,tmp_A,'r','LineWidth',lw)
xlim([-60 800])
hold on
% Fill Confidence intervals
x2 = [erptimes, fliplr(erptimes)];
inBetween = [CI_amb_pos, fliplr(CI_amb_neg)];
fill(x2, inBetween, [0.5 0 0])
hold off
alpha(.5)
hold on
x2 = [erptimes, fliplr(erptimes)];
inBetween = [CI_unamb_pos, fliplr(CI_unamb_neg)];
fill(x2, inBetween, [0.3 0.3 0.3])
hold off
alpha(.5)
hold on
% significance line perm
scatter(erptimes,sig_arr*-1,'o', 'MarkerEdgeColor', 'r', 'MarkerFaceColor', 'r')
hold on
% significance line wilc
scatter(erptimes,sig_arr_wilc*-2,'o', 'MarkerEdgeColor', 'b', 'MarkerFaceColor', 'b')
hold on
% horizontal line
plot([min(erptimes) max(erptimes)],[0 0],'k')
hold on
% vertical line
plot([0 0],[-2 5],'k')
ylabel('Amplitude in \muV','FontSize', 14)
xlabel('Time in ms','FontSize', 14')
xt = get(gca, 'XTick');
set(gca, 'FontSize', 14)
% save figure
print('/Applications/eeglab14_1_2b/Smiley_U_A_Cz_nice','-dpng','-r300');



var1=unamb_SR;
var2=amb_SR;

% create Global field power
gfp=std(squeeze(mean(var1-var2,1))); 
% gfp_mast=std(squeeze(mean(unamb_mast-amb_mast,1))); 
% CSD stuff
for i = 1:length(ALLEEG(1).chanlocs)
    chans(i) = {ALLEEG(1).chanlocs(i).labels};
end
chans=chans.';
pause(0.1)
[M] = ExtractMontage ('10-5-System_Mastoids_EGI129.csd', chans);
% MapMontage(M)
[G,H] = GetGH(M); %default: 4, otherwise e.g.: GetGH(M, 3)

% Temporal regions of interest (Smileys)
tROI=[find(erptimes==-60),find(erptimes==72);...
    find(erptimes==72),find(erptimes==220);...
    find(erptimes==220),find(erptimes==356);...
    find(erptimes==356),find(erptimes==588);...
    find(erptimes==588),find(erptimes==800)];

% Globa field power with running average with sliding window of width "3"

M = movmean(gfp,3);
figure;
pos = get(gcf, 'Position');
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
set(gca, 'FontSize', fsz, 'LineWidth', alw); %<- Set properties
plot(erptimes,M, 'k','linewidth', lw)
xlim([-60 800])
hold on
plot(erptimes(1,tROI(1,1):tROI(5,2)),M(1,tROI(1,1):tROI(5,2)),'k','linewidth',3)
ylabel('GFP [\muV]', 'FontSize',13)
xlabel('Time [ms]',  'FontSize',13)
xt = get(gca, 'XTick');
set(gca, 'FontSize', 13)
hold on


for i = 1:length(tROI)
    area(erptimes(1,tROI(i,1):tROI(i,2)),M(1,tROI(i,1):tROI(i,2)))
end
% save to eeglab path
print('/Applications/eeglab14_1_2b/Smiley_U-A_GFP','-dpng','-r300');


for i=1:length(tROI)
    tmp_range = tROI(i,1):tROI(i,2);
    a_crit=0.05;
    timerange=[erptimes(tROI(i,1)) erptimes(tROI(i,2))];
    elecs=zeros(1,ALLEEG(1).nbchan);
    for k=1:length(elecs)
        %[p,d,cohd]=permutationTest(mean(var1(:,i,t1:t2),3),mean(var2(:,i,t1:t2),3),2000);
        [p,~] = signrank(mean(var1(:,k,tmp_range),3),mean(var2(:,k,tmp_range),3));
        if p<a_crit
            elecs(k)=1;
        end
    end
    [~,low] = min(abs(erptimes-timerange(1)));
    [~,high] = min(abs(erptimes-timerange(2)));
    find(erptimes==timerange(1))
    figure;
    subplot(1,3,1)
    tmp=squeeze(mean(var1-var2,1));
    tmp = tmp(:,low:high);
    topoplot(mean(tmp,2), ALLEEG(1).chanlocs,'emarker2' ,{[find(elecs==1)],'*','k',4})
    cbh=colorbar;
    cbh.FontSize = 13;
    ylabel(cbh, '[\muV]')
    title(sprintf('U-A Topo at %d-%d ms', timerange(1), timerange(2)))
    subplot(1,3,2)
    CSD_tmp = CSD(tmp,G,H);
    topoplot(mean(CSD_tmp,2), ALLEEG(1).chanlocs,'emarker2' ,{[find(elecs==1)],'*','k',4})
    cbh=colorbar;
    cbh.FontSize = 13;
    ylabel(cbh, '[\muV / cm^2]')
    title(sprintf('U-A CSD at %d-%d ms', timerange(1), timerange(2)))
    subplot(1,3,3)
    headplot(mean(CSD_tmp,2),'idk.spl','electrodes','off','view','top')
    title(sprintf('U-A CSD at %d-%d ms', timerange(1), timerange(2)))
    print(sprintf('/Applications/eeglab14_1_2b/Smiley_U-A_%d_%dms',erptimes(tROI(i,1)),erptimes(tROI(i,2))),'-dpng','-r300')
end

eeglab2loreta(ALLEEG(1).chanlocs, squeeze(mean(var1,1))-squeeze(mean(var2,1)), 'exporterp','on','labelonly','on','filecomp', 'Smileys_U-A')
