%% t-crits of EPT with tROI approach loreta sources
lat_t_pos = [0.671222,NaN,2.310611,0.694165,0.608951];
lat_t_neg = [-1.867387,-0.624567,-1.992299,NaN,NaN];
smi_t_pos = [0.918438,NaN,0.717688,0.852018,0.472690];
smi_t_neg = [-3.861601,-1.040895,NaN,NaN,NaN];

foldernames = ["fullTime","tROI1","tROI2","tROI3","tROI4"];
filenames_lat = ["LatfullTime","tROI1","tROI2","tROI3","tROI4"];
filenames_smi = ["SmifullTime","tROI1","tROI2","tROI3","tROI4"];
dest_lat = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/loreta_brainplots/Surfaces/Lat/';
dest_smi = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/loreta_brainplots/Surfaces/Smi/';
sLORS_smi = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/LORETA/eLORETA_SNR49/smi/eLor_49/stats/t-stat_sw-norm_BLC/';
sLORS_lat = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/LORETA/eLORETA_SNR49/lat/eLor_49/stats/t-stat_sw-norm_BLC/';
for i=1:length(foldernames)
    %render_loreta_on_MNI(sLORS_lat, [char(filenames_lat(i)), '.slor'],[lat_t_pos(i),lat_t_neg(i)],2,[dest_lat, char(foldernames(i))])
    render_loreta_on_MNI(sLORS_smi, [char(filenames_smi(i)), '.slor'],[smi_t_pos(i),smi_t_neg(i)],2,[dest_smi, char(foldernames(i))])
end



LatTables = NaN(length(foldernames),6239,4);
SmiTables = NaN(length(foldernames),6239,4);
for i = 1:length(foldernames)
    LatTables(i,:,:)=slor_to_table(sLORS_lat, [char(filenames_lat(i)), '.slor']);
    SmiTables(i,:,:)=slor_to_table(sLORS_smi, [char(filenames_smi(i)), '.slor']);
end

% Cluster U>A
ClustStrucPos.LatFullTime = cluster_results_duplicates(LatTables(1,:,:),1,0,lat_t_pos(1));
ClustStrucPos.LattROI1 = cluster_results_duplicates(LatTables(2,:,:),1,0,lat_t_pos(2));
ClustStrucPos.LattROI2 = cluster_results_duplicates(LatTables(3,:,:),1,0,lat_t_pos(3));
ClustStrucPos.LattROI3 = cluster_results_duplicates(LatTables(4,:,:),1,0,lat_t_pos(4));
ClustStrucPos.LattROI4 = cluster_results_duplicates(LatTables(5,:,:),1,0,lat_t_pos(5));

ClustStrucPos.SmiFullTime = cluster_results_duplicates(SmiTables(1,:,:),1,0,smi_t_pos(1));
ClustStrucPos.SmitROI1 = cluster_results_duplicates(SmiTables(2,:,:),1,0,smi_t_pos(2));
ClustStrucPos.SmitROI2 = cluster_results_duplicates(SmiTables(3,:,:),1,0,smi_t_pos(3));
ClustStrucPos.SmitROI3 = cluster_results_duplicates(SmiTables(4,:,:),1,0,smi_t_pos(4));
ClustStrucPos.SmitROI4 = cluster_results_duplicates(SmiTables(5,:,:),1,0,smi_t_pos(5));

%% A>U
ClustStrucNeg.LatFullTime = cluster_results_duplicates(LatTables(1,:,:),0,0,lat_t_neg(1));
ClustStrucNeg.LattROI1 = cluster_results_duplicates(LatTables(2,:,:),0,0,lat_t_neg(2));
ClustStrucNeg.LattROI2 = cluster_results_duplicates(LatTables(3,:,:),0,0,lat_t_neg(3));
ClustStrucNeg.LattROI3 = cluster_results_duplicates(LatTables(4,:,:),0,0,lat_t_neg(4));
ClustStrucNeg.LattROI4 = cluster_results_duplicates(LatTables(5,:,:),0,0,lat_t_neg(5));

ClustStrucNeg.SmiFullTime = cluster_results_duplicates(SmiTables(1,:,:),0,0,smi_t_neg(1));
ClustStrucNeg.SmitROI1 = cluster_results_duplicates(SmiTables(2,:,:),0,0,smi_t_neg(2));
ClustStrucNeg.SmitROI2 = cluster_results_duplicates(SmiTables(3,:,:),0,0,smi_t_neg(3));
ClustStrucNeg.SmitROI3 = cluster_results_duplicates(SmiTables(4,:,:),0,0,smi_t_neg(4));
ClustStrucNeg.SmitROI4 = cluster_results_duplicates(SmiTables(5,:,:),0,0,smi_t_neg(5));


%% Overlaps pos
ClustStrucOLPos.FullTime = cluster_results_overlap(LatTables(1,:,:),SmiTables(1,:,:),0,0,lat_t_pos(1),smi_t_pos(1));
ClustStrucOLPos.tROI1 = cluster_results_overlap(LatTables(2,:,:),SmiTables(2,:,:),0,0,lat_t_pos(2),smi_t_pos(2));
ClustStrucOLPos.tROI2 = cluster_results_overlap(LatTables(3,:,:),SmiTables(3,:,:),0,0,lat_t_pos(3),smi_t_pos(3));
ClustStrucOLPos.tROI3 = cluster_results_overlap(LatTables(4,:,:),SmiTables(4,:,:),0,0,lat_t_pos(4),smi_t_pos(4));
ClustStrucOLPos.tROI4 = cluster_results_overlap(LatTables(5,:,:),SmiTables(5,:,:),0,0,lat_t_pos(5),smi_t_pos(5));
%% Overlaps neg
ClustStrucOLNeg.FullTime = cluster_results_overlap(LatTables(1,:,:),SmiTables(1,:,:),1,0,lat_t_neg(1),smi_t_neg(1));
ClustStrucOLNeg.tROI1 = cluster_results_overlap(LatTables(2,:,:),SmiTables(2,:,:),1,0,lat_t_neg(2),smi_t_neg(2));
ClustStrucOLNeg.tROI2 = cluster_results_overlap(LatTables(3,:,:),SmiTables(3,:,:),1,0,lat_t_neg(3),smi_t_neg(3));
ClustStrucOLNeg.tROI3 = cluster_results_overlap(LatTables(4,:,:),SmiTables(4,:,:),1,0,lat_t_neg(4),smi_t_neg(4));
ClustStrucOLNeg.tROI4 = cluster_results_overlap(LatTables(5,:,:),SmiTables(5,:,:),1,0,lat_t_neg(5),smi_t_neg(5));
