% ClustStrucNeg.LattROI2(2:end) = [];

addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/')
addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/template/atlas/aal/')
ft_defaults 
atlas = ft_read_atlas('ROI_MNI_V4.nii');


data=[];
data = tmp(tmp(:,4)>0,:);
data = data(data(:,4)>=myT(7),:);
% for i = 1:length(ClustStrucNeg.LattROI2)
%     data = [data;ClustStrucNeg.LattROI2(i).data];
% end
centroid_x = mean(data(:,1));
centroid_y = mean(data(:,2));
centroid_z = mean(data(:,3));
centroid = [centroid_x,centroid_y,centroid_z];
m = mean(data(:,4));
mx = max(abs(data(:,4)));
lorMaxVox = data(data(:,4)==max(data(:,4)),1:3);    

tmpStruc.side = 'L';
tmpStruc.lorMaxAAL = get_label(lorMaxVox,atlas);
tmpStruc.tClust_Max = [char(num2str(m)) ' (' char(num2str(mx)) ')'];
tmpStruc.nVoxels = length(data);
tmpStruc.tX = lorMaxVox(1);
tmpStruc.tY = lorMaxVox(2);
tmpStruc.tZ = lorMaxVox(3);
tmpStruc.cX = centroid_x;
tmpStruc.cY = centroid_y;
tmpStruc.cZ = centroid_z;

% tmpStruc.data = data;
tmpStruc.lorMaxVox = lorMaxVox;
tmpStruc.tMaxAAL = get_label(lorMaxVox,atlas); 
tmpStruc.VoxDist = sqrt(sum((centroid-lorMaxVox).^2));

GiantStruct{1,7} = tmpStruc;