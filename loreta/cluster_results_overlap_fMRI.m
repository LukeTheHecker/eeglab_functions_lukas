function ClustStruc=cluster_results_overlap_fMRI(tablename1,tablename2, posOrNeg, clustCorrect, t_crit)
%% example: ClustStruc = cluster_results(LattROI1p05, 1, 1)
% cluster the voxels in LattROI1p05, look only at positive (U>A) effects
% and correc cluster values for multiple comparisons
addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/')
ft_defaults 

if posOrNeg == 0
    tablename1(find(tablename1(:,4)<0),:) = [];
    tablename2(find(tablename2(:,4)<0),:) = [];
elseif posOrNeg == 1
    tablename1(find(tablename1(:,4)>0),:) = [];
    tablename2(find(tablename2(:,4)>0),:) = [];
    tablename1(:,4) = abs(tablename1(:,4));
    tablename2(:,4) = abs(tablename2(:,4));
end

if t_crit ~= 0
    tablename1(find(abs(tablename1(:,4))<t_crit),:) = [];
    tablename2(find(abs(tablename2(:,4))<t_crit),:) = [];
end


[~,idx_A,idx_B] = intersect(tablename1(:,1:3),tablename2(:,1:3),'rows');
myLocsAct_A = tablename1(idx_A,:);
myLocsAct_B = tablename2(idx_B,:);
myLocsAct = myLocsAct_A;
myLocsAct(:,4) = (myLocsAct_A(:,4)+myLocsAct_B(:,4))./2 ;





%% Duplicate entries based on their t value
sub_t = min(myLocsAct(:,4));
myLocsAct(:,4) =myLocsAct(:,4)-sub_t+1; 
initial_length=length(myLocsAct(:,1));
for i = 1:initial_length
    if myLocsAct(i,4) >= 1.5
        for k = 1:round(myLocsAct(i,4))
            myLocsAct(end+1,:) = myLocsAct(i,:);
        end
    end
end
myLocsAct(:,4) = myLocsAct(:,4)+sub_t-1;
       
if initial_length<=4
    ClustStruc=[];
    return
end
% %% if too few voxels available:
% if length(myLocsAct_Ori)<=20
%     ClustStruc(1).name='Cls 1';
%     ClustStruc(1).data=myLocsAct(:,1:3);
%     ClustStruc(1).nVoxels=length(myLocsAct(:,1));
%     ClustStruc(1).centroid = [mean(myLocsAct(:,1));mean(myLocsAct(:,2));mean(myLocsAct(:,3))];
%     tmpDiff=ClustStruc(1).data-ClustStruc(1).centroid;
%     distArr=NaN(1,ClustStruc(1).nVoxels);
%     for i=1:ClustStruc(1).nVoxels 
%         distArr(i)=sqrt(sum(tmpDiff(i,:).^2));
%     end
%     indx=find(distArr==min(distArr));
%     ClustStruc(1).centroidOri = ClustStruc(1).data(indx(1),:);
%     for g = 1:length(myLocsAct_Ori)
%         if tablename{g,[1 2 3]} == ClustStruc(k).centroidOri
%             ClustStruc(1).centrBrodmann = tablename{g,8};
%             ClustStruc(1).centrLobe = char(tablename{g,9});
%             ClustStruc(1).centrStructure= char(tablename{g,10});
%         end
%     end
%     tVals=myLocsAct(:,4);
%     maxVox=myLocsAct(find(tVals==max(tVals)),1:3);
%     ClustStruc(1).lorMaxVox=maxVox(1,:);
%     tmp=myLocsAct(find(tVals==max(tVals)),1:4);
%     tmp(:,4) = tmp(:,4)+sub_t-1;
%     for h = 1:length(myLocsAct_Ori)
%         if tablename{h,[1 2 3 7]} == tmp(1,:)
%             ClustStruc(1).lorMaxBrodmann=tablename{h,8};
%             ClustStruc(1).lorMaxLobe=char(tablename{h,9});
%             ClustStruc(1).lorMaxStructure=char(tablename{h,10});
%             break
%         end
%     end
%     ClustStruc(1).VoxDist = sqrt(sum([ClustStruc(k).lorMaxVox-ClustStruc(k).centroid].^2));
%     return 
% end
     
% plot all data
figure;
scatter3(myLocsAct(:,1),myLocsAct(:,2), myLocsAct(:,3))
xlabel('X')
ylabel('Y')
zlabel('Z')
rotate3d
axis vis3d
title('All significant Voxels')
    

%% kmeans
cidx=[];
cmeans=[];
silh=[];
sizeFit=zeros(1,8);
% find out good cluster size based on mean distance
for i=2:8
    try
        [cidx,cmeans] = kmeans(myLocsAct(:,1:3),i,'dist','sqeuclidean', 'replicates',100);
        silh = silhouette(myLocsAct(:,1:3),cidx,'sqeuclidean');
        fprintf('%d clusters = %d', i, mean(silh(i)))
        fprintf('\n')
        sizeFit(i-1)=mean(silh(i));
    catch
        disp('too few data points')
    end
end
nClusts=find(sizeFit==max(sizeFit))+1;
fprintf('Choosing %d Clusters', nClusts)
[cidx,cmeans] = kmeans(myLocsAct(:,1:3),nClusts,'dist','sqeuclidean', 'replicates',100);
silh = silhouette(myLocsAct(:,1:3),cidx,'sqeuclidean');

if clustCorrect == 1
    tcrit = abs(tinv(1-(1-(0.05/nClusts)),20));
    selection(find(abs(selection(:,4))<tcrit),:) = [];
    myLocsAct(find(abs(myLocsAct(:,4))<tcrit),:) = [];
end

figure;

ptsymb = {'bs','r^','md','go','c+', 'ko','y*', 'k^',};
for i = 1:nClusts
    clust = find(cidx==i);
    plot3(myLocsAct(clust,1),myLocsAct(clust,2),myLocsAct(clust,3),ptsymb{i});
    hold on
end
plot3(cmeans(:,1),cmeans(:,2),cmeans(:,3),'ko');
plot3(cmeans(:,1),cmeans(:,2),cmeans(:,3),'kx');
hold off
xlabel('Sepal Length');
ylabel('Sepal Width');
zlabel('Petal Length');
view(-137,10);
rotate3d
grid on
axis vis3d
title('Clusters with all Significant Voxels')
[myLocsAct,IA,IC] = unique(myLocsAct,'rows');
cidx = cidx(IA);
% sort 'myLocs' into spereate array clusters
for k = 1:nClusts
    disp(k)
    ClustStruc(k).data=myLocsAct(find(cidx==k),1:4);
    ClustStruc(k).nVoxels=length(myLocsAct(cidx==k));
    if length(find(ClustStruc(k).data(:,1)<0)) == 0
        side = 'R';
    elseif length(find(ClustStruc(k).data(:,1)>0)) == 0
        side = 'L';
    else
        side = 'L/R';
    end
    ClustStruc(k).side = side;
    ClustStruc(k).centroid = cmeans(k,:);
    ClustStruc(k).cX = round(ClustStruc(k).centroid(1));
    ClustStruc(k).cY = round(ClustStruc(k).centroid(2));
    ClustStruc(k).cZ = round(ClustStruc(k).centroid(3));
%     for g = 1:height(tablename)
%         if tablename{g,[1 2 3]} == ClustStruc(k).centroidOri
%             ClustStruc(k).centrBrodmann = tablename{g,8};
%             ClustStruc(k).centrLobe = char(tablename{g,9});
%             ClustStruc(k).centrStructure= char(tablename{g,10});
%         end
%     end
    ClustStruc(k).centrAAL = get_label(ClustStruc(k).centroid);
    tVals=ClustStruc(k).data(:,4);
    tMax = max(ClustStruc(k).data(:,4));
    tClust = mean(ClustStruc(k).data(:,4));
    ClustStruc(k).tClust_Max = [num2str(round(tClust,2)), ' (', num2str(round(tMax,2)), ')'];
    ClustStruc(k).lorMaxVox=ClustStruc(k).data(find(ClustStruc(k).data(:,4)==tMax),1:3);
    ClustStruc(k).tX = ClustStruc(k).lorMaxVox(1);
    ClustStruc(k).tY = ClustStruc(k).lorMaxVox(2);
    ClustStruc(k).tZ = ClustStruc(k).lorMaxVox(3);
%     for h = 1:height(tablename)
%         if tablename{h,[1 2 3]} == ClustStruc(k).lorMaxVox
%             ClustStruc(k).lorMaxBrodmann=tablename{h,8};
%             ClustStruc(k).lorMaxLobe=char(tablename{h,9});
%             ClustStruc(k).lorMaxStructure=char(tablename{h,10});
%             break
%         end
%     end
    ClustStruc(k).tMaxAAL = get_label(ClustStruc(k).centroid);
    tmpDiff=ClustStruc(k).data(:,1:3)-cmeans(k,:);
    distArr=NaN(1,ClustStruc(k).nVoxels);
    for i=1:ClustStruc(k).nVoxels 
        distArr(i)=sqrt(sum(tmpDiff(i,:).^2));
    end
    indx=find(distArr==min(distArr));
    ClustStruc(k).centroidOri = ClustStruc(k).data(indx(1),1:3);
    ClustStruc(k).tMax = tMax;
    ClustStruc(k).tClust = tClust;

    ClustStruc(k).VoxDist = sqrt(sum([ClustStruc(k).lorMaxVox-ClustStruc(k).centroid].^2));
end
end