% matrix with condition X ROI X Subject X pnts
lat_amb = LatAmbsLorRoiLogSubjW{22:end,:};
lat_unamb = LatUnambsLorRoiLogSubjW{22:end,:};
smi_amb = SmiAmbsLorRoiLogSubjW{22:end,:};
smi_unamb = SmiUnambsLorRoiLogSubjW{22:end,:};

lat_amb = reshape(lat_amb, [21,10,351]);
lat_unamb = reshape(lat_unamb, [21,10,351]);
smi_amb = reshape(smi_amb, [21,10,351]);
smi_unamb = reshape(smi_unamb, [21,10,351]);

ROI_names = ["Insula R","Insula L","Fusiform L","Hippocampus L",...
    "Cingulum Ant L","Pallidum L","Cingulum Mid R","Cingulum Post L",...
    "Parietal Inf R","Occipital Sup R"];

xrange=-400:4:1000;

%% BLC by subtracting -400:-100
for a = 1:21
    for b = 1:10
        lat_amb_blc(a,b,:) = lat_amb(a,b,:)/mean(squeeze(lat_amb(a,b,86:111)));
        lat_unamb_blc(a,b,:) = lat_unamb(a,b,:)/mean(squeeze(lat_unamb(a,b,86:111)));
        smi_amb_blc(a,b,:) = smi_amb(a,b,:)/mean(squeeze(smi_amb(a,b,86:111)));
        smi_unamb_blc(a,b,:) = smi_unamb(a,b,:)/mean(squeeze(smi_unamb(a,b,86:111)));
    end
end

lat_diff = lat_unamb_blc-lat_amb_blc;
smi_diff = smi_unamb_blc-smi_amb_blc;

lat_diff_mean = squeeze(mean(lat_diff,1));
smi_diff_mean = squeeze(mean(smi_diff,1));
lat_diff_smooth = NaN(size(lat_diff_mean));
smi_diff_smooth = NaN(size(smi_diff_mean));

for h = 1:10
    lat_diff_smooth(h,:) = movmean(lat_diff_mean(h,:),10);
    smi_diff_smooth(h,:) = movmean(smi_diff_mean(h,:),10);
end
    
figure;  
peakrange = [101;251]; %(160-240)
peaks = NaN(2,10);
for i = 1:10
    subplot(5,2,i)
    plot(xrange, lat_diff_smooth(i,:),'k')
    hold on
    plot(xrange, smi_diff_smooth(i,:),'r')
    hold on
    plot([-400 1000],[0 0],'--k')
    hold on
    [pks, locs] = findpeaks(lat_diff_smooth(i,peakrange(1):peakrange(2)));
    [~,idx]=max(pks);
    plot(xrange(peakrange(1)+locs(idx)-1), pks(idx), 'kd','MarkerSize',10)
    peaks(1,i) = xrange(peakrange(1)+locs(idx)-1);
    hold on
    [pks, locs] = findpeaks(smi_diff_smooth(i,peakrange(1):peakrange(2)));
    [~,idx]=max(pks);
    plot(xrange(peakrange(1)+locs(idx)-1), pks(idx), 'rd','MarkerSize',10)
    peaks(2,i) = xrange(peakrange(1)+locs(idx)-1);
    title(sprintf('%s',ROI_names(i)))
    legend('Lattice Diff','Smiley Diff')
end

figure;
plot(1:10, peaks(1,:))
hold on
plot(1:10, peaks(2,:))
legend('lat','smi')
xticklabels(ROI_names)
xtickangle(45)


% condition: 1=Amb; 2=Unamb
big_mat_blc = NaN(size(big_mat));

%% plot individual subjects
figure;
for k = 1:21
    plot(xrange, squeeze(big_mat_blc(2,2,k,:)))
    hold on
end

%% statistics
stats = NaN(8,710);
for j = 1:8
    for k = 1:710
        stats(j,k) = signrank(squeeze(big_mat_blc(1,j,:,k)),squeeze(big_mat_blc(2,j,:,k)));
    end
end
stats_sig = stats;
stats_sig(stats_sig>=0.05) = NaN;
stats_sig(~isnan(stats_sig)) = 0.01;

%% plot mean diffs
diffs = squeeze(big_mat_blc(2,:,:,:)-big_mat_blc(1,:,:,:));
figure;
for k = 1:8
    tmp_A = squeeze(mean(big_mat_blc(1,k,:,:),3));
    tmp_U = squeeze(mean(big_mat_blc(2,k,:,:),3));
    tmp_diff = squeeze(mean(diffs(k,:,:),2));
    
    subplot(8,1,k)
    
%     plot(xrange,tmp_A,'r')
%     hold on
%     plot(xrange,tmp_U,'k')
%     hold on
    plot(xrange,tmp_diff,'b')
    hold on
    plot([-1000,2000],[0,0])
    hold on
    % plot statistics
    scatter(xrange,stats_sig(k,:),'o', 'MarkerEdgeColor', 'r', 'MarkerFaceColor', 'r')
    
    xlim([-60 800])
    if k==1
        legend('Ambiguous','Unambiguous','Difference')
    end
    title(sprintf('%s',ROI_names(k)))
end
print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/LORETA/eLORETA_SNR49/fMRI Seeding/Results_LDvsLA_fMRI-ConjunctionSeed','-dpng','-r600');
