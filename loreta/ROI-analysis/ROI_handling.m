% matrix with condition X ROI X Subject X pnts
sublist = {AAsLorRoiLogSubjW, ACsLorRoiLogSubjW, ADsLorRoiLogSubjW,...
    AEsLorRoiLogSubjW, AFsLorRoiLogSubjW, AGsLorRoiLogSubjW,...
    AHsLorRoiLogSubjW, AIsLorRoiLogSubjW, AJsLorRoiLogSubjW,...
    AKsLorRoiLogSubjW, ALsLorRoiLogSubjW, AMsLorRoiLogSubjW,...
    ANsLorRoiLogSubjW, AOsLorRoiLogSubjW, APsLorRoiLogSubjW,...
    AQsLorRoiLogSubjW, ARsLorRoiLogSubjW, ASsLorRoiLogSubjW,...
    ATsLorRoiLogSubjW, AUsLorRoiLogSubjW, AVsLorRoiLogSubjW};

big_mat = NaN(2,8,21,710);
ROI_names = ["Frontal Mid Orb R","Occipital Inf L","Frontal Inf Tri R","Angular R","Parietal Sup L","Frontal Inf Tri L", "Temporal Inf R", "Frontal Mid R"];
xrange=-916:4:1920;
% condition: 1=Amb; 2=Unamb
for a = 2%
    % ROI: see ROI_names
    for b = 1:8
        for c = 1:21
                tmp_arr = table2array(sublist{c}).';
                big_mat(a,b,c,:) = tmp_arr(b,:);
        end
    end
end
big_mat_blc = NaN(size(big_mat));
%% BLC by subtracting -400:-100
for a = 1:2
    for b = 1:8
        for c = 1:21
            big_mat_blc(a,b,c,:) = big_mat(a,b,c,:)-mean(squeeze(big_mat(a,b,c,130:205)));
        end
    end
end

%% plot individual subjects
figure;
for k = 1:21
    plot(xrange, squeeze(big_mat_blc(2,2,k,:)))
    hold on
end

%% statistics
stats = NaN(8,710);
for j = 1:8
    for k = 1:710
        stats(j,k) = signrank(squeeze(big_mat_blc(1,j,:,k)),squeeze(big_mat_blc(2,j,:,k)));
    end
end
stats_sig = stats;
stats_sig(stats_sig>=0.05) = NaN;
stats_sig(~isnan(stats_sig)) = 0.01;

%% plot mean diffs
diffs = squeeze(big_mat_blc(2,:,:,:)-big_mat_blc(1,:,:,:));
figure;
for k = 1:8
    tmp_A = squeeze(mean(big_mat_blc(1,k,:,:),3));
    tmp_U = squeeze(mean(big_mat_blc(2,k,:,:),3));
    tmp_diff = squeeze(mean(diffs(k,:,:),2));
    
    subplot(8,1,k)
    
%     plot(xrange,tmp_A,'r')
%     hold on
%     plot(xrange,tmp_U,'k')
%     hold on
    plot(xrange,tmp_diff,'b')
    hold on
    plot([-1000,2000],[0,0])
    hold on
    % plot statistics
    scatter(xrange,stats_sig(k,:),'o', 'MarkerEdgeColor', 'r', 'MarkerFaceColor', 'r')
    
    xlim([-60 800])
    if k==1
        legend('Ambiguous','Unambiguous','Difference')
    end
    title(sprintf('%s',ROI_names(k)))
end
print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/LORETA/eLORETA_SNR49/fMRI Seeding/Results_LDvsLA_fMRI-ConjunctionSeed','-dpng','-r600');
