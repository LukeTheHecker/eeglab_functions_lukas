function myTable=slor_to_table(path, name)
addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/')
ft_defaults

source=loreta2fieldtrip([path name]);
myTable = NaN(source.dim(1)*source.dim(2)*source.dim(3),4);
cnt=1;
for a = 1:source.dim(1)
    for b = 1:source.dim(2)
        for c = 1:source.dim(3)
            if ~isempty(source.mom(a,b,c))
                tmp=(source.transform*[a b c 1]')';
                myTable(cnt,1:3) = tmp(1:3);
                myTable(cnt,4) = source.mom(a,b,c);
                cnt = cnt+1;
            end
        end
    end
end
myTable=myTable(~isnan(myTable(:,4)),:);


end