base_lat='LattROI';
base_smi='SmitROI';

%% t-stat, sw-normed, & BLC:
% at p<0.05 FWE two-sided
t_crit_two = [4.189,3.919,4.195,4.224,4.037...
    4.256,4.133,4.383,4.123,4.16];
% cluster-wise: not interested

%% U>A
ClustStrucPos.LatFullTime = cluster_results_duplicates(LatFullTime,1,0,t_crit_two(1));
ClustStrucPos.LattROI1 = cluster_results_duplicates(LattROI1,1,0,t_crit_two(2));
ClustStrucPos.LattROI2 = cluster_results_duplicates(LattROI2,1,0,t_crit_two(3));
ClustStrucPos.LattROI3 = cluster_results_duplicates(LattROI3,1,0,t_crit_two(4));
ClustStrucPos.LattROI4 = cluster_results_duplicates(LattROI4,1,0,t_crit_two(5));

ClustStrucPos.SmiFullTime = cluster_results_duplicates(SmiFullTime,1,0,t_crit_two(6));
ClustStrucPos.SmitROI1 = cluster_results_duplicates(SmitROI1,1,0,t_crit_two(7));
ClustStrucPos.SmitROI2 = cluster_results_duplicates(SmitROI2,1,0,t_crit_two(8));
ClustStrucPos.SmitROI3 = cluster_results_duplicates(SmitROI3,1,0,t_crit_two(9));
ClustStrucPos.SmitROI4 = cluster_results_duplicates(SmitROI4,1,0,t_crit_two(10));

%% A>U
ClustStrucNeg.LatFullTime = cluster_results_duplicates(LatFullTime,0,0,t_crit_two(1));
ClustStrucNeg.LattROI1 = cluster_results_duplicates(LattROI1,0,0,t_crit_two(2));
ClustStrucNeg.LattROI2 = cluster_results_duplicates(LattROI2,0,0,t_crit_two(3));
ClustStrucNeg.LattROI3 = cluster_results_duplicates(LattROI3,0,0,t_crit_two(4));
ClustStrucNeg.LattROI4 = cluster_results_duplicates(LattROI4,0,0,t_crit_two(5));

ClustStrucNeg.SmiFullTime = cluster_results_duplicates(SmiFullTime,0,0,t_crit_two(6));
ClustStrucNeg.SmitROI1 = cluster_results_duplicates(SmitROI1,0,0,t_crit_two(7));
ClustStrucNeg.SmitROI2 = cluster_results_duplicates(SmitROI2,0,0,t_crit_two(8));
ClustStrucNeg.SmitROI3 = cluster_results_duplicates(SmitROI3,0,0,t_crit_two(9));
ClustStrucNeg.SmitROI4 = cluster_results_duplicates(SmitROI4,0,0,t_crit_two(10));


%% Overlaps pos
ClustStrucOLPos.FullTime = cluster_results_overlap(LatFullTime,SmiFullTime,0,0,t_crit_two(1),t_crit_two(6));
ClustStrucOLPos.tROI1 = cluster_results_overlap(LattROI1,SmitROI1,0,0,t_crit_two(2),t_crit_two(7));
ClustStrucOLPos.tROI2 = cluster_results_overlap(LattROI2,SmitROI2,0,0,t_crit_two(3),t_crit_two(8));
ClustStrucOLPos.tROI3 = cluster_results_overlap(LattROI3,SmitROI3,0,0,t_crit_two(4),t_crit_two(9));
ClustStrucOLPos.tROI4 = cluster_results_overlap(LattROI4,SmitROI4,0,0,t_crit_two(5),t_crit_two(10));
%% Overlaps neg
ClustStrucOLNeg.FullTime = cluster_results_overlap(LatFullTime,SmiFullTime,1,0,t_crit_two(1),t_crit_two(6));
ClustStrucOLNeg.tROI1 = cluster_results_overlap(LattROI1,SmitROI1,1,0,t_crit_two(2),t_crit_two(7));
ClustStrucOLNeg.tROI2 = cluster_results_overlap(LattROI2,SmitROI2,1,0,t_crit_two(3),t_crit_two(8));
ClustStrucOLNeg.tROI3 = cluster_results_overlap(LattROI3,SmitROI3,1,0,t_crit_two(4),t_crit_two(9));
ClustStrucOLNeg.tROI4 = cluster_results_overlap(LattROI4,SmitROI4,1,0,t_crit_two(5),t_crit_two(10));

%% Cluster Peak-Data (P200 P400 stab-stab / rev-rev)
ClustStrucStabStabP200Pos = cluster_results_duplicates(LDS_LAS_P200,1,0,t_LDS_LAS_P200_pos);
ClustStrucStabStabP400Pos = cluster_results_duplicates(LDS_LAS_P400,1,0,t_LDS_LAS_P400_pos);
ClustStrucRevRevP200Pos = cluster_results_duplicates(LDR_LAR_P200,1,0,t_LDR_LAR_P200_pos);
ClustStrucRevRevP400Pos = cluster_results_duplicates(LDR_LAR_P400,1,0,t_LDR_LAR_P400_pos);

ClustStrucStabStabP200Neg = cluster_results_duplicates(LDS_LAS_P200,0,0,t_LDS_LAS_P200_neg);
ClustStrucStabStabP400Neg = cluster_results_duplicates(LDS_LAS_P400,0,0,t_LDS_LAS_P400_neg);
ClustStrucRevRevP200Neg = cluster_results_duplicates(LDR_LAR_P200,0,0,t_LDR_LAR_P200_neg);
ClustStrucRevRevP400Neg = cluster_results_duplicates(LDR_LAR_P400,0,0,t_LDR_LAR_P400_neg);
%Smileys
ClustStrucStabStabP200Pos = cluster_results_duplicates(SDS_SAS_P200,1,0,t_SDS_SAS_P200_pos);
ClustStrucStabStabP400Pos = cluster_results_duplicates(SDS_SAS_P400,1,0,t_SDS_SAS_P400_pos);
ClustStrucRevRevP200Pos = cluster_results_duplicates(SDR_SAR_P200,1,0,t_SDR_SAR_P200_pos);
ClustStrucRevRevP400Pos = cluster_results_duplicates(SDR_SAR_P400,1,0,t_SDR_SAR_P400_pos);

ClustStrucStabStabP200Neg = cluster_results_duplicates(SDS_SAS_P200,0,0,t_SDS_SAS_P200_neg);
ClustStrucStabStabP400Neg = cluster_results_duplicates(SDS_SAS_P400,0,0,t_SDS_SAS_P400_neg);
ClustStrucRevRevP200Neg = cluster_results_duplicates(SDR_SAR_P200,0,0,t_SDR_SAR_P200_neg);
ClustStrucRevRevP400Neg = cluster_results_duplicates(SDR_SAR_P400,0,0,t_SDR_SAR_P400_neg);
% Conjunction
%U-stab vs A-stab
ClustStrucConjStabStabP200Pos = cluster_results_overlap(LDS_LAS_P200,SDS_SAS_P200,0,0,t_LDS_LAS_P200_pos,t_SDS_SAS_P200_pos);
ClustStrucConjStabStabP400Pos = cluster_results_overlap(LDS_LAS_P400,SDS_SAS_P400,0,0,t_LDS_LAS_P400_pos,t_SDS_SAS_P400_pos);

ClustStrucConjStabStabP200Neg = cluster_results_overlap(LDS_LAS_P200,SDS_SAS_P200,1,0,t_LDS_LAS_P200_neg,t_SDS_SAS_P200_neg);
ClustStrucConjStabStabP400Neg = cluster_results_overlap(LDS_LAS_P400,SDS_SAS_P400,1,0,t_LDS_LAS_P400_neg,t_SDS_SAS_P400_neg);
%U-rev vs A-rev
ClustStrucConjRevRevP200Pos = cluster_results_overlap(LDR_LAR_P200,SDR_SAR_P200,0,0,t_LDR_LAR_P200_pos,t_SDR_SAR_P200_pos);
ClustStrucConjRevRevP400Pos = cluster_results_overlap(LDR_LAR_P400,SDR_SAR_P400,0,0,t_LDR_LAR_P400_pos,t_SDR_SAR_P400_pos);

ClustStrucConjRevRevP200Neg = cluster_results_overlap(LDR_LAR_P200,SDR_SAR_P200,1,0,t_LDR_LAR_P200_neg,t_SDR_SAR_P200_neg);
ClustStrucConjRevRevP400Neg = cluster_results_overlap(LDR_LAR_P400,SDR_SAR_P400,1,0,t_LDR_LAR_P400_neg,t_SDR_SAR_P400_neg);


%% Cluster fMRI overlaps
path = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/group_fullfactorial/';
file1 = 'spmT_0009.nii';
file2 = 'spmT_0011.nii';
% 0.05 FWE corrected:
t_crit = 4.9;
% 0.001 uncorrected:
t_crit_uc = 3.23;

[source1] = ft_read_mri([path file1]);
[source2] = ft_read_mri([path file2]);
myTable1 = NaN(source1.dim(1)*source1.dim(2)*source1.dim(3),4);
% get table-like results [x y z t]
cnt=1;
for a = 1:source1.dim(1)
    for b = 1:source1.dim(2)
        for c = 1:source1.dim(3)
            tmp=(source1.transform*[a b c 1]')';
            myTable1(cnt,1:3) = tmp(1:3);
            myTable1(cnt,4) = source1.anatomy(a,b,c);
            cnt=cnt+1;
        end
    end
end
myTable2 = NaN(source2.dim(1)*source2.dim(2)*source2.dim(3),4);
% get table-like results [x y z t]
cnt=1;
for a = 1:source2.dim(1)
    for b = 1:source2.dim(2)
        for c = 1:source2.dim(3)
            tmp=(source2.transform*[a b c 1]')';
            myTable2(cnt,1:3) = tmp(1:3);
            myTable2(cnt,4) = source2.anatomy(a,b,c);
            cnt=cnt+1;
        end
    end
end

ClustStrucFMRI.UA_OL_UC = cluster_results_overlap_fMRI(myTable1,myTable2,0,0,t_crit_uc);
ClustStrucFMRI.UA_OL_FWE = cluster_results_overlap_fMRI(myTable1,myTable2,0,0,t_crit);
