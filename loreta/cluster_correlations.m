function ClustStruc=cluster_results_overlap(tablename1)
%% example: ClustStruc = cluster_results(LattROI1p05, 1, 1)
% cluster the voxels in LattROI1p05, look only at positive (U>A) effects
% and correc cluster values for multiple comparisons
addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/')
ft_defaults 
addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/template/atlas/aal/')

myLocsAct = tablename1;

      
if length(myLocsAct)<=4
    ClustStruc=[];
    return
end

%% kmeans_opt.m
[cidx,cmeans] = kmeans_opt(myLocsAct(:,1:3),8,0.9,100);
nClusts = max(cidx);
fprintf('Choosing %d Clusters\n', nClusts)

figure;

ptsymb = {'bs','r^','md','go','c+', 'ko','y*', 'k^',};
for i = 1:nClusts
    clust = find(cidx==i);
    plot3(myLocsAct(clust,1),myLocsAct(clust,2),myLocsAct(clust,3),ptsymb{i});
    hold on
end
plot3(cmeans(:,1),cmeans(:,2),cmeans(:,3),'ko');
plot3(cmeans(:,1),cmeans(:,2),cmeans(:,3),'kx');
hold off
xlabel('Sepal Length');
ylabel('Sepal Width');
zlabel('Petal Length');
view(-137,10);
rotate3d
grid on
axis vis3d
title('Clusters with all Significant Voxels')

[myLocsAct,IA,IC] = unique(myLocsAct,'rows');
cidx = cidx(IA);
atlas = ft_read_atlas('ROI_MNI_V4.nii');

for k = 1:nClusts
    disp(k)
    data = myLocsAct(find(cidx==k),1:4);
    centroid = cmeans(k,:);
    tMax = max(abs(data(:,4)));
    tClust = mean(data(:,4));
    lorMaxVox=data(find(data(:,4)==tMax),1:3);
    if length(find(data(:,1)<0)) == 0
        side = 'R';
    elseif length(find(data(:,1)>0)) == 0
        side = 'L';
    else
        side = 'L/R';
    end
    ClustStruc(k).side = side;
    ClustStruc(k).lorMaxAAL = get_label(lorMaxVox,atlas);
    ClustStruc(k).tClust_Max = [num2str(round(tClust,2)), ' (', num2str(round(tMax,2)), ')'];
    ClustStruc(k).nVoxels=length(myLocsAct(cidx==k));
    ClustStruc(k).tX = lorMaxVox(1);
    ClustStruc(k).tY = lorMaxVox(2);
    ClustStruc(k).tZ = lorMaxVox(3);
    ClustStruc(k).cX = round(centroid(1));
    ClustStruc(k).cY = round(centroid(2));
    ClustStruc(k).cZ = round(centroid(3));
end

end