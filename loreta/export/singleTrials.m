%% single trials
for i = 1:21
    % For Baseline
%     EEG_tmp = pop_loadset(ALLEEG(i).filename,ALLEEG(i).filepath);
%     EEG_A = pop_epoch(EEG_tmp,{'amb_reversal' 'amb_stability'},[-0.3 -0.1]);
%     EEG_U = pop_epoch(EEG_tmp,{'unamb_reversal' 'unamb_stability'},[-0.3 -0.1]);
%     mydir = ['/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/CRSS/SA/BL/' ALLEEG(i).subject '/'];
%     cd(mydir)
%     for k = 1:EEG_A.trials
%         eeglab2loreta( ALLEEG(i).chanlocs,EEG_A.data(:,:,k), 'exporterp','on','filecomp', sprintf('%s_SA_%d',ALLEEG(i).subject,k));
%     end
%     mydir = ['/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/CRSS/SD/BL/' ALLEEG(i).subject '/'];
%     cd(mydir)
%     for k = 1:EEG_U.trials
%         eeglab2loreta( ALLEEG(i).chanlocs,EEG_U.data(:,:,k), 'exporterp','on','filecomp', sprintf('%s_SD_%d',ALLEEG(i).subject,k));
%     end
    
    % For Trial
    EEG_A = pop_epoch(EEG_tmp,{'amb_reversal' 'amb_stability'},[0.04 0.8]);
    EEG_U = pop_epoch(EEG_tmp,{'unamb_reversal' 'unamb_stability'},[0.04 0.8]);
    mydir = ['/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/CRSS/LA/Trial/' ALLEEG(i).subject '/'];
    cd(mydir)
    for k = 1:EEG_A.trials
        eeglab2loreta( ALLEEG(i).chanlocs,EEG_A.data(:,:,k), 'exporterp','on','filecomp', sprintf('%s_LA_%d',ALLEEG(i).subject,k));
    end
    mydir = ['/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/CRSS/LD/Trial/' ALLEEG(i).subject '/'];
    cd(mydir)
    for k = 1:EEG_U.trials
        eeglab2loreta( ALLEEG(i).chanlocs,EEG_U.data(:,:,k), 'exporterp','on','filecomp', sprintf('%s_LD_%d',ALLEEG(i).subject,k));
    end
end
% %% concat. trials -_-_-> doesnt work with loreta key!
% for i = 1:21
%     EEG_tmp = pop_loadset(ALLEEG(i).filename,ALLEEG(i).filepath);
%     EEG_A = pop_epoch(EEG_tmp,{'amb_reversal' 'amb_stability'},[-1 1.99600000000000]);
%     EEG_U = pop_epoch(EEG_tmp,{'unamb_reversal' 'unamb_stability'},[-1 1.99600000000000]);
%     mydir = ['/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/CRSS/LA/'];
%     cd(mydir)
%     tmp_A = reshape(EEG_A.data,[EEG_A.nbchan,EEG_A.trials*EEG_A.pnts]);
%     tmp_U = reshape(EEG_U.data,[EEG_U.nbchan,EEG_U.trials*EEG_U.pnts]);
%     eeglab2loreta( ALLEEG(i).chanlocs,tmp_A, 'exporterp','on','filecomp', sprintf('%s_LA',ALLEEG(i).subject));
%     mydir = ['/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/CRSS/LD/'];
%     cd(mydir)
%     eeglab2loreta( ALLEEG(i).chanlocs,tmp_U, 'exporterp','on','filecomp', sprintf('%s_LD',ALLEEG(i).subject));
% 
% end



cd '/Applications/eeglab14_1_2b'

for i = 1:21
    EEG_tmp = pop_loadset(ALLEEG(i).filename,ALLEEG(i).filepath);
    EEG_A = pop_epoch(EEG_tmp,{'amb_reversal' 'amb_stability'},[-0.4 1]);
    EEG_U = pop_epoch(EEG_tmp,{'unamb_reversal' 'unamb_stability'},[-0.4 1]);
    mydir = ['/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/CRSS/SA/' ALLEEG(i).subject '/'];
    cd(mydir)
    for k = 1:EEG_A.trials
        eeglab2loreta( ALLEEG(i).chanlocs,EEG_A.data(:,:,k), 'exporterp','on','filecomp', sprintf('%s_SA_%d',ALLEEG(i).subject,k));
    end
    mydir = ['/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/CRSS/SD/' ALLEEG(i).subject '/'];
    cd(mydir)
    for k = 1:EEG_U.trials
        eeglab2loreta( ALLEEG(i).chanlocs,EEG_U.data(:,:,k), 'exporterp','on','filecomp', sprintf('%s_SD_%d',ALLEEG(i).subject,k));
    end

end

cd '/Applications/eeglab14_1_2b'