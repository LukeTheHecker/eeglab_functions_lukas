%% Get Peaks and the resulting time ranges /Timeframe ranges of each subject
% and export lat + smi stab/rev to loreta

namestruc = dir(['/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/processed/UA_Lattices_01_120_CL_AR100_SASICA/SASICA/*.set']);
% namestruc = dir(['/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/processed/UA_Smileys_01_120_CL_AR100_SASICA/SASICA/*.set']);
[STUDY ALLEEG] = pop_loadstudy('filename', 'allSets.study', 'filepath', namestruc(1).folder);
STUDY = std_makedesign(STUDY, ALLEEG, 1, 'variable1','type','variable2','','name','STUDY.design 1','pairing1','on','pairing2','on','delfiles','off','defaultdesign','off','values1',{'amb_reversal' 'amb_stability' 'unamb_reversal' 'unamb_stability'},'subjselect',{'AA' 'AC' 'AD' 'AE' 'AF' 'AG' 'AH' 'AI' 'AJ' 'AK' 'AL' 'AM' 'AN' 'AO' 'AP' 'AQ' 'AR' 'AS' 'AT' 'AU' 'AV'});

[STUDY erpdata erptimes pgroup pcond pinter] = std_erpplot(STUDY, ALLEEG);
%% Conditions:
STUDY.design.variable(1).value
amb_rev = permute(erpdata{1},[3,2,1]);
amb_stab = permute(erpdata{2},[3,2,1]);
unamb_rev = permute(erpdata{3},[3,2,1]);
unamb_stab = permute(erpdata{4},[3,2,1]);

%% who has no reversals? Reduce data! Lattice: AN(13), AV(21)
sublist = ["AA", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM","AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU","AV"];
% sublist = ["AA", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AO", "AP", "AQ", "AR", "AS", "AT", "AU"];

amb_stab = amb_stab([1:12, 14:20], :, :);
unamb_stab = unamb_stab([1:12, 14:20], :, :);
unamb_rev = unamb_rev([1:12, 14:20], :, :);

%% Put all conditions and subjects in one variable for ease of use
allInOne = NaN(4,length(sublist),32,length(amb_rev(1,1,:)));
allInOne(1,:,:,:) = amb_rev;
allInOne(2,:,:,:) = amb_stab;
allInOne(3,:,:,:) = unamb_rev;
allInOne(4,:,:,:) = unamb_stab;

SDS_p2 = squeeze(mean(unamb_stab(:,:,xrange(1):xrange(2)),1));
SAS_p2 = squeeze(mean(amb_stab(:,:,xrange(1):xrange(2)),1));
SDS_p4 = squeeze(mean(unamb_stab(:,:,xrange(3):xrange(4)),1));
SAS_p4 = squeeze(mean(amb_stab(:,:,xrange(3):xrange(4)),1));

SDR_p2 = squeeze(mean(unamb_rev(:,:,xrange(1):xrange(2)),1));
SAR_p2 = squeeze(mean(amb_rev(:,:,xrange(1):xrange(2)),1));
SDR_p4 = squeeze(mean(unamb_rev(:,:,xrange(3):xrange(4)),1));
SAR_p4 = squeeze(mean(amb_rev(:,:,xrange(3):xrange(4)),1));


%% Definitions
% Timerange to look for components
comp_range = [150:300,...
    300:600];
% Pntrange to look for components
xrange = [53,91;...
    91,166];

%% peaks = P200/P400; 4 conditions, 19 subjects
peaks_pnt = NaN(2,4,length(sublist)); % latency in timeframe of erptimes
peaks_latency = NaN(2,4,length(sublist));
peaks_amplitude = NaN(2,4,length(sublist));
% Find P200 peak latencies, amplitudes and TFs (pnts)
for b = 1:4
    for c = 1:21
        tmp = squeeze(allInOne(b,c,14,xrange(1):xrange(2)));
        [locs]=findpeaks(tmp);
        pks = tmp(locs.loc);
        [~,idx] = max(pks);
        peaks_amplitude(1,b,c) = pks(idx);
        peaks_pnt(1,b,c) = locs.loc(idx)+xrange(1)-1;
        peaks_latency(1,b,c) = erptimes(peaks_pnt(1,b,c));
    end
end
% Find P400 peak latencies, amplitudes and TFs (pnts)
for b = 1:4
    for c = 1:21
        tmp = squeeze(allInOne(b,c,14,xrange(3):xrange(4)));
        [locs]=findpeaks(tmp);
        pks = tmp(locs.loc);
        [~,idx] = max(pks);
        peaks_amplitude(2,b,c) = pks(idx);
        peaks_pnt(2,b,c) = locs.loc(idx)+xrange(3)-1;
        peaks_latency(2,b,c) = erptimes(peaks_pnt(2,b,c));
    end
end

%%Visualize Peaks
for i = 1:21
    figure
    
    plot(erptimes.', squeeze(amb_rev(i,14,:)))
    hold on
    plot(squeeze(peaks_latency(1,1,i)), squeeze(peaks_amplitude(1,1,i)), 'ro','HandleVisibility','off')
    hold on
    plot(squeeze(peaks_latency(2,1,i)), squeeze(peaks_amplitude(2,1,i)),'rx','HandleVisibility','off')
    hold on

    
    plot(erptimes.', squeeze(amb_stab(i,14,:)))
    hold on
    plot(squeeze(peaks_latency(1,2,i)), squeeze(peaks_amplitude(1,2,i)), 'ro','HandleVisibility','off')
    hold on
    plot(squeeze(peaks_latency(2,2,i)), squeeze(peaks_amplitude(2,2,i)),'rx','HandleVisibility','off')
    hold on
    
      
    plot(erptimes.', squeeze(unamb_rev(i,14,:)))
    hold on
    plot(squeeze(peaks_latency(1,3,i)), squeeze(peaks_amplitude(1,3,i)), 'ro','HandleVisibility','off')
    hold on
    plot(squeeze(peaks_latency(2,3,i)), squeeze(peaks_amplitude(2,3,i)),'rx','HandleVisibility','off')
    hold on

    plot(erptimes.', squeeze(unamb_stab(i,14,:)))
    hold on
    plot(squeeze(peaks_latency(1,4,i)), squeeze(peaks_amplitude(1,4,i)), 'ro','HandleVisibility','off')
    hold on
    plot(squeeze(peaks_latency(2,4,i)), squeeze(peaks_amplitude(2,4,i)),'rx','HandleVisibility','off')
    hold on
    legend('amb rev','amb stab','unamb rev','unamb stab')
    title(sprintf('Smileys %s',sublist(i)))
end
% Next: Export data +- 16 ms/ 4 TFs total from each
% component/participant/condition (= 2*19*4 = 152 files)
%P200
for k = 1:21
    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/PeakAnalysis/forLoreta/P200/SAR/';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(1,k,:,peaks_pnt(1,1,k)-4:peaks_pnt(1,1,k)+4)), 'exporterp','on','filecomp', sprintf('%s_LAR',char(sublist(k))));

    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/PeakAnalysis/forLoreta/P200/SAS';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(2,k,:,peaks_pnt(1,2,k)-4:peaks_pnt(1,2,k)+4)), 'exporterp','on','filecomp', sprintf('%s_LAS',char(sublist(k))));

    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/PeakAnalysis/forLoreta/P200/SDR/';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(3,k,:,peaks_pnt(1,3,k)-4:peaks_pnt(1,3,k)+4)), 'exporterp','on','filecomp', sprintf('%s_LDR',char(sublist(k))));

    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/PeakAnalysis/forLoreta/P200/SDS/';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(4,k,:,peaks_pnt(1,4,k)-4:peaks_pnt(1,4,k)+4)), 'exporterp','on','filecomp', sprintf('%s_LDS',char(sublist(k))));

end
%P400
for k = 1:21
    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/PeakAnalysis/forLoreta/P400/SAR/';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(1,k,:,peaks_pnt(2,1,k)-4:peaks_pnt(2,1,k)+4)), 'exporterp','on','filecomp', sprintf('%s_LAR',char(sublist(k))));

    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/PeakAnalysis/forLoreta/P400/SAS';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(2,k,:,peaks_pnt(2,2,k)-4:peaks_pnt(2,2,k)+4)), 'exporterp','on','filecomp', sprintf('%s_LAS',char(sublist(k))));

    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/PeakAnalysis/forLoreta/P400/SDR/';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(3,k,:,peaks_pnt(2,3,k)-4:peaks_pnt(2,3,k)+4)), 'exporterp','on','filecomp', sprintf('%s_LDR',char(sublist(k))));

    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/PeakAnalysis/forLoreta/P400/SDS/';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(4,k,:,peaks_pnt(2,4,k)-4:peaks_pnt(2,4,k)+4)), 'exporterp','on','filecomp', sprintf('%s_LDS',char(sublist(k))));

end
% Whole Epoch
for k = 1:length(amb_rev(:,1,1))
    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/LORETA/stabrev_rawfiles/SAR/';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(1,k,:,:)), 'exporterp','on','filecomp', sprintf('%s_SAR',char(sublist(k))));

    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/LORETA/stabrev_rawfiles/SAS/';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(2,k,:,:)), 'exporterp','on','filecomp', sprintf('%s_SAS',char(sublist(k))));

    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/LORETA/stabrev_rawfiles/SDR/';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(3,k,:,:)), 'exporterp','on','filecomp', sprintf('%s_SDR',char(sublist(k))));

    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/LORETA/stabrev_rawfiles/SDS/';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(4,k,:,:)), 'exporterp','on','filecomp', sprintf('%s_SDS',char(sublist(k))));

end

%% P200 using average of 150-300
for k = 1:21
    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/PeakAnalysis/forLoreta/Avg/P200/LAR/';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(1,k,:,xrange(1):xrange(2))), 'exporterp','on','filecomp', sprintf('%s_LAR',char(sublist(k))));

    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/PeakAnalysis/forLoreta/Avg/P200/LAS';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(2,k,:,xrange(1):xrange(2))), 'exporterp','on','filecomp', sprintf('%s_LAS',char(sublist(k))));

    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/PeakAnalysis/forLoreta/Avg/P200/LDR/';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(3,k,:,xrange(1):xrange(2))), 'exporterp','on','filecomp', sprintf('%s_LDR',char(sublist(k))));

    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/PeakAnalysis/forLoreta/Avg/P200/LDS/';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(4,k,:,xrange(1):xrange(2))), 'exporterp','on','filecomp', sprintf('%s_LDS',char(sublist(k))));

end
%P400 using avg of 300-600
for k = 1:length(sublist)
    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/PeakAnalysis/forLoreta/Avg/P400/LAR/';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(1,k,:,xrange(3):xrange(4))), 'exporterp','on','filecomp', sprintf('%s_LAR',char(sublist(k))));

    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/PeakAnalysis/forLoreta/Avg/P400/LAS';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(2,k,:,xrange(3):xrange(4))), 'exporterp','on','filecomp', sprintf('%s_LAS',char(sublist(k))));

    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/PeakAnalysis/forLoreta/Avg/P400/LDR/';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(3,k,:,xrange(3):xrange(4))), 'exporterp','on','filecomp', sprintf('%s_LDR',char(sublist(k))));

    mydir = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/PeakAnalysis/forLoreta/Avg/P400/LDS/';
    cd(mydir)
    eeglab2loreta( ALLEEG(1).chanlocs,squeeze(allInOne(4,k,:,xrange(3):xrange(4))), 'exporterp','on','filecomp', sprintf('%s_LDS',char(sublist(k))));
end
figure;
plot(erptimes.',squeeze(mean(unamb_rev(:,14,:),1)))
hold on
plot(erptimes.',squeeze(mean(unamb_stab(:,14,:),1)))

%% Create CSD Maps
for i = 1:length(ALLEEG(1).chanlocs)
    chans(i) = {ALLEEG(1).chanlocs(i).labels};
end
chans=chans.';
pause(1)
[M] = ExtractMontage ('10-5-System_Mastoids_EGI129.csd', chans);
% MapMontage(M)
pause(1)
[G,H] = GetGH(M); %default: 4, otherwise e.g.: GetGH(M, 3)


%% Ustab & Astab Graph

var1 = unamb_stab;
var2 = amb_stab;

% calculate 95% confidence intervals 
for i = 1:size(var1,3)
    tmp = var1(:,14,i);
    SEM = std(tmp)/sqrt(length(tmp));
    ts = tinv([0.025  0.975],length(tmp)-1);
    CI_amb_stab_pos(1,i) = mean(tmp)+max(ts)*SEM;
    CI_amb_stab_neg(1,i) = mean(tmp)+min(ts)*SEM;
end
for i = 1:size(var2,3)
    tmp = var2(:,14,i);
    SEM = std(tmp)/sqrt(length(tmp));
    ts = tinv([0.025  0.975],length(tmp)-1);
    CI_amb_rev_pos(1,i) = mean(tmp)+max(ts)*SEM;
    CI_amb_rev_neg(1,i) = mean(tmp)+min(ts)*SEM;
end

avg1=squeeze(mean(var1(:,14,:),1));
avg2=squeeze(mean(var2(:,14,:),1));

% Running Permutation Test on var1 vs. var2
sig_arr = NaN(1,size(var1,3));
for i=1:length(sig_arr(1,:))
    [p] = pairedPerm(var1(:,14,i),var2(:,14,i), 10000);
    if p<0.05
        sig_arr(1,i)=1;
    end
end

% Smooth it for better looks with moving average
tmp_var1_smooth = movmean(avg1,5);
tmp_var2_smooth = movmean(avg2,5);
CI_var1_pos_smooth = movmean(CI_amb_stab_pos,5);
CI_var1_neg_smooth = movmean(CI_amb_stab_neg,5);
CI_var2_pos_smooth = movmean(CI_amb_rev_pos,5);
CI_var2_neg_smooth = movmean(CI_amb_rev_neg,5);

% Some settings
width = 5;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 18;      % Fontsize
lw = 2;        % LineWidth
msz = 8;       % MarkerSize


% Plot it
figure;
% % shade p200 range
% h1 = area([150 300],[10 10]);
% h1.FaceColor=[0.9,0.9,0.9];
% h1 = area([150 300],[-10 -10]);
% h1.FaceColor=[0.9,0.9,0.9];
% hold on
% % shade p400 range
% h1 = area([300 600],[10 10]);
% h1.FaceColor=[0.8,0.8,0.8];
% h1 = area([300 600],[-10 -10]);
% h1.FaceColor=[0.8,0.8,0.8];
% hold on
% draw var2 and var2 Line
plot(erptimes,tmp_var1_smooth,'k','LineWidth',lw)
hold on
plot(erptimes,tmp_var2_smooth,'r','LineWidth',lw)
xlim([-60 800])
hold on
% Fill Confidence intervals...
% of var 1...
x2 = [erptimes, fliplr(erptimes)];
inBetween = [CI_var1_pos_smooth, fliplr(CI_var1_neg_smooth)];
fill(x2, inBetween, [0.3 0.3 0.3])
hold off
alpha(.5)
hold on
% ...of var 2
x2 = [erptimes, fliplr(erptimes)];
inBetween = [CI_var2_pos_smooth, fliplr(CI_var2_neg_smooth)];
fill(x2, inBetween, [0.5 0 0])
hold off
alpha(.5)
hold on
% significance line
scatter(erptimes,sig_arr*-1,'o', 'MarkerEdgeColor', 'r', 'MarkerFaceColor', 'r')
hold on
% horizontal line
plot([min(erptimes) max(erptimes)],[0 0],'k')
hold on
% vertical line
plot([0 0],[-10 10],'k')
% Axis & label Settings
ylabel('Amplitude in \muV','FontSize', 18)
xlabel('Time in ms','FontSize', 18')
xt = get(gca, 'XTick');
set(gca, 'FontSize', 18)
axis([-60 800 -4 4])
% save figure
print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/ERP-Ambiguity-Effect/Smi/U-stab_A-stab/Smiley_Ustab_Astab_Cz_smooth','-dpng','-r300');

% Scalp Maps var1
figure;
subplot(1,4,1)
topoplot(mean(SDS_p2,2),ALLEEG(1).chanlocs)
title('SDS P200')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
subplot(1,4,2)
topoplot(CSD(mean(SDS_p2,2),G,H),ALLEEG(1).chanlocs)
title('SDS P200 CSD')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
subplot(1,4,3)
topoplot(mean(SDS_p4,2),ALLEEG(1).chanlocs)
title('SDS P400')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
subplot(1,4,4)
topoplot(CSD(mean(SDS_p4,2),G,H),ALLEEG(1).chanlocs)
title('SDS P400 CSD')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
set(gcf, 'Position', [pos(1) pos(2) width*200, height*200]); %<- Set size

print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/ERP-Ambiguity-Effect/Smi/U-stab_A-stab/Smiley_Ustab_ScalpMaps','-dpng','-r300');

% Scalp Maps var2
figure;
subplot(1,4,1)
topoplot(mean(SAS_p2,2),ALLEEG(1).chanlocs)
title('SAS P200')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
subplot(1,4,2)
topoplot(CSD(mean(SAS_p2,2),G,H),ALLEEG(1).chanlocs)
title('SAS P200 CSD')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
subplot(1,4,3)
topoplot(mean(SAS_p4,2),ALLEEG(1).chanlocs)
title('SAS P400')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
subplot(1,4,4)
topoplot(CSD(mean(SAS_p4,2),G,H),ALLEEG(1).chanlocs)
title('SAS P400 CSD')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
set(gcf, 'Position', [pos(1) pos(2) width*200, height*200]); %<- Set size

print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/ERP-Ambiguity-Effect/Smi/U-stab_A-stab/Smiley_Astab_ScalpMaps','-dpng','-r300');

% Diff

for i = 1:size(var2,3)
    tmp = var1(:,14,i)-var2(:,14,i);
    SEM = std(tmp)/sqrt(length(tmp));
    ts = tinv([0.025  0.975],length(tmp)-1);
    CI_diff_pos(1,i) = mean(tmp)+max(ts)*SEM;
    CI_diff_neg(1,i) = mean(tmp)+min(ts)*SEM;
end
CI_diff_pos_smooth = movmean(CI_diff_pos,5);
CI_diff_neg_smooth = movmean(CI_diff_neg,5);

figure;
plot(erptimes,tmp_var1_smooth-tmp_var2_smooth,'b','LineWidth',lw)
xlim([-60 800])
hold on
% Fill Confidence intervals...
% of var 1...
x2 = [erptimes, fliplr(erptimes)];
inBetween = [CI_diff_pos_smooth, fliplr(CI_diff_neg_smooth)];
fill(x2, inBetween, [0 0 0.8])
hold off
alpha(.2)
hold on
% significance line
scatter(erptimes,sig_arr*-0.5,'o', 'MarkerEdgeColor', 'r', 'MarkerFaceColor', 'r')
hold on
% horizontal line
plot([min(erptimes) max(erptimes)],[0 0],'k')
hold on
% vertical line
plot([0 0],[-10 10],'k')
% Axis & label Settings
ylabel('Amplitude in \muV','FontSize', 18)
xlabel('Time in ms','FontSize', 18')
xt = get(gca, 'XTick');
set(gca, 'FontSize', 18)
axis([-60 800 -1 4])
% save figure
print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/ERP-Ambiguity-Effect/Lat/U-stab_A-stab/Lattice_Ustab_Astab_Diff_Cz_smooth','-dpng','-r300');

% Scalp Maps var1-var2
figure;
subplot(1,4,1)
topoplot(mean(SDS_p2,2)-mean(SAS_p2,2),ALLEEG(1).chanlocs)
title('SDS-SAS P200')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
subplot(1,4,2)
topoplot(CSD(mean(SDS_p2,2),G,H)-CSD(mean(SAS_p2,2),G,H),ALLEEG(1).chanlocs)
title('SDS-SAS P200 CSD')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
subplot(1,4,3)
topoplot(mean(SDS_p4,2)-mean(SAS_p4,2),ALLEEG(1).chanlocs)
title('SDS-SAS P400')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
subplot(1,4,4)
topoplot(CSD(mean(SDS_p4,2),G,H)-CSD(mean(SAS_p4,2),G,H),ALLEEG(1).chanlocs)
title('SDS-SAS P400 CSD')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
set(gcf, 'Position', [pos(1) pos(2) width*200, height*200]); %<- Set size

print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/ERP-Ambiguity-Effect/Smi/U-stab_A-stab/Smiley_diff_ScalpMaps','-dpng','-r300');

%% Urev & Arev Graph

var1 = unamb_rev;
var2 = amb_rev;

% calculate 95% confidence intervals 
for i = 1:size(var1,3)
    tmp = var1(:,14,i);
    SEM = std(tmp)/sqrt(length(tmp));
    ts = tinv([0.025  0.975],length(tmp)-1);
    CI_amb_stab_pos(1,i) = mean(tmp)+max(ts)*SEM;
    CI_amb_stab_neg(1,i) = mean(tmp)+min(ts)*SEM;
end
for i = 1:size(var2,3)
    tmp = var2(:,14,i);
    SEM = std(tmp)/sqrt(length(tmp));
    ts = tinv([0.025  0.975],length(tmp)-1);
    CI_amb_rev_pos(1,i) = mean(tmp)+max(ts)*SEM;
    CI_amb_rev_neg(1,i) = mean(tmp)+min(ts)*SEM;
end

avg1=squeeze(mean(var1(:,14,:),1));
avg2=squeeze(mean(var2(:,14,:),1));

% Running Permutation Test on var1 vs. var2
sig_arr = NaN(1,size(var1,3));
for i=1:length(sig_arr(1,:))
    [p] = pairedPerm(var1(:,14,i),var2(:,14,i), 10000);
    if p<0.05
        sig_arr(1,i)=1;
    end
end

% Smooth it for better looks with moving average
tmp_var1_smooth = movmean(avg1,5);
tmp_var2_smooth = movmean(avg2,5);
CI_var1_pos_smooth = movmean(CI_amb_stab_pos,5);
CI_var1_neg_smooth = movmean(CI_amb_stab_neg,5);
CI_var2_pos_smooth = movmean(CI_amb_rev_pos,5);
CI_var2_neg_smooth = movmean(CI_amb_rev_neg,5);

% Some settings
width = 5;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 18;      % Fontsize
lw = 2;        % LineWidth
msz = 8;       % MarkerSize


% Plot it
figure;
plot(erptimes,tmp_var1_smooth,'k','LineWidth',lw)
hold on
plot(erptimes,tmp_var2_smooth,'r','LineWidth',lw)
xlim([-60 800])
hold on
% Fill Confidence intervals...
% of var 1...
x2 = [erptimes, fliplr(erptimes)];
inBetween = [CI_var1_pos_smooth, fliplr(CI_var1_neg_smooth)];
fill(x2, inBetween, [0.3 0.3 0.3])
hold off
alpha(.5)
hold on
% ...of var 2
x2 = [erptimes, fliplr(erptimes)];
inBetween = [CI_var2_pos_smooth, fliplr(CI_var2_neg_smooth)];
fill(x2, inBetween, [0.5 0 0])
hold off
alpha(.5)
hold on
% significance line
scatter(erptimes,sig_arr*-1,'o', 'MarkerEdgeColor', 'r', 'MarkerFaceColor', 'r')
hold on
% horizontal line
plot([min(erptimes) max(erptimes)],[0 0],'k')
hold on
% vertical line
plot([0 0],[-10 10],'k')
% Axis & label Settings
ylabel('Amplitude in \muV','FontSize', 18)
xlabel('Time in ms','FontSize', 18')
xt = get(gca, 'XTick');
set(gca, 'FontSize', 18)
axis([-60 800 -4 8])
% save figure
print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/ERP-Ambiguity-Effect/Smi/U-rev_A-rev/Lattice_Urev_Arev_Cz_smooth','-dpng','-r300');

% Scalp Maps var1
figure;
pos = get(gcf, 'Position');
subplot(1,4,1)
topoplot(mean(SDR_p2,2),ALLEEG(1).chanlocs)
title('LDR P200')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
subplot(1,4,2)
topoplot(CSD(mean(SDR_p2,2),G,H),ALLEEG(1).chanlocs)
title('LDR P200 CSD')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
subplot(1,4,3)
topoplot(mean(SDR_p4,2),ALLEEG(1).chanlocs)
title('LDR P400')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
subplot(1,4,4)
topoplot(CSD(mean(SDR_p4,2),G,H),ALLEEG(1).chanlocs)
title('LDR P400 CSD')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
set(gcf, 'Position', [pos(1) pos(2) width*200, height*200]); %<- Set size

print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/ERP-Ambiguity-Effect/Smi/U-rev_A-rev/Lattice_Urev_ScalpMaps','-dpng','-r300');

% Scalp Maps var2
figure;
subplot(1,4,1)
topoplot(mean(SAR_p2,2),ALLEEG(1).chanlocs)
title('LAR P200')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
subplot(1,4,2)
topoplot(CSD(mean(SAR_p2,2),G,H),ALLEEG(1).chanlocs)
title('LAR P200 CSD')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
subplot(1,4,3)
topoplot(mean(SAR_p4,2),ALLEEG(1).chanlocs)
title('LAR P400')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
subplot(1,4,4)
topoplot(CSD(mean(SAR_p4,2),G,H),ALLEEG(1).chanlocs)
title('LAR P400 CSD')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
set(gcf, 'Position', [pos(1) pos(2) width*200, height*200]); %<- Set size

print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/ERP-Ambiguity-Effect/Smi/U-rev_A-rev/Lattice_Arev_ScalpMaps','-dpng','-r300');

% Diff

for i = 1:size(var2,3)
    tmp = var1(:,14,i)-var2(:,14,i);
    SEM = std(tmp)/sqrt(length(tmp));
    ts = tinv([0.025  0.975],length(tmp)-1);
    CI_diff_pos(1,i) = mean(tmp)+max(ts)*SEM;
    CI_diff_neg(1,i) = mean(tmp)+min(ts)*SEM;
end
CI_diff_pos_smooth = movmean(CI_diff_pos,5);
CI_diff_neg_smooth = movmean(CI_diff_neg,5);

figure;
plot(erptimes,tmp_var1_smooth-tmp_var2_smooth,'b','LineWidth',lw)
xlim([-60 800])
hold on
% Fill Confidence intervals...
% of var 1...
x2 = [erptimes, fliplr(erptimes)];
inBetween = [CI_diff_pos_smooth, fliplr(CI_diff_neg_smooth)];
fill(x2, inBetween, [0 0 0.8])
hold off
alpha(.2)
hold on
% significance line
scatter(erptimes,sig_arr*-0.5,'o', 'MarkerEdgeColor', 'r', 'MarkerFaceColor', 'r')
hold on
% horizontal line
plot([min(erptimes) max(erptimes)],[0 0],'k')
hold on
% vertical line
plot([0 0],[-10 10],'k')
% Axis & label Settings
ylabel('Amplitude in \muV','FontSize', 18)
xlabel('Time in ms','FontSize', 18')
xt = get(gca, 'XTick');
set(gca, 'FontSize', 18)
axis([-60 800 -1 8])
% save figure
print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/ERP-Ambiguity-Effect/Smi/U-rev_A-rev/Lattice_Urev_Arev_diff_Cz_smooth','-dpng','-r300');

% Scalp Maps var1-var2
figure;
subplot(1,4,1)
topoplot(mean(SDR_p2,2)-mean(SAR_p2,2),ALLEEG(1).chanlocs)
title('SDR-SAR P200')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
subplot(1,4,2)
topoplot(CSD(mean(SDR_p2,2),G,H)-CSD(mean(SAR_p2,2),G,H),ALLEEG(1).chanlocs)
title('SDR-SAR P200 CSD')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
subplot(1,4,3)
topoplot(mean(SDR_p4,2)-mean(SAR_p4,2),ALLEEG(1).chanlocs)
title('SDR-SAR P400')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
subplot(1,4,4)
topoplot(CSD(mean(SDR_p4,2),G,H)-CSD(mean(SAR_p4,2),G,H),ALLEEG(1).chanlocs)
title('SDR-SAR P400 CSD')
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
set(gcf, 'Position', [pos(1) pos(2) width*200, height*200]); %<- Set size

print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/ERP-Ambiguity-Effect/Smi/U-rev_A-rev/Lattice_diff_ScalpMaps','-dpng','-r300');
