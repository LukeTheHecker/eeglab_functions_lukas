function ClustStruc=cluster_results_overlap(tablename1,tablename2, posOrNeg, normalization, t_crit_1,t_crit_2)
%% example: ClustStruc = cluster_results(LattROI1p05, 1, 1)
% cluster the voxels in LattROI1p05, look only at positive (U>A) effects
% and correc cluster values for multiple comparisons
addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/')
ft_defaults 
addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/template/atlas/aal/')

if isnan(t_crit_1) || isnan(t_crit_2)
    ClustStruc=[];
    return
end

tablename1=squeeze(tablename1);
tablename2=squeeze(tablename2);

A = tablename1;
B = tablename2;
A(find(abs(A(:,4))<abs(t_crit_1)),:) = [];
B(find(abs(B(:,4))<abs(t_crit_2)),:) = [];


if posOrNeg == 0
    A(find(A(:,4)<0),:) = [];
    B(find(B(:,4)<0),:) = [];
elseif posOrNeg == 1
    A(find(A(:,4)>0),:) = [];
    B(find(B(:,4)>0),:) = [];
    A(:,4) = abs(A(:,4));
    B(:,4) = abs(B(:,4));
end

[~,idx_A,idx_B] = intersect(A(:,1:3),B(:,1:3),'rows');
myLocsAct_A = A(idx_A,:);
myLocsAct_B = B(idx_B,:);

for i = 1:length(myLocsAct_A(:,4))
    tmp_A = length(find(myLocsAct_A(:,4)<myLocsAct_A(i,4)))/length(myLocsAct_A(:,4));
    tmp_B = length(find(myLocsAct_B(:,4)<myLocsAct_B(i,4)))/length(myLocsAct_B(:,4));
    
    myLocsAct_A(i,4) = tmp_A;
    myLocsAct_B(i,4) = tmp_B;
end
%% normalize both images to have an equal mean t-value
% if normalization == 1
%     if max(myLocsAct_B(:,4))>max(myLocsAct_A(:,4))
%         myLocsAct_B(:,4) = myLocsAct_B(:,4) * (max(myLocsAct_A(:,4))/max(myLocsAct_B(:,4)));
%     else
%         myLocsAct_A(:,4) = myLocsAct_A(:,4) * (max(myLocsAct_B(:,4))/max(myLocsAct_A(:,4)));
%     end
% end

myLocsAct = myLocsAct_A;
myLocsAct(:,4) = 1;%(myLocsAct_A(:,4)+myLocsAct_B(:,4))./2 ;


%% Duplicate entries based on their t value
sub_t = min(myLocsAct(:,4));
% myLocsAct(:,4) =myLocsAct(:,4)-sub_t+1; 
initial_length=length(myLocsAct(:,1));
for i = 1:initial_length
    if myLocsAct(i,4)/sub_t >= 1.5
        for k = 1:round(myLocsAct(i,4)/sub_t)
            myLocsAct(end+1,:) = myLocsAct(i,:);
        end
    end
end
% myLocsAct(:,4) = myLocsAct(:,4)+sub_t-1;
       
if initial_length<=4
    ClustStruc=[];
    return
end

%% kmeans_opt.m
[cidx,cmeans] = kmeans_opt(myLocsAct(:,1:3),8,0.9,100);
nClusts = max(cidx);
fprintf('Choosing %d Clusters\n', nClusts)

figure;

ptsymb = {'bs','r^','md','go','c+', 'ko','y*', 'k^',};
for i = 1:nClusts
    clust = find(cidx==i);
    plot3(myLocsAct(clust,1),myLocsAct(clust,2),myLocsAct(clust,3),ptsymb{i});
    hold on
end
plot3(cmeans(:,1),cmeans(:,2),cmeans(:,3),'ko');
plot3(cmeans(:,1),cmeans(:,2),cmeans(:,3),'kx');
hold off
xlabel('Sepal Length');
ylabel('Sepal Width');
zlabel('Petal Length');
view(-137,10);
rotate3d
grid on
axis vis3d
title('Clusters with all Significant Voxels')

[myLocsAct,IA,IC] = unique(myLocsAct,'rows');
cidx = cidx(IA);
atlas = ft_read_atlas('ROI_MNI_V4.nii');

for k = 1:nClusts
    disp(k)
    data = myLocsAct(find(cidx==k),1:4);
    centroid = cmeans(k,:);
    tMax = max(abs(data(:,4)));
    tClust = mean(data(:,4));
    lorMaxVox=data(find(data(:,4)==tMax),1:3);
    if length(find(data(:,1)<0)) == 0
        side = 'R';
    elseif length(find(data(:,1)>0)) == 0
        side = 'L';
    else
        side = 'L/R';
    end
    ClustStruc(k).side = side;
    ClustStruc(k).lorMaxAAL = get_label(lorMaxVox,atlas);
    ClustStruc(k).tClust_Max = [num2str(round(tClust,2)), ' (', num2str(round(tMax,2)), ')'];
    ClustStruc(k).nVoxels=length(myLocsAct(cidx==k));
    ClustStruc(k).tX = lorMaxVox(1);
    ClustStruc(k).tY = lorMaxVox(2);
    ClustStruc(k).tZ = lorMaxVox(3);
    ClustStruc(k).cX = round(centroid(1));
    ClustStruc(k).cY = round(centroid(2));
    ClustStruc(k).cZ = round(centroid(3));
end

end