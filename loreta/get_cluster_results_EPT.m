%% F-Log ratio of averages, normalized subjectwise

t_critsF_UA = [0.386,0.388,0.498,0.548,0.596,...
    0.547,0.742,0.568,0.678,0.711];
t_critsF_AU = [0.386,0.388,0.498,0.552,0.595,...
    0.547,0.737,0.565,0.673,0.706];
% Clusterwise-significance level (according to exceedence proportion test):
% (2tailes)
t_critsF_EPT = [0.0578,0.051853,NaN,0.091266,0.079379,...
    0.069810,0.078872,0.087333,0.121211,0.080572];
% 1 tailed
t_critsF_EPT_AU = [0.226344,0.051853,NaN,NaN,NaN,...
    0.433001,0.078872,NaN,NaN,0.402858];
t_critsF_EPT_UA = [0.057800,NaN,NaN,0.091266,0.079379,...
    0.069810,NaN,0.174667,0.121211,NaN];

%% U>A
ClustStrucPosEPT.LatFullTime = cluster_results_duplicates(LatFullTime,1,0,t_critsF_EPT_UA(1));
ClustStrucPosEPT.LattROI1 = cluster_results_duplicates(LattROI1,1,0,t_critsF_EPT_UA(2));
ClustStrucPosEPT.LattROI2 = cluster_results_duplicates(LattROI2,1,0,t_critsF_EPT_UA(3));
ClustStrucPosEPT.LattROI3 = cluster_results_duplicates(LattROI3,1,0,t_critsF_EPT_UA(4));
ClustStrucPosEPT.LattROI4 = cluster_results_duplicates(LattROI4,1,0,t_critsF_EPT_UA(5));

ClustStrucPosEPT.SmiFullTime = cluster_results_duplicates(SmiFullTime,1,0,t_critsF_EPT_UA(6));
ClustStrucPosEPT.SmitROI1 = cluster_results_duplicates(SmitROI1,1,0,t_critsF_EPT_UA(7));
ClustStrucPosEPT.SmitROI2 = cluster_results_duplicates(SmitROI2,1,0,t_critsF_EPT_UA(8));
ClustStrucPosEPT.SmitROI3 = cluster_results_duplicates(SmitROI3,1,0,t_critsF_EPT_UA(9));
ClustStrucPosEPT.SmitROI4 = cluster_results_duplicates(SmitROI4,1,0,t_critsF_EPT_UA(10));

%% A>U
ClustStrucNegEPT.LatFullTime = cluster_results_duplicates(LatFullTime,0,0,t_critsF_EPT_AU(1));
ClustStrucNegEPT.LattROI1 = cluster_results_duplicates(LattROI1,0,0,t_critsF_EPT_AU(2));
ClustStrucNegEPT.LattROI2 = cluster_results_duplicates(LattROI2,0,0,t_critsF_EPT_AU(3));
ClustStrucNegEPT.LattROI3 = cluster_results_duplicates(LattROI3,0,0,t_critsF_EPT_AU(4));
ClustStrucNegEPT.LattROI4 = cluster_results_duplicates(LattROI4,0,0,t_critsF_EPT_AU(5));

ClustStrucNegEPT.SmiFullTime = cluster_results_duplicates(SmiFullTime,0,0,t_critsF_EPT_AU(6));
ClustStrucNegEPT.SmitROI1 = cluster_results_duplicates(SmitROI1,0,0,t_critsF_EPT_AU(7));
ClustStrucNegEPT.SmitROI2 = cluster_results_duplicates(SmitROI2,0,0,t_critsF_EPT_AU(8));
ClustStrucNegEPT.SmitROI3 = cluster_results_duplicates(SmitROI3,0,0,t_critsF_EPT_AU(9));
ClustStrucNegEPT.SmitROI4 = cluster_results_duplicates(SmitROI4,0,0,t_critsF_EPT_AU(10));


%% Overlaps pos
ClustStrucOLPosEPT.FullTime = cluster_results_overlap(LatFullTime,SmiFullTime,0,0,t_critsF_EPT_UA(1),t_critsF_EPT_UA(6));
ClustStrucOLPosEPT.tROI1 = cluster_results_overlap(LattROI1,SmitROI1,0,0,t_critsF_EPT_UA(2),t_critsF_EPT_UA(7));
ClustStrucOLPosEPT.tROI2 = cluster_results_overlap(LattROI2,SmitROI2,0,0,t_critsF_EPT_UA(3),t_critsF_EPT_UA(8));
ClustStrucOLPosEPT.tROI3 = cluster_results_overlap(LattROI3,SmitROI3,0,0,t_critsF_EPT_UA(4),t_critsF_EPT_UA(9));
ClustStrucOLPosEPT.tROI4 = cluster_results_overlap(LattROI4,SmitROI4,0,0,t_critsF_EPT_UA(5),t_critsF_EPT_UA(10));
%% Overlaps neg
ClustStrucOLNegEPT.FullTime = cluster_results_overlap(LatFullTime,SmiFullTime,1,0,t_critsF_EPT_AU(1),t_critsF_EPT_AU(6));
ClustStrucOLNegEPT.tROI1 = cluster_results_overlap(LattROI1,SmitROI1,1,0,t_critsF_EPT_AU(2),t_critsF_EPT_AU(7));
ClustStrucOLNegEPT.tROI2 = cluster_results_overlap(LattROI2,SmitROI2,1,0,t_critsF_EPT_AU(3),t_critsF_EPT_AU(8));
ClustStrucOLNegEPT.tROI3 = cluster_results_overlap(LattROI3,SmitROI3,1,0,t_critsF_EPT_AU(4),t_critsF_EPT_AU(9));
ClustStrucOLNegEPT.tROI4 = cluster_results_overlap(LattROI4,SmitROI4,1,0,t_critsF_EPT_AU(5),t_critsF_EPT_AU(10));
