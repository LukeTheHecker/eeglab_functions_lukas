function myTable = mom_to_table(myStruc)
if isempty(myStruc)
    myTable = [];
    return
end

myTable = NaN(myStruc.dim(1)*myStruc.dim(2)*myStruc.dim(3),4);
cnt=1;
for a = 1:myStruc.dim(1)
    for b = 1:myStruc.dim(2)
        for c = 1:myStruc.dim(3)
            if ~isempty(myStruc.mom(a,b,c))
                tmp=(myStruc.transform*[a b c 1]')';
                myTable(cnt,1:3) = tmp(1:3);
                myTable(cnt,4) = myStruc.mom(a,b,c);
                cnt = cnt+1;
            end
        end
    end
end
myTable=myTable(~isnan(myTable(:,4)),:);



    

end