function myTable=slor_to_table_multi_TF(path, name)
addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/')
ft_defaults

source=loreta2fieldtrip([path name]);
myTable = NaN(76,source.dim(1),source.dim(2),source.dim(3));
cnt=1;
for a = 1:source.dim(1)
    for b = 1:source.dim(2)
        for c = 1:source.dim(3)
            if ~isempty(source.mom{a,b,c}) == 1
                myTable(:,a,b,c) = source.mom{a,b,c};
            else
                myTable(:,a,b,c) = NaN;
            end
        end
    end
end


end