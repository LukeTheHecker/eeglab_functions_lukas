function [myIntersect]=cluster_results_overlap(tablename1,tablename2, posOrNeg, t_crit_1,t_crit_2)
%% example: ClustStruc = cluster_results(LattROI1p05, 1, 1)
% cluster the voxels in LattROI1p05, look only at positive (U>A) effects
% and correc cluster values for multiple comparisons
addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/')
ft_defaults 

if isnan(t_crit_1) || isnan(t_crit_2)
    myIntersect=[];
    return
end
myIntersect = NaN(1,2);
tablename1=squeeze(tablename1);
tablename2=squeeze(tablename2);

A = tablename1;
B = tablename2;
A(find(abs(A(:,4))<abs(t_crit_1)),:) = [];
B(find(abs(B(:,4))<abs(t_crit_2)),:) = [];


if posOrNeg == 0
    A(find(A(:,4)<0),:) = [];
    B(find(B(:,4)<0),:) = [];
elseif posOrNeg == 1
    A(find(A(:,4)>0),:) = [];
    B(find(B(:,4)>0),:) = [];
    A(:,4) = abs(A(:,4));
    B(:,4) = abs(B(:,4));
end

[~,idx_A,idx_B] = intersect(A(:,1:3),B(:,1:3),'rows');
myIntersect(1) = length(idx_A)/length(A);
myIntersect(2) = length(idx_B)/length(B);
end