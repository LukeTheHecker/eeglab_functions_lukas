function ClustStruc=cluster_results_duplicates(tablename, posOrNeg, clustCorrect, t_crit)
%% example: ClustStruc = cluster_results(LattROI1p05, 1, 1)
% cluster the voxels in LattROI1p05, look only at positive (U>A) effects
% and correc cluster values for multiple comparisons

addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/')
ft_defaults 

tablename=squeeze(tablename);
myLocsAct=tablename;
myLocsAct_Ori=myLocsAct;
if posOrNeg == 1
    myLocsAct(find(myLocsAct(:,4)<0),:) = [];
elseif posOrNeg == 0
    myLocsAct(find(myLocsAct(:,4)>0),:) = [];
end

if t_crit ~= 0 
    myLocsAct(find(abs(myLocsAct(:,4))<abs(t_crit)),:) = [];
end
if isnan(t_crit)
    ClustStruc=[];
    return
end

%% Duplicate entries based on their t value
sub_t = min(abs(myLocsAct(:,4)));
% myLocsAct(:,4) =abs(myLocsAct(:,4))-sub_t+1; 
initial_length=length(myLocsAct(:,1));
for i = 1:initial_length
    if myLocsAct(i,4)/sub_t >= 1.5
        for k = 1:round(myLocsAct(i,4)/sub_t)
            myLocsAct(end+1,:) = myLocsAct(i,:);
        end
    end
end
% myLocsAct(:,4) = myLocsAct(:,4)+sub_t-1;

if initial_length<=8
    atlas = ft_read_atlas('ROI_MNI_V4.nii');


    data = myLocsAct;
    centroid_x = mean(data(:,1));
    centroid_y = mean(data(:,2));
    centroid_z = mean(data(:,3));
    centroid = [centroid_x,centroid_y,centroid_z];
    m = mean(data(:,4));
    mx = max(abs(data(:,4)));
    lorMaxVox = data(data(:,4)==max(data(:,4)),1:3);    

    ClustStruc(1).side = 'L';
    ClustStruc(1).lorMaxAAL = get_label(lorMaxVox,atlas);
    ClustStruc(1).tClust_Max = [char(num2str(m)) ' (' char(num2str(mx)) ')'];
    ClustStruc(1).nVoxels = length(data);
    ClustStruc(1).tX = lorMaxVox(1);
    ClustStruc(1).tY = lorMaxVox(2);
    ClustStruc(1).tZ = lorMaxVox(3);
    ClustStruc(1).cX = centroid_x;
    ClustStruc(1).cY = centroid_y;
    ClustStruc(1).cZ = centroid_z;

    % ClustStrucNeg.LattROI2(1).data = data;
    ClustStruc(1).lorMaxVox = lorMaxVox;
    ClustStruc(1).tMaxAAL = get_label(lorMaxVox,atlas); 
    ClustStruc(1).VoxDist = sqrt(sum((centroid-lorMaxVox).^2));
    return
end
     
%% kmeans_opt.m
[cidx,cmeans] = kmeans_opt(myLocsAct(:,1:3),8,0.9);
nClusts = max(cidx);
fprintf('Choosing %d Clusters\n', nClusts)

[myLocsAct,IA,IC] = unique(myLocsAct,'rows');
cidx = cidx(IA);

% sort 'myLocs' into spereate array clusters
addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/template/atlas/aal/')
atlas = ft_read_atlas('ROI_MNI_V4.nii');

for k = 1:nClusts
    disp(k)
    data = myLocsAct(find(cidx==k),1:4);
    centroid = cmeans(k,:);
    tMax = max(abs(data(:,4)));
    tClust = mean(data(:,4));
    lorMaxVox=data(find(abs(data(:,4))==tMax),1:3);
    if length(find(data(:,1)<0)) == 0
        side = 'R';
    elseif length(find(data(:,1)>0)) == 0
        side = 'L';
    else
        side = 'L/R';
    end
    
    ClustStruc(k).side = side;
    ClustStruc(k).lorMaxAAL = get_label(lorMaxVox,atlas);
    ClustStruc(k).tClust_Max = [num2str(round(abs(tClust),2)), ' (', num2str(round(abs(tMax),2)), ')'];
    ClustStruc(k).nVoxels=length(myLocsAct(cidx==k));
    ClustStruc(k).tX = lorMaxVox(1);
    ClustStruc(k).tY = lorMaxVox(2);
    ClustStruc(k).tZ = lorMaxVox(3);
    ClustStruc(k).cX = round(centroid(1));
    ClustStruc(k).cY = round(centroid(2));
    ClustStruc(k).cZ = round(centroid(3));

%    ClustStruc(k).data=data;
%    ClustStruc(k).centroid = centroid;
%     tVals=ClustStruc(k).data(:,4);
%     for h = 1:height(tablename)
%         if tablename{h,[1 2 3]} == ClustStruc(k).lorMaxVox
%             ClustStruc(k).lorMaxBrodmann=tablename{h,8};
%             ClustStruc(k).lorMaxLobe=char(tablename{h,9});
%             ClustStruc(k).lorMaxStructure=char(tablename{h,10});
%             break
%         end
%     end
%     ClustStruc(k).tMaxAAL = get_label(ClustStruc(k).centroid,atlas);
%     tmpDiff=ClustStruc(k).data(:,1:3)-cmeans(k,:);
%     distArr=NaN(1,ClustStruc(k).nVoxels);
%     for i=1:ClustStruc(k).nVoxels 
%         distArr(i)=sqrt(sum(tmpDiff(i,:).^2));
%     end
%     indx=find(distArr==min(distArr));
%     ClustStruc(k).centroidOri = ClustStruc(k).data(indx(1),1:3);
%     ClustStruc(k).tMax = tMax;
%     ClustStruc(k).tClust = tClust;
%     ClustStruc(k).VoxDist = sqrt(sum([ClustStruc(k).lorMaxVox-ClustStruc(k).centroid].^2));
end
end