function myCorrMat = loreta_conjunction_correlation(path1,name1,t_crit1,path2,name2, t_crit2,PosOrNeg,dest)
addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/')
ft_defaults 

if isnan(t_crit1) || isnan(t_crit2)
    myCorrMat = [];
    return
end

source1=loreta2fieldtrip([path1 char(name1)]);
source2=loreta2fieldtrip([path2 char(name2)]);
% length(source1.mom(~isnan(source1.mom)))

%% Remove sub-threshold voxels
if length(t_crit1)==1 && length(t_crit2)==1
    source1.mom(find(abs(source1.mom)<t_crit1))=NaN;
    source2.mom(find(abs(source2.mom)<t_crit2))=NaN;
end
mysize1 = size(source1.mom);
mysize2 = size(source2.mom);

%% PosOrNeg
if PosOrNeg == 0
    source1.mom(find(source1.mom<0))=NaN;
    source2.mom(find(source2.mom<0))=NaN;
    myCmap = 'hot';
    fcl = 'zeromax';
elseif PosOrNeg == 1
    source1.mom(find(source1.mom>0))=NaN;
    source2.mom(find(source2.mom>0))=NaN;
    myCmap = 'winter';
    fcl = 'minzero';
end
if PosOrNeg == 2 && length(t_crit1)==2
    for a=1:mysize1(1)
        for b=1:mysize1(2)
            for c=1:mysize1(3)
                if source1.mom(a,b,c)>0 && ~isnan(t_crit1(1)) && source1.mom(a,b,c)<t_crit1(1)
                    source1.mom(a,b,c) = NaN;
                elseif source1.mom(a,b,c)<0 && ~isnan(t_crit1(2)) && abs(source1.mom(a,b,c))<abs(t_crit1(2))
                    source1.mom(a,b,c) = NaN;
                end
                if source2.mom(a,b,c)>0 && ~isnan(t_crit2(1)) && source2.mom(a,b,c)<t_crit2(1)
                    source2.mom(a,b,c) = NaN;
                elseif source2.mom(a,b,c)<0 && ~isnan(t_crit2(2)) && abs(source2.mom(a,b,c))<abs(t_crit2(2))
                    source2.mom(a,b,c) = NaN;
                end

            end
        end
    end
    
end

%% 3x3x3 cube

winsize = 3;

data_A = source1.mom;
data_A(find(isnan(data_A)))=0;
data_B = source2.mom;
data_B(find(isnan(data_B)))=0;

% Simulation verifies that randomly generated sources are not as highly
% correlated as the original data!

% sourcemin = min(min(min(source1.mom)));
% sourcemax = max(max(max(source1.mom)));
% data_A = (sourcemax-sourcemin).*rand(size(data_A)) + sourcemin;
% sourcemin = min(min(min(source2.mom)));
% sourcemax = max(max(max(source2.mom)));
% data_B = (sourcemax-sourcemin).*rand(size(data_B)) + sourcemin;


dst = floor(winsize/2);
sourceCorr = source1;
sourceCorr.mom = NaN(size(data_A));
for a = 1+dst:size(data_A,1)-dst
    for b = 1+dst:size(data_A,2)-dst
        for c = 1+dst:size(data_A,3)-dst
            if data_A(a,b,c)~=0 && data_B(a,b,c)~=0 
                cube_1 = data_A(getrange(a,dst),getrange(b,dst),getrange(c,dst));
                cube_2 = data_B(getrange(a,dst),getrange(b,dst),getrange(c,dst));
                cube_1 = reshape(cube_1, [1, prod(size(cube_1))]);
                cube_2 = reshape(cube_2, [1, prod(size(cube_2))]);
                if sum(cube_1==0) < 24 && sum(cube_2==0) < 24
                    [crr,p] = corrcoef(cube_1,cube_2);
                    if min(min(crr)) > 0 %&& min(min(p)) < 0.05    %% remove condition on p!!! 
                        sourceCorr.mom(a,b,c) = min(min(crr));  
                    end
                end
            end
        end
    end
end
            
            
            
%% 5x1 sliding window
% winsize = 3;
% helper = floor(winsize/2);
% sourceCorr = source1;
% sourceCorr.mom = NaN([size(source1.mom) 6]);
% for a = 1:size(source1.mom,1)
%     for b = 1:size(source1.mom,2)
%         for c = round(winsize/2):1:size(source1.mom,3)-round(winsize/2)
%             % check if both sources are free of NaN in that window
%             if max(isnan(source1.mom(a,b,c-helper:c+helper)))==0 && max(isnan(source2.mom(a,b,c-helper:c+helper)))==0
%                 if min(min(corrcoef(squeeze(source1.mom(a,b,c-helper:c+helper)),squeeze(source2.mom(a,b,c-helper:c+helper)))))>0
%                     sourceCorr.mom(a,b,c,1) = min(min(corrcoef(squeeze(source1.mom(a,b,c-helper:c+helper)),squeeze(source2.mom(a,b,c-helper:c+helper)))));
%                 end
%             end
%         end
%     end
% end
% for a = 1:size(source1.mom,1)
%     for c = 1:size(source1.mom,3)
%         for b = round(winsize/2):1:size(source1.mom,2)-round(winsize/2)
%             % check if both sources are free of NaN in that window
%             if max(isnan(source1.mom(a,b-helper:b+helper,c)))==0 && max(isnan(source2.mom(a,b-helper:b+helper,c)))==0
%                 if min(min(corrcoef(squeeze(source1.mom(a,b-helper:b+helper,c)),squeeze(source2.mom(a,b-helper:b+helper,c)))))>0
%                     sourceCorr.mom(a,b,c,2) = min(min(corrcoef(squeeze(source1.mom(a,b-helper:b+helper,c)),squeeze(source2.mom(a,b-helper:b+helper,c)))));
%                 end
%             end
%         end
%     end
% end
% for c = 1:size(source1.mom,3)
%     for a = 1:size(source1.mom,1)
%         for b = round(winsize/2):1:size(source2.mom,3)-round(winsize/2)
%             % check if both sources are free of NaN in that window
%             if max(isnan(source1.mom(a,b-helper:b+helper,c)))==0 && max(isnan(source2.mom(a,b-helper:b+helper,c)))==0
%                 if min(min(corrcoef(squeeze(source1.mom(a,b-helper:b+helper,c)),squeeze(source2.mom(a,b-helper:b+helper,c)))))>0
%                     sourceCorr.mom(a,b,c,3) = min(min(corrcoef(squeeze(source1.mom(a,b-helper:b+helper,c)),squeeze(source2.mom(a,b-helper:b+helper,c)))));
%                 end
%             end
%         end
%     end
% end
% for c = 1:size(source1.mom,3)
%     for b = 1:size(source1.mom,2)
%         for a = round(winsize/2):1:size(source1.mom,1)-round(winsize/2)
%             % check if both sources are free of NaN in that window
%             if max(isnan(source1.mom(a-helper:a+helper,b,c)))==0 && max(isnan(source2.mom(a-helper:a+helper,b,c)))==0
%                 if min(min(corrcoef(squeeze(source1.mom(a-helper:a+helper,b,c)),squeeze(source2.mom(a-helper:a+helper,b,c)))))>0
%                     sourceCorr.mom(a,b,c,4) = min(min(corrcoef(squeeze(source1.mom(a-helper:a+helper,b,c)),squeeze(source2.mom(a-helper:a+helper,b,c)))));
%                 end
%             end
%         end
%     end
% end
% for b = 1:size(source1.mom,2)
%     for a = 1:size(source1.mom,1)
%         for c = round(winsize/2):1:size(source1.mom,3)-round(winsize/2)
%             % check if both sources are free of NaN in that window
%             if max(isnan(source1.mom(a,b,c-helper:c+helper)))==0 && max(isnan(source2.mom(a,b,c-helper:c+helper)))==0
%                 if min(min(corrcoef(squeeze(source1.mom(a,b,c-helper:c+helper)),squeeze(source2.mom(a,b,c-helper:c+helper)))))>0
%                     sourceCorr.mom(a,b,c,5) = min(min(corrcoef(squeeze(source1.mom(a,b,c-helper:c+helper)),squeeze(source2.mom(a,b,c-helper:c+helper)))));
%                 end
%             end
%         end
%     end
% end
% for b = 1:size(source1.mom,2)
%     for c = 1:size(source1.mom,3)
%         for a = round(winsize/2):1:size(source1.mom,1)-round(winsize/2)
%             % check if both sources are free of NaN in that window
%             if max(isnan(source1.mom(a-helper:a+helper,b,c)))==0 && max(isnan(source2.mom(a-helper:a+helper,b,c)))==0
%                 if min(min(corrcoef(squeeze(source1.mom(a-helper:a+helper,b,c)),squeeze(source2.mom(a-helper:a+helper,b,c)))))>0
%                     sourceCorr.mom(a,b,c,6) = min(min(corrcoef(squeeze(source1.mom(a-helper:a+helper,b,c)),squeeze(source2.mom(a-helper:a+helper,b,c)))));
%                 end
%             end
%         end
%     end
% end
% sourceCorr.mom=nanmean(sourceCorr.mom,4);
%% Check if any Voxels survived
if min(min(min(isnan(sourceCorr.mom)))) == 1 
    disp('No supra-threshold voxels left')
    return
end



%%
cur_path_FT = '/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206';
% Read in the MNI template from SP
mri = ft_read_mri([cur_path_FT, '/external/spm8/templates/T1.nii']); 
cfg = [];
mri = ft_volumereslice(cfg, mri);

%% Downsample
cfg=[];
cfg.downsample=[6];
smooth_mri = ft_volumedownsample(cfg,mri);

% interpolate sources
cfg            = [];
cfg.downsample = [];% was 10
cfg.parameter  = 'mom';
conjunctionDiffInt  = ft_sourceinterpolate(cfg, sourceCorr , smooth_mri);

% stats
% ft_sourcestatistics


% Interpolate your LORETA volume on the MNI template: 
% figure;
% csg=[];
% cfg.parameter = 'avg';
% [interp_mean] = ft_sourceinterpolate(cfg, source, template);

%% plot surface
% if only pos values
if min(min(min(sourceCorr.mom)))>=0
    fclim = 'zeromax';
    fcmap = 'hot';
% if only neg values
elseif max(max(max(sourceCorr.mom)))<=0
    fclim = 'minzero';
    fcmap = 'cool';
else
    fcmap = 'jet';
    fclim = [min(min(min(sourceCorr.mom))) max(max(max(sourceCorr.mom)))];
end
fcmap = 'jet';
fclim = 'zeromax';
cfg = [];
cfg.method         = 'surface';
cfg.funparameter = 'mom';
cfg.maskparameter  = cfg.funparameter;
cfg.funcolorlim    = fclim;%'maxabs';
cfg.funcolormap    = fcmap;%myCmap;
cfg.opacitylim     = 'auto';%[0.0 1.2];
cfg.opacitymap     = 'auto';  
cfg.surfdownsample = [20];  % downsample to speed up processing
cfg.surffile       = 'surface_white_both.mat'; % Cortical sheet from canonical MNI brain
% cfg.surfinflated = 'surface_inflated_both.mat';
cfg.camlight = 'no'; % illumination of the brain
cfg.projmethod = 'sphere_avg';
cfg.sphereradius = 5;
ft_sourceplot(cfg, sourceCorr);
view ([90 0])             % rotate the object in the view
title(sprintf('Conjunction of %s and %s',name1, name2))
print([dest '/RightView'],'-dpng','-r300');

view ([0 90])             % rotate the object in the view
print([dest '/TopView'],'-dpng','-r300');

view ([-180 -90])             % rotate the object in the view
print([dest '/BottomView'],'-dpng','-r300');

view ([-90 0])             % rotate the object in the view
print([dest '/LeftView'],'-dpng','-r300');

view ([90 0])             % rotate the object in the view
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
print([dest '/Colorbar'],'-dpng','-r300');


cfg.surffile       = 'surface_white_right.mat'; % Cortical sheet from canonical MNI brain
ft_sourceplot(cfg, sourceCorr);
view ([-90 0])             % rotate the object in the view
title(sprintf('%s and %s',name1,name2))
print([dest '/LH_LeftView'],'-dpng','-r300');

cfg.surffile       = 'surface_white_left.mat'; % Cortical sheet from canonical MNI brain
ft_sourceplot(cfg, sourceCorr);
view ([90 0])             % rotate the object in the view
title(sprintf('%s and %s',name1,name2))
print([dest '/RH_RightView'],'-dpng','-r300');

myCorrMat = sourceCorr;

end