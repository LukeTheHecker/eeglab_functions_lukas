function ClustStruc=cluster_results(tablename, posOrNeg, clustCorrect)
%% example: ClustStruc = cluster_results(LattROI1p05, 1, 1)
% cluster the voxels in LattROI1p05, look only at positive (U>A) effects
% and correc cluster values for multiple comparisons

myLocsAct=table2array(tablename(1:end,[1:3 7]));
if posOrNeg == 1
    myLocsAct(find(myLocsAct(:,4)<0),:) = [];
elseif posOrNeg == 0
    myLocsAct(find(myLocsAct(:,4)>0),:) = [];
end

% plot all data
figure;
scatter3(myLocsAct(:,1),myLocsAct(:,2), myLocsAct(:,3))
xlabel('X')
ylabel('Y')
zlabel('Z')
rotate3d
axis vis3d
title('All significant Voxels')
% plot 'top'% of the data
top=0.1;
if round(length(myLocsAct)*0.1)<100
    top = 100/length(myLocsAct);
end
    
figure;
smyLocsAct = sort(myLocsAct,4);
selection = smyLocsAct(1:round(top*length(smyLocsAct)),:);
scatter3(selection(:,1),selection(:,2), selection(:,3),'*k')
xlabel('X')
ylabel('Y')
zlabel('Z')
rotate3d
axis vis3d
title(sprintf('Top %d %%',round(top*100)))

%% kmeans on top 10%
cidx=[];
cmeans=[];
silh=[];
sizeFit=zeros(1,7);
% find out good cluster size based on mean distance
for i=3:8
    [cidx,cmeans] = kmeans(selection(:,1:3),i,'dist','sqeuclidean', 'replicates',100);
    silh = silhouette(selection,cidx,'sqeuclidean');
    fprintf('%d clusters = %d', i, mean(silh(i)))
    fprintf('\n')
    sizeFit(i-2)=mean(silh(i));
end
nClusts=find(sizeFit==max(sizeFit))+2;
fprintf('Choosing %d Clusters', nClusts)

if clustCorrect == 1
    tcrit = abs(tinv(1-(1-(0.05/nClusts)),20));
    selection(find(abs(selection(:,4))<tcrit),:) = [];
    myLocsAct(find(abs(myLocsAct(:,4))<tcrit),:) = [];
end

[cidx,cmeans] = kmeans(selection(:,1:3),nClusts,'dist','sqeuclidean', 'replicates',100);
[cidx,cmeans] = kmeans(myLocsAct(:,1:3),nClusts,'dist','sqeuclidean','start',cmeans);
silh = silhouette(myLocsAct(:,1:3),cidx,'sqeuclidean');


figure;

ptsymb = {'bs','r^','md','go','c+', 'ko','y*', 'k^',};
for i = 1:nClusts
    clust = find(cidx==i);
    plot3(myLocsAct(clust,1),myLocsAct(clust,2),myLocsAct(clust,3),ptsymb{i});
    hold on
end
plot3(cmeans(:,1),cmeans(:,2),cmeans(:,3),'ko');
plot3(cmeans(:,1),cmeans(:,2),cmeans(:,3),'kx');
hold off
xlabel('Sepal Length');
ylabel('Sepal Width');
zlabel('Petal Length');
view(-137,10);
rotate3d
grid on
axis vis3d
title('Clusters with all Significant Voxels')

% sort 'myLocs' into spereate array clusters
for k = 1:nClusts
    disp(k)
    ClustStruc(k).name=['Cls ', num2str(k)];
    ClustStruc(k).data=myLocsAct(cidx==k,1:3);
    ClustStruc(k).nVoxels=length(myLocsAct(cidx==k));
    ClustStruc(k).centroid = cmeans(k,:);
    tmpDiff=ClustStruc(k).data-cmeans(k,:);
    distArr=NaN(1,ClustStruc(k).nVoxels);
    for i=1:ClustStruc(k).nVoxels 
        distArr(i)=sum(tmpDiff(i,:));
    end
    indx=find(distArr==min(distArr));
    ClustStruc(k).centroidOri = ClustStruc(k).data(indx(1),:);
    ClustStruc(k).centrBrodmann = tablename{indx(1),8};
    ClustStruc(k).centrLobe = char(tablename{indx(1),9});
    ClustStruc(k).centrStructure= char(tablename{indx(1),10});
    tVals=table2array(tablename(1:end,7));
    ClustStruc(k).lorMaxVox=myLocsAct(find(tVals==max(tVals(cidx==k))),1:3);
    ClustStruc(k).lorMaxBrodmann=tablename{find(tVals==max(tVals(cidx==k))),8};
    ClustStruc(k).lorMaxLobe=char(tablename{find(tVals==max(tVals(cidx==k))),9});
    ClustStruc(k).lorMaxStructure=char(tablename{find(tVals==max(tVals(cidx==k))),10});
end
end