%% Exclude Outlier max 3 if they deviate from median by 5 SD 
tmp_diff=STUDY.etc.FreqAnalysis.diff_UA;
cnt=0;
for repeat = 1:3
    for sub = 1:length(ALLEEG)
        for band = 1:length(STUDY.etc.FreqAnalysis.bands) 
            if max(abs(tmp_diff(sub,:,band)))>median(abs(tmp_diff(sub,:,band)))+5*std(tmp_diff(sub,:,band))
                cnt = cnt+1
                [M,I] = max(tmp_diff(sub,:,band));
                %I
                if I==22
                    disp('i get here')
                end
                if I>1 && I<32
                    tmp_diff(sub,I,band)=StupidMatlab(tmp_diff(sub,I-1,band),tmp_diff(sub,I+1,band));
                    %disp('goes if 1')
                else
                    tmp_diff(sub,I,band)= mean(tmp_diff(sub,:,band),2);
                    %disp('goes if 2')
                end
                %fprintf('Trimming Sub %s on electrode %s in band %s',ALLEEG(sub).subject, ALLEEG(1).chanlocs(I).labels, STUDY.etc.FreqAnalysis.bands(band))
            end
            %STUDY.etc.FreqAnalysis.bands(band)
        end
    end
end
