function d = cohensd(set1,set2)

diff = mean(set1)-mean(set2);
variance = sqrt((var(set1)+var(set2))/2);
d = diff/variance;
end