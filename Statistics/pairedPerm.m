function [p, obs_diff] = pairedPerm(set1, set2, nreps)
% calculates the p value with a paired-samples Permutation Test.
% function is based on this description:
% https://www.uvm.edu/~dhowell/StatPages/ResamplingWithR/RandomMatchedSample/RandomMatchedSampleR.html

if size(set1)~=size(set2)
    error('Dimension mismatch. Your sets of data did not have the same size!!')
elseif isnan(nreps) || nreps<10
    error('nreps seems to be a wrong number')
elseif length(size(set1)) ~= 2 || length(size(set2)) ~= 2 
    error('Size of set1 and set2 needs to be [nsubjects 1')
end

if length(set1(:,1)) < length(set1(1,:))
    set1=permute(set1,[2 1]);
end
if length(set2(:,1)) < length(set2(1,:))
    set2=permute(set2,[2 1]);
end

diffObt = mean(set1)-mean(set2);
difference = set1.'-set2.';
resampMeanDiff=NaN(1,nreps);
for i = 1:nreps
    signs = randsample([-1 1],length(difference), true);
    resamp = difference.*signs;
    resampMeanDiff(1,i)=mean(resamp);
end
diffObt = abs(diffObt);

highprob = length(resampMeanDiff(resampMeanDiff>=diffObt))/nreps;
lowprob = length(resampMeanDiff(resampMeanDiff<=diffObt*(-1)))/nreps;
p = lowprob+highprob;
obs_diff = diffObt;
end