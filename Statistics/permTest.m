% permTest

t_obs = abs(mean(y1)-mean(y2));

y_cellArr = { y1.' y2.'};

[stats, df, pvals, surrog] = statcond(y_cellArr, 'method', 'perm', 'naccu', 2000,'structoutput','on');

