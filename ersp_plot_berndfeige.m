amb=squeeze(mean(cell2mat(erspdata(1)),4));
unamb=squeeze(mean(cell2mat(erspdata(2)),4));

figure;
subplot(3,1,1)
imagesc(unamb-amb)
title('Unamb-Amb ERSP at Fp1')

subplot(3,1,2) % pos
pos = unamb-amb;
pos(pos<0)=0;
plot(-500:2000/170:1499, mean(pos,1),'r')

subplot(3,1,3) % neg
neg = unamb-amb;
neg(neg>0)=0;
plot(-500:2000/170:1499, mean(neg,1),'b')