sublist = ["AA", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV"];

pVals_LD_latrev_Cz = zeros(21, 700);
diffs_LD_latrev_Cz = zeros(21, 700);
effects_LD_latrev_Cz = zeros(21, 700);

for i=1:length(sublist)
    try
    sublist(i)
    EEG_s = pop_loadset(char(strcat(sublist(i),'_UO_STAB_filtered_eventcorr_mastref_noartifact.set')), 'C:\Users\Lukas Hecker\Data_Analysis\Attention\ecvp\mastoid\LD');
    EEG_s = pop_epoch(EEG_s, {'stability'}, [-0.3 1.1]);
    data_A = EEG_s.data;
    EEG_r = pop_loadset(char(strcat(sublist(i),'_UO_STAB_filtered_eventcorr_mastref_noartifact.set')), 'C:\Users\Lukas Hecker\Data_Analysis\Attention\ecvp\mastoid\LD');
    EEG_r = pop_epoch(EEG_r, {'rev_lat'}, [-0.3 1.1]);
    data_U = EEG_r.data;
    ALLEEG = [EEG_s, EEG_r];
    trials = min(length(data_U(1,1,:)),length(data_A(1,1,:)));
    for chan_cnt=14:14  % ALLEEG(1).nbchan
        for pnts_cnt = 1:ALLEEG(1).pnts
            a_tmp = zeros(1,trials);
            u_tmp = zeros(1,trials);
            a_tmp(:) =  data_A(chan_cnt,pnts_cnt,1:trials);
            u_tmp(:) =  data_U(chan_cnt,pnts_cnt,1:trials);
            [p, observeddifference, effectsize]=permutationTest(a_tmp, u_tmp, 5000 );%, 'plotresult',1, 'showprogress', 250 );
            pVals_LD_latrev_Cz(i, pnts_cnt)=p;
            diffs_LD_latrev_Cz(i, pnts_cnt)=observeddifference;
            effects_LD_latrev_Cz(i, pnts_cnt)=effectsize;
            pnts_cnt
        end
    end
    catch
        disp('catch')
    end
end


sublist = ["AA", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV"];

pVals_SD_latrev_Cz = zeros(21, 700);
diffs_SD_latrev_Cz = zeros(21, 700);
effects_SD_latrev_Cz = zeros(21, 700);

for i=1:length(sublist)
    try
    sublist(i)
    EEG_s = pop_loadset(char(strcat(sublist(i),'_EO_STAB_filtered_eventcorr_mastref_noartifact.set')), 'C:\Users\Lukas Hecker\Data_Analysis\Attention\ecvp\mastoid\SD\');
    EEG_s = pop_epoch(EEG_s, {'stability'}, [-0.3 1.1]);
    data_A = EEG_s.data;
    EEG_r = pop_loadset(char(strcat(sublist(i),'_EO_STAB_filtered_eventcorr_mastref_noartifact.set')), 'C:\Users\Lukas Hecker\Data_Analysis\Attention\ecvp\mastoid\SD');
    EEG_r = pop_epoch(EEG_r, {'rev_lat'}, [-0.3 1.1]);
    data_U = EEG_r.data;
    ALLEEG = [EEG_s, EEG_r];
    trials = min(length(data_U(1,1,:)),length(data_A(1,1,:)));
    for chan_cnt=14:14  % ALLEEG(1).nbchan
        for pnts_cnt = 1:ALLEEG(1).pnts
            a_tmp = zeros(1,trials);
            u_tmp = zeros(1,trials);
            a_tmp(:) =  data_A(chan_cnt,pnts_cnt,1:trials);
            u_tmp(:) =  data_U(chan_cnt,pnts_cnt,1:trials);
            [p, observeddifference, effectsize]=permutationTest(a_tmp, u_tmp, 5000 );%, 'plotresult',1, 'showprogress', 250 );
            pVals_SD_latrev_Cz(i, pnts_cnt)=p;
            diffs_SD_latrev_Cz(i, pnts_cnt)=observeddifference;
            effects_SD_latrev_Cz(i, pnts_cnt)=effectsize;
            pnts_cnt
        end
    end
    catch
        disp("catch")
    end
end

