%Here you can export all subjects of a given condition to loreta erp and
%chanlocs
%sublist = ["AA", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV"];
sublist = ["AA", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV"];

root = 'D:\Bibliotheken\Attention\processed\UA_Smileys_freq\';
cnt=0;
for i= 1:length(sublist)
    try
        d = dir([root, char(strcat(sublist(i), '_MO.set'))]);
        EEG = pop_loadset('filename',{d.name},'filepath',root);
        EEG = pop_epoch(EEG, {'amb_stability', 'amb_reversal'}, [-0.3 1.1], 'newname', strcat(EEG.setname, '_STAB'), 'epochinfo', 'yes');
        EEG = eeg_checkset( EEG);
        eeglab2loreta(EEG.chanlocs, mean(EEG.data(1:32,:,:),3),'exporterp', 'on', 'filecomp', char(strcat(EEG.setname(1:2),'_', EEG.setname(4:5), '')));
    catch
        disp('no file or epochs found')
        cnt = cnt+1;
    end
end

disp(['done. ', num2str(cnt), ' file exports were not successfull'])