% signal (time 40:800)
[STUDY erpdata_signal erptimes_signal pgroup pcond pinter] = std_erpplot(STUDY, ALLEEG);
signal_Allsubs = permute((erpdata_signal{1}+erpdata_signal{2})/2,[2 1 3]);
% noise (time -60:40)
[STUDY erpdata_noise erptimes_noise pgroup pcond pinter] = std_erpplot(STUDY, ALLEEG);
noise_Allsubs = permute((erpdata_noise{1}+erpdata_noise{2})/2,[2 1 3]);

% get Grand Mean signal ERP
GM_signal = squeeze(mean(signal_Allsubs.^2,3));
% get Grand Mean noise
GM_noise = squeeze(mean(noise_Allsubs.^2,3));

mean_GM_signal = mean(GM_signal,2);
mean_GM_noise = mean(GM_noise,2);
mean(mean_GM_signal./mean_GM_noise)

sub_SNRs = NaN(1,21);
for i = 1:21
    sub_SNRs(i) = mean(mean(signal_Allsubs(:,:,i).^2,2))./mean(mean(noise_Allsubs(:,:,i).^2,2));
end
mean(sub_SNRs)

% GM P400 peak / var
xrange = 40:4:800;
p400 = 344;
p200 = 192;
n170 = 180;
P204 = 204;
P344 = 344;
P188 = 188;


abs_std = mean(abs(GM_signal(:,find(xrange==p400)-10:find(xrange==p400)+10)),2)./mean(((std(signal_Allsubs(:,find(xrange==p400)-10:find(xrange==p400)+10,:),[],3))/sqrt(21)),2);
% P400
max(abs_std)
% P200
abs_std = mean(abs(GM_signal(:,find(xrange==p200)-10:find(xrange==p200)+10)),2)./mean(((std(signal_Allsubs(:,find(xrange==p200)-10:find(xrange==p200)+10,:),[],3))/sqrt(21)),2);
max(abs_std)
% N170
abs_std = mean(abs(GM_signal(:,find(xrange==n170)-10:find(xrange==n170)+10)),2)./mean(((std(signal_Allsubs(:,find(xrange==n170)-10:find(xrange==n170)+10,:),[],3))/sqrt(21)),2);
max(abs_std)

% P188
abs_std = mean(abs(GM_signal(:,find(xrange==P188)-10:find(xrange==P188)+10)),2)./mean(((std(signal_Allsubs(:,find(xrange==P188)-10:find(xrange==P188)+10,:),[],3))/sqrt(21)),2);
max(abs_std)
% P344
abs_std = mean(abs(GM_signal(:,find(xrange==P344)-10:find(xrange==P344)+10)),2)./mean(((std(signal_Allsubs(:,find(xrange==P344)-10:find(xrange==P344)+10,:),[],3))/sqrt(21)),2);
max(abs_std)
% P204
abs_std = mean(abs(GM_signal(:,find(xrange==P204)-10:find(xrange==P204)+10)),2)./mean(((std(signal_Allsubs(:,find(xrange==P204)-10:find(xrange==P204)+10,:),[],3))/sqrt(21)),2);
max(abs_std)

% signal^2 / var()

% N170
abs_std = mean(abs(GM_signal(:,find(xrange==n170)-10:find(xrange==n170)+10)).^2,2)./mean(var(signal_Allsubs(:,find(xrange==n170)-10:find(xrange==n170)+10,:),[],3),2);
max(abs_std)
% p200
abs_std = mean(abs(GM_signal(:,find(xrange==p200)-10:find(xrange==p200)+10)).^2,2)./mean(var(signal_Allsubs(:,find(xrange==p200)-10:find(xrange==p200)+10,:),[],3),2);
max(abs_std)
% p400
abs_std = mean(abs(GM_signal(:,find(xrange==p400)-10:find(xrange==p400)+10)).^2,2)./mean(var(signal_Allsubs(:,find(xrange==p400)-10:find(xrange==p400)+10,:),[],3),2);
max(abs_std)



% P188
abs_std = mean(abs(GM_signal(:,find(xrange==P188)-10:find(xrange==P188)+10)).^2,2)./mean(var(signal_Allsubs(:,find(xrange==P188)-10:find(xrange==P188)+10,:),[],3),2);
max(abs_std)
% P344
abs_std = mean(abs(GM_signal(:,find(xrange==P344)-10:find(xrange==P344)+10)).^2,2)./mean(var(signal_Allsubs(:,find(xrange==P344)-10:find(xrange==P344)+10,:),[],3),2);
max(abs_std)
% P204
abs_std = mean(abs(GM_signal(:,find(xrange==P204)-10:find(xrange==P204)+10)).^2,2)./mean(var(signal_Allsubs(:,find(xrange==P204)-10:find(xrange==P204)+10,:),[],3),2);
max(abs_std)


