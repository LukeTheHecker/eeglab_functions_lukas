sublist = ["AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV"];
root = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/processed/UA_Mouths/AMICA/';

for k = 1: length(sublist)    
    
    d = dir([root, char(strcat(sublist(k), '_*M_mrge1_STAB_filtered_eventcorr_avgref_ORI_noartifact_AMICA_Dipfit.set'))]);
    EEG_U = pop_loadset(d(1).name, d(1).folder);
    EEG_A = pop_loadset(d(2).name, d(2).folder);
    for l = 1:length(EEG_U.event)
        if isequal(EEG_U.event(l).type,'stability')
            EEG_U.event(l).type = 'unamb_stability';
        end
    end
    for l = 1:length(EEG_A.event)
        if isequal(EEG_A.event(l).type, 'stability')
            EEG_A.event(l).type = 'amb_stability';
        end
    end   
    ALLEEG_new = [EEG_U EEG_A];
    merge = pop_mergeset(ALLEEG_new, [1 2]);
    merge.icaweights = EEG_U.icaweights;
    merge.icasphere = EEG_U.icasphere;
    merge = eeg_checkset(merge, 'ica');
    pop_saveset(merge,'filename', EEG_U.setname,'filepath', [root 'merged']);
end