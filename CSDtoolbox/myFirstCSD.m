% CSD stuff

for i = 1:length(ALLEEG(1).chanlocs)
    chans(i) = {ALLEEG(1).chanlocs(i).labels}
end
chans=chans.'
[M] = ExtractMontage ('10-5-System_Mastoids_EGI129.csd', chans);
MapMontage(M)
[G,H] = GetGH(M); %default: 4, otherwise e.g.: GetGH(M, 3)
D = mean(bandpowerTR,2);

X = CSD(D,G,H);
plot(X)
figure;
topoplot(X, ALLEEG(1).chanlocs)

% tst:
%     {'Fp1' }
%     {'Fp2' }
%     {'F7'  }
%     {'F3'  }
%     {'Fz'  }
%     {'F4'  }
%     {'F8'  }
%     {'FC5' }
%     {'FC1' }
%     {'FC2' }
%     {'FC6' }
%     {'T7'  }
%     {'C3'  }
%     {'Cz'  }
%     {'C4'  }
%     {'T8'  }
%     {'TP9' }
%     {'CP5' }
%     {'CP1' }
%     {'CP2' }
%     {'CP6' }
%     {'TP10'}
%     {'P7'  }
%     {'P3'  }
%     {'Pz'  }
%     {'P4'  }
%     {'P8'  }
%     {'PO9' }
%     {'O1'  }
%     {'Oz'  }
%     {'O2'  }
%     {'PO10'}