namestruc = dir(['/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/processed/UA_Lattices_01_120_CL_AR100_SASICA/SASICA/*.set']);
namestruc = dir(['/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/processed/UA_Smileys_01_120_CL_AR100_SASICA/SASICA/*.set']);

%handselected lattices:
namestruc = dir(['/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/processed/UA_Lattices_01_120_CL_AR100_SASICA/Handselected/*.set']);



%entries = cell(1,length(namestruc)+1);
entries = cell(1,length(namestruc));

for i = 1:length(namestruc)
    entries{i} = {'index' i 'load' [namestruc(i).folder '/' namestruc(i).name] 'subject' namestruc(i).name(1:2) 'condition' 'Lattices'};
end
% entries{i+1}={ 'dipselect' 0.15 };
% Create STUDY
[STUDY ALLEEG] = std_editset( STUDY, [], 'commands', entries);
% Save STUDY
STUDY = pop_savestudy(STUDY, EEG, 'filename', 'allSets', 'filepath',namestruc(1).folder );
% Make design combining stab&rev
STUDY = std_makedesign(STUDY, ALLEEG, 1, 'variable1','type','variable2','','name','STUDY.design 1','pairing1','on','pairing2','on','delfiles','off','defaultdesign','off','values1',{{'amb_reversal' 'amb_stability'} {'unamb_reversal' 'unamb_stability'}},'subjselect',{'AA' 'AC' 'AD' 'AE' 'AF' 'AG' 'AH' 'AI' 'AJ' 'AK' 'AL' 'AM' 'AN' 'AO' 'AP' 'AQ' 'AR' 'AS' 'AT' 'AU' 'AV'});
% Make design separating stab&rev
STUDY = std_makedesign(STUDY, ALLEEG, 1, 'variable1','type','variable2','','name','STUDY.design 1','pairing1','on','pairing2','on','delfiles','off','defaultdesign','off','values1',{'amb_reversal' 'amb_stability' 'unamb_reversal' 'unamb_stability'},'subjselect',{'AA' 'AC' 'AD' 'AE' 'AF' 'AG' 'AH' 'AI' 'AJ' 'AK' 'AL' 'AM' 'AN' 'AO' 'AP' 'AQ' 'AR' 'AS' 'AT' 'AU' 'AV'});
% resave
STUDY = pop_savestudy(STUDY, EEG, 'savemode', 'resave');
% load it
[STUDY ALLEEG] = pop_loadstudy('filename', 'allSets.study', 'filepath', namestruc(1).folder);

% precompute ersp from 8 to 120 hz

STUDY = pop_statparams(STUDY, 'method','param','alpha',NaN,'condstats','on');
% [STUDY, ALLEEG] = std_precomp(STUDY, ALLEEG, {},'interp','on','recompute','on','ersp','on','erspparams',{'freqs' [8 120]  'cycles' [4.6 0.2]  'nfreqs' 120 'ntimesout' 200, 'baseline', [-200 -100]},'itc','on');

% precompute ersp from 5 to 40 hz
[STUDY, ALLEEG] = std_precomp(STUDY, ALLEEG, {},'interp','on','recompute','on','ersp','on','erspparams',{'freqs' [5 40]  'cycles' [3 0.8]  'nfreqs' 35 'ntimesout' 200, 'baseline', [-300 -100]},'itc','on');

STUDY = pop_savestudy(STUDY, EEG, 'savemode', 'resave');


[STUDY erspdata ersptimes erspfreqs pgroup ersppcond pinter] = std_erspplot(STUDY,ALLEEG,'channels',{ALLEEG(1).chanlocs.labels}, 'plotsubjects', 'on','noplot','on');
[STUDY itcdata itctimes itcfreqs itcpgroup itcpcond itcpinter] = std_itcplot(STUDY,ALLEEG,'channels',{ALLEEG(1).chanlocs.labels}, 'plotsubjects', 'on','noplot','on');

% save ersp
STUDY.etc.timef.ersptimes = ersptimes;
STUDY.etc.timef.ersp = erspdata;
STUDY.etc.timef.erspfreq = erspfreqs;
STUDY.etc.timef.ersp_pcond = ersppcond;
% save itc
STUDY.etc.timef.itctimes = itctimes;
STUDY.etc.timef.itc = itcdata;
STUDY.etc.timef.itcfreqs = itcfreqs;
STUDY.etc.timef.itc_pcond = itcpcond;

STUDY = pop_savestudy(STUDY, EEG, 'savemode', 'resave');

% % precompute alpha and gamma ersp
% [STUDY ALLEEG] = std_precomp(STUDY, ALLEEG, {},'interp','on','recompute','on','ersp','on','erspparams',{'freqs' [8 13]  'cycles' [3 1]  'nfreqs' 5 'ntimesout' 200},'itc','on');
% [STUDY, erspdata, ersptimes, erspfreqs, pgroup, pcond, pinter] = std_erspplot(STUDY,ALLEEG,'channels',{ALLEEG(1).chanlocs.labels}, 'plotsubjects', 'on','noplot','on');
% [STUDY, itcdata, itctimes, itcfreqs, pgroup, pcond, pinter] = std_itcplot(STUDY,ALLEEG,'channels',{ALLEEG(1).chanlocs.labels}, 'plotsubjects', 'on','noplot','on');
% % save ersp
% STUDY.etc.timef.ersptimesalpha = ersptimes;
% STUDY.etc.timef.erspalpha = erspdata;
% STUDY.etc.timef.erspfreqsalpha = erspfreqs;
% % save itc
% STUDY.etc.timef.itctimesalpha = itctimes;
% STUDY.etc.timef.itcalpha = itcdata;
% STUDY.etc.timef.itcfreqsalpha = itcfreqs;
% 
% [STUDY ALLEEG] = std_precomp(STUDY, ALLEEG, {},'interp','on','recompute','on','ersp','on','erspparams',{'freqs' [30 120]  'cycles' [5 0.3]  'nfreqs' 10 'ntimesout' 200},'itc','on');
% [STUDY, erspdata, ersptimes, erspfreqs, pgroup, pcond, pinter] = std_erspplot(STUDY,ALLEEG,'channels',{ALLEEG(1).chanlocs.labels}, 'plotsubjects', 'on','noplot','on');
% STUDY.etc.timef.ersptimesgamma = ersptimes;
% STUDY.etc.timef.erspgamma = erspdata;
% STUDY.etc.timef.erspfreqsgamma = erspfreqs;
% % save itc
% STUDY.etc.timef.itctimesgamma = itctimes;
% STUDY.etc.timef.itcgamma = itcdata;
% STUDY.etc.timef.itcfreqsgamma = itcfreqs;
% resave it