% This script helps to export all ICs within a certain Cluster to loreta.
% Loreta takes either ERPs or the inverse of the weight matrix (W^-1),
% i.e. the scalp maps as input. 

%Say which Cluster you want to export. Example: Cl 3 = 3
clust = 13;

for i = 1:length(STUDY.cluster(3).comps)
    disp('i: ')
    disp(i)
    setmat = cell2mat(STUDY.cluster(clust).setinds(1));%%
    set = setmat(i);
    component = STUDY.cluster(clust).comps(i);
    chanlocs = ALLEEG(1).chanlocs;
    data = ALLEEG(set).icawinv;
    eeglab2loreta( chanlocs, data, 'compnum', component, 'labelonly', 'on', 'filecomp', char(strcat(ALLEEG(set).subject,'_comp')));
end