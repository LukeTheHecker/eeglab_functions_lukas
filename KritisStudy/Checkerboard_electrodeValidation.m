
EEG = pop_chanedit(EEG, 'lookup','/Applications/eeglab14_1_2b/plugins/dipfit2.3/standard_BEM/elec/standard_1005.elc','eval','chans = pop_chancenter( chans, [],[]);');
EEG = pop_interp(EEG, [33  34], 'spherical');
EEG = pop_reref( EEG, [] );
EEG = pop_eegfiltnew(EEG, 'locutoff',0.1,'plotfreqz',0);

for i=1:numel(EEG.event)
    if isequal(EEG.event(i).type, 'S  1')
        for n=i:numel(EEG.event)
            if isequal(EEG.event(n).type, 'S  2')
                EEG.event(i-1).type = num2str(50*round((EEG.event(n).latency - EEG.event(i).latency)/50));
                break;
            end
        end
    end
end
trigs_unamb=["150", "50", "200","100" ];
for i=numel(EEG.event):-1:10
%         if i>15 && EEG.event(i).epoch > 1 && isequal(EEG.event(i).type , char(trigs_unamb(1))) || isequal(EEG.event(i).type , char(trigs_unamb(3))) 
    if i>15 && isequal(EEG.event(i).type , char(trigs_unamb(1))) || isequal(EEG.event(i).type , char(trigs_unamb(3))) 
                EEG.event(i).type = 'LargeFrequent';
                
    elseif i>15 && isequal(EEG.event(i).type , char(trigs_unamb(2))) || isequal(EEG.event(i).type , char(trigs_unamb(4))) 
                EEG.event(i).type = 'SmallRare';
    end
end
