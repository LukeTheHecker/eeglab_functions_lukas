sublist = ["AA", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV"];
path = 'C:\Users\Lukas Hecker\Data_Analysis\Attention\processed\Unambig_Lattices\'
filename = '_UO_01_filtered_eventcorr_avgref_epochs_noartifact_mrge_TwoDipoles.set'

for i = 1:length(sublist)
    ALLEEG(i) = pop_loadset('filename', char(strcat(sublist(i),filename)), 'filepath',path);
    ALLEEG(i) = eeg_checkset(ALLEEG);
end
