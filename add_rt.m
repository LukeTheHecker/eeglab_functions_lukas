%figure; pop_erpimage(EEG,1, [14],[[]],'Cz',10,1,{},[],'' ,'yerplabel','\muV','erp','on','cbar','on','topo', { [14] EEG.chanlocs EEG.chaninfo } );

R1= 'R  1';
R2= 'R128';

for j=numel(EEG.event):-1:10
    %         if i>15 && EEG.event(i).epoch > 1 && isequal(EEG.event(i).type , char(trigs(1))) || isequal(EEG.event(i).type , char(trigs(3))) 
        if j>15 && isequal(EEG.event(j).type , R1) || isequal(EEG.event(j).type , R2) 
            for b=j-1:-1:j-10
                if isequal(EEG.event(b).type , 'stability') || isequal(EEG.event(j).type , 'reversal')
                    EEG.event(b).rt = (EEG.event(j).latency - EEG.event(b).latency);
                    break;
                end
            end
        end
end

% diff_EEG = EEG;
% trials = min(ALLEEG(6).trials, ALLEEG(26).trials)
% diff_EEG.data =
for i=1:length(ALLEEG)
    for j=numel(ALLEEG.event):-1:10
    %         if i>15 && EEG.event(i).epoch > 1 && isequal(EEG.event(i).type , char(trigs(1))) || isequal(EEG.event(i).type , char(trigs(3))) 
        if j>15 && isequal(ALLEEG(i).event(j).type , R1) || isequal(ALLEEG(i).event(j).type , R2) 
            for b=j-1:-1:j-10
                if isequal(ALLEEG(i).event(b).type , 'stability') || isequal(ALLEEG(i).event(j).type , 'reversal')
                    ALLEEG(i).event(b).rt = (ALLEEG(i).event(j).latency - ALLEEG(i).event(b).latency);
                    break;
                end
            end
        end
    end
    if mod(i,2) ==0
        figure; pop_erpimage(ALLEEG(i),1, [14],[[]],char(ALLEEG(i).setname),10,1,{ 'reversal' 'stability'},[],'rt' ,'yerplabel','\muV','erp','on','cbar','on','topo', { [14] ALLEEG(i).chanlocs ALLEEG(i).chaninfo } );
    end
end