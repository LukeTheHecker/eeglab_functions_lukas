%This script will apply all necessary preprocessing steps to all subjects
function prepro_attention_rLock(mypath, dest, datastruct, trigs)

addpath('C:\Program Files (x86)\eeglab14_1_2b\myfunctions')

mypath=char(mypath);
EEG = [];
for i = 1:length(datastruct)
    EEG = [EEG, pop_loadbv(mypath, char(datastruct(i).name))];
    EEG(i).setname= strcat(EEG(i).comments(19:21),EEG(i).comments(16:18), EEG(i).comments(22:23));
    EEG(i).filepath = mypath;
end
EEG = eeg_checkset( EEG );
setName = EEG(1).setname(1:5);
EEG = pop_mergeset(EEG, [1:length(EEG)]);
EEG.setname = setName;

%event correction
for i=1:numel(EEG.event)
    if isequal(EEG.event(i).type, 'S  1')
        for n=i:numel(EEG.event)
            if isequal(EEG.event(n).type, 'S  2')
                EEG.event(i-1).type = num2str(50*round((EEG.event(n).latency - EEG.event(i).latency)/50));
                break;
            end
        end
    end
end

% Stab/Rev Unterteilung 
% (in D:\Bibliotheken\Attention\processed\Unambig_Smileys\AMICA\stabRev)

% for i=numel(EEG.event):-1:10
% %         if i>15 && EEG.event(i).epoch > 1 && isequal(EEG.event(i).type , char(trigs(1))) || isequal(EEG.event(i).type , char(trigs(3))) 
%     if i>15 && isequal(EEG.event(i).type , char(trigs(1))) || isequal(EEG.event(i).type , char(trigs(3))) 
%         for b=i-1:-1:i-10
%             if isequal(EEG.event(b).type , char(trigs(2))) || isequal(EEG.event(i).type , char(trigs(4)))
%                 EEG.event(i).type = 'reversal';
%                 break;
%             elseif isequal(EEG.event(b).type , char(trigs(1))) || isequal(EEG.event(i).type , char(trigs(3)))
%                 EEG.event(i).type = 'stability';
%                 break;
%             end
%         end
%     elseif i>15 && isequal(EEG.event(i).type , char(trigs(2))) || isequal(EEG.event(i).type , char(trigs(4))) 
%         for b=i-1:-1:i-10
%             if isequal(EEG.event(b).type , char(trigs(1))) || isequal(EEG.event(b).type , char(trigs(3)))
%                 EEG.event(i).type = 'reversal';
%                 break;
%             elseif isequal(EEG.event(b).type , char(trigs(2))) || isequal(EEG.event(b).type , char(trigs(4)))
%                 EEG.event(i).type = 'stability';
%                 break;
%             end
%         end   
%     end
% end
% All stable (fix and background) (for LD,SD):
% EEG.event(:).rt = deal(zeros());
cnt=0;
cnt2=0;
for i=numel(EEG.event):-1:10
%         if i>15 && EEG.event(i).epoch > 1 && isequal(EEG.event(i).type , char(trigs(1))) || isequal(EEG.event(i).type , char(trigs(3))) 
    if i>15 && isequal(EEG.event(i).type , char(trigs(1))) || isequal(EEG.event(i).type , char(trigs(2))) || isequal(EEG.event(i).type , char(trigs(3))) || isequal(EEG.event(i).type , char(trigs(4)))
        currTrig = EEG.event(i).type;
        for b=i-1:-1:i-10
            if isequal(EEG.event(b).type , char(currTrig)) 
                EEG.event(i).type = 'stability';
                break;
            elseif (isequal(EEG.event(i).type , char(trigs(1))) || isequal(EEG.event(i).type , char(trigs(2))) || isequal(EEG.event(i).type , char(trigs(3))) || isequal(EEG.event(i).type , char(trigs(4)))) && ~~isequal(EEG.event(b).type , char(currTrig))
                EEG.event(i).type = 'reversal';
            end
        end
    end
end
for l=1:numel(EEG.event)
    if isequal(EEG.event(l).type , char(trigs(1))) || isequal(EEG.event(l).type , char(trigs(2))) || isequal(EEG.event(l).type , char(trigs(3))) || isequal(EEG.event(l).type , char(trigs(4)))
        EEG.event(l).type = 'reversal';
    end
end
for k=numel(EEG.event):-1:10
    if k>15 && isequal(EEG.event(k).type , 'stability')
        cnt = cnt+1;
        for m=k+1:1:length(EEG.event)
            if isequal(EEG.event(m).type, 'R  1') || isequal(EEG.event(m).type, 'R128') && ~isequal(EEG.event(m).type, 'reversal')
                cnt2 = cnt2+1;
                EEG.event(m).rt = EEG.event(m).latency - EEG.event(k).latency ;
                if EEG.event(m).rt<=500 && EEG.event(m).rt>=150 
                    EEG.event(m).type = 'response';
                    break;
                end
            end
        end
    end
end

EEG_STAB = pop_epoch(EEG, {'response'}, [-0.14 0.6], 'newname', strcat(EEG.setname, '_STAB'), 'epochinfo', 'yes');
EEG_STAB = eeg_checkset( EEG_STAB );
EEG_STAB = pop_editset(EEG_STAB, 'condition', char(strcat(EEG.condition,'S' )));

%Delete VEOG since it is not initially referenced to the same electrode
EEG_STAB = pop_select(EEG_STAB,'nochannel',33);
EEG_STAB = eeg_checkset(EEG_STAB);

%set subject code, condition and session
EEG_STAB = pop_editset(EEG_STAB, 'subject', EEG_STAB.setname(1:2), 'condition', EEG_STAB.setname(4:5));

%Downsample Only for the ICA-dataset
tmpname = EEG_STAB.setname;
EEG_STAB = pop_resample( EEG_STAB, 500);
EEG_STAB.setname=tmpname;


%filter HPF: 0.1Hz & 25 Hz LPF
EEG_STAB = pop_eegfiltnew(EEG_STAB, 0.2, 24.95, [], 0, [], 0);
EEG_STAB = eeg_checkset( EEG_STAB );

EEG_STAB.setname=strcat(EEG_STAB.setname, '_filtered_eventcorr');

%Edit Channels
EEG_STAB = pop_chanedit(EEG_STAB, 'lookup','C:\Program Files (x86)\eeglab14_1_2b\plugins\dipfit2.3\standard_BEM\elec\standard_1005.elc','eval','chans = pop_chancenter( chans, [],[]);');

%reref to avg
EEG_STAB.nbchan = EEG_STAB.nbchan+1;
EEG_STAB.data(end+1,:) = zeros(1, EEG_STAB.pnts*EEG_STAB.trials);
EEG_STAB.chanlocs(1,EEG_STAB.nbchan).labels = 'initialReference';
EEG_STAB = pop_reref( EEG_STAB, []);
EEG_STAB = pop_select( EEG_STAB,'nochannel',{'initialReference'});
EEG_STAB.setname=strcat(EEG_STAB.setname, '_avgref');

%Baseline removal for EEG_STAB - or not?
EEG_STAB = pop_rmbase( EEG_STAB, [-60  40]);

%Artifact rejection
EEG_STAB = pop_autorej(EEG_STAB, 'nogui','on','threshold',100,'eegplot','off','electrodes', 1:32);
EEG_STAB.setname=strcat(EEG_STAB.setname, '_noartifact');
EEG_STAB = pop_saveset( EEG_STAB, 'filename',EEG_STAB.setname,'filepath',dest);

clear('EEG')
clear('EEG_ORI')
end