%This script will apply all necessary preprocessing steps to all subjects
function prepro_attention(mypath, dest, datastruct, trigs)

addpath('C:\Program Files (x86)\eeglab14_1_2b\myfunctions')

mypath=char(mypath);
EEG = [];
for i = 1:length(datastruct)
    EEG = [EEG, pop_loadbv(mypath, char(datastruct(i).name))];
    EEG(i).setname= strcat(EEG(i).comments(19:21),EEG(i).comments(16:18), EEG(i).comments(22:23));
    EEG(i).filepath = mypath;
end
EEG = eeg_checkset( EEG );
setName = EEG(1).setname(1:5);
EEG = pop_mergeset(EEG, [1:length(EEG)]);
EEG.setname = setName;

%event correction
for i=1:numel(EEG.event)
    if isequal(EEG.event(i).type, 'S  1')
        for n=i:numel(EEG.event)
            if isequal(EEG.event(n).type, 'S  2')
                EEG.event(i-1).type = num2str(50*round((EEG.event(n).latency - EEG.event(i).latency)/50));
                break;
            end
        end
    end
end

% Stab/Rev Unterteilung 
if isequal(EEG.setname(4), 'A')
    cntR1=0;
    cntR128=0;
    for i=numel(EEG.event):-1:10
        if isequal(EEG.event(i).type,'R  1')
            cntR1= cntR1+1;
        elseif isequal(EEG.event(i).type, 'R128')
            cntR128 = cntR128+1;
        end
    end
    if cntR1>cntR128
        stabResp = 'R  1';
        revResp = 'R128';
    elseif cntR1<cntR128
        stabResp = 'R128';
        revResp = 'R  1';
    end
    for i=1:numel(EEG.event)
        if isequal(EEG.event(i).type , char(trigs(1))) || isequal(EEG.event(i).type , char(trigs(2))) || isequal(EEG.event(i).type , char(trigs(3))) || isequal(EEG.event(i).type , char(trigs(4)))
            for j=i+1:i+10
                if j<= numel(EEG.event) && isequal(EEG.event(j).type,stabResp)
                    EEG.event(i).type = 'stability_perc';
                    break;
                elseif  j<= numel(EEG.event) && isequal(EEG.event(j).type,revResp)
                    EEG.event(i).type = 'reversal_perc';
                end
            end
        end
    end
    EEG_STAB = pop_epoch(EEG, {'stability_perc', 'reversal_perc'}, [-0.3 1.1], 'newname', strcat(EEG.setname, '_epoched'), 'epochinfo', 'yes');


    
else
    for i=numel(EEG.event):-1:10
    %         if i>15 && EEG.event(i).epoch > 1 && isequal(EEG.event(i).type , char(trigs(1))) || isequal(EEG.event(i).type , char(trigs(3))) 
        if i>15 && isequal(EEG.event(i).type , char(trigs(1))) || isequal(EEG.event(i).type , char(trigs(2))) || isequal(EEG.event(i).type , char(trigs(3))) || isequal(EEG.event(i).type , char(trigs(4)))
            currTrig = EEG.event(i).type;
            for b=i-1:-1:i-10
                if isequal(EEG.event(b).type , char(currTrig)) 
                    EEG.event(i).type = 'stability';
                    break;
                elseif isequal(EEG.event(i).type, char(trigs(1)))
                    if b>0 && isequal(EEG.event(b).type , char(trigs(2)))
                        EEG.event(i).type = 'rev_lat';
                        break;
                    elseif b>0 && isequal(EEG.event(b).type , char(trigs(3)))
                        EEG.event(i).type = 'rev_fix';
                        break;
                    elseif b>0 && isequal(EEG.event(b).type , char(trigs(4)))
                        EEG.event(i).type = 'rev_both';
                        break;
                    end 
                elseif isequal(EEG.event(i).type, char(trigs(2)))
                    if b>0 && isequal(EEG.event(b).type , char(trigs(1)))
                        EEG.event(i).type = 'rev_lat';
                        break;
                    elseif b>0 && isequal(EEG.event(b).type , char(trigs(3)))
                        EEG.event(i).type = 'rev_both';
                        break;
                    elseif b>0 && isequal(EEG.event(b).type , char(trigs(4)))
                        EEG.event(i).type = 'rev_fix';
                        break;
                    end
                elseif isequal(EEG.event(i).type, char(trigs(3)))
                    if b>0 && isequal(EEG.event(b).type , char(trigs(1)))
                        EEG.event(i).type = 'rev_fix';
                        break;
                    elseif b>0 && isequal(EEG.event(b).type , char(trigs(2)))
                        EEG.event(i).type = 'rev_both';
                        break;
                    elseif b>0 && isequal(EEG.event(b).type , char(trigs(4)))
                        EEG.event(i).type = 'rev_lat';
                        break;
                    end
                elseif isequal(EEG.event(i).type, char(trigs(4)))
                    if b>0 && isequal(EEG.event(b).type , char(trigs(1)))
                        EEG.event(i).type = 'rev_both';
                        break;
                    elseif b>0 && isequal(EEG.event(b).type , char(trigs(2)))
                        EEG.event(i).type = 'rev_fix';
                        break;
                    elseif b>0 && isequal(EEG.event(b).type , char(trigs(3)))
                        EEG.event(i).type = 'rev_lat';
                        break;
                    end
                end
            end
        end
    end
    EEG_STAB = pop_epoch(EEG, {'stability', 'rev_both', 'rev_fix', 'rev_lat'}, [-0.3 1.1], 'newname', strcat(EEG.setname, '_epoched'), 'epochinfo', 'yes');
end


EEG_STAB = eeg_checkset( EEG_STAB );

%Delete VEOG since it is not initially referenced to the same electrode
EEG_STAB = pop_select(EEG_STAB,'nochannel',33);
EEG_STAB = eeg_checkset(EEG_STAB);

%set subject code, condition and session
EEG_STAB = pop_editset(EEG_STAB, 'subject', EEG_STAB.setname(1:2), 'condition', EEG_STAB.setname(4:5));

%Downsample Only for the ICA-dataset
tmpname = EEG_STAB.setname;
EEG_STAB = pop_resample( EEG_STAB, 500);
EEG_STAB.setname=tmpname;


%filter HPF: 0.1Hz & 25 Hz LPF
EEG_STAB = pop_eegfiltnew(EEG_STAB, 0.2, 24.9, [], 0, [], 0);
EEG_STAB = eeg_checkset( EEG_STAB );

EEG_STAB.setname=strcat(EEG_STAB.setname, '_filtered_eventcorr');

%Edit Channels
EEG_STAB = pop_chanedit(EEG_STAB, 'lookup','C:\Program Files (x86)\eeglab14_1_2b\plugins\dipfit2.3\standard_BEM\elec\standard_1005.elc','eval','chans = pop_chancenter( chans, [],[]);');

% reref to mastoid
EEG_STAB = pop_reref( EEG_STAB, [17 22]);
EEG_STAB.setname=strcat(EEG_STAB.setname, '_mastref');

%Baseline removal for EEG_STAB - or not?
EEG_STAB = pop_rmbase( EEG_STAB, [-60  40]);

%Artifact rejection
EEG_STAB = pop_autorej(EEG_STAB, 'nogui','on','threshold',100,'eegplot','off','electrodes', 1:EEG_STAB.nbchan);
EEG_STAB.setname=strcat(EEG_STAB.setname, '_noartifact');
EEG_STAB = pop_saveset( EEG_STAB, 'filename',EEG_STAB.setname,'filepath',dest);

clear('EEG')
clear('EEG_STAB')
end