sublist = ["AA", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV"];
sublist = ["AP", "AQ", "AR", "AS", "AT", "AU", "AV"];



root= strings(1,length(sublist));
% for UO: 
%triggers = ["500"; "550"; "650"; "700"];
% for AO:
%triggers = ["600"; "750"; "600"; "750"];
% for EF:
%triggers = ["100"; "150"; "300"; "350"];
% for MF:
%triggers = ["200";"250";"400";"450"];



for i=1:length(sublist)
    root(1,i) = strcat("C:\\Users\\Lukas Hecker\\Data_Analysis\\Attention\\AllSubs\\",sublist(i));
end

% for UO:
triggers = ["500"; "550"; "650"; "700"];
dest = 'C:\Users\Lukas Hecker\Data_Analysis\Attention\ecvp\mastoid\LD\';
for i=1:length(sublist)
    d = dir([char(root(i)), '\UO_*.vhdr']);
    try    
        prepro_attention(root(1,i), dest, d, triggers);
    catch
         disp('File was not found, I will go on though')
    end

end

% for AO:
triggers = ["600"; "750"; "600"; "750"];
dest = 'C:\Users\Lukas Hecker\Data_Analysis\Attention\ecvp\mastoid\LA\';
for i=1:length(sublist)
    d = dir([char(root(i)), '\AO_*.vhdr']);
    %try    
        prepro_attention(root(1,i), dest, d, triggers);
    %catch
    %     disp('File was not found, I will go on though')
    %end

end

% for EO: 
triggers = ["100"; "150"; "300"; "350"];
dest = 'C:\Users\Lukas Hecker\Data_Analysis\Attention\ecvp\mastoid\SD\';
for i=1:length(sublist)
    d = dir([char(root(i)), '\EO_*.vhdr']);
    try    
        prepro_attention(root(1,i), dest, d, triggers);
    catch
         disp('File was not found, I will go on though')
    end

end

% for MO:
triggers = ["200";"250";"400";"450"];
dest = 'C:\Users\Lukas Hecker\Data_Analysis\Attention\ecvp\mastoid\SA\';
for i=1:length(sublist)
    d = dir([char(root(i)), '\MO_*.vhdr']);
    try    
        prepro_attention(root(1,i), dest, d, triggers);
    catch
         disp('File was not found, I will go on though')
    end

end
