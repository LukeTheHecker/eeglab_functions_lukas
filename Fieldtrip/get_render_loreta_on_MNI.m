%% t-stat, sw-normed, & BLC tROI-based:
% at p<0.05 FWE two-sided
t_crit_two = [4.189,3.919,4.195,4.224,4.037...
    4.256,4.133,4.383,4.123,4.16];
%% t-stat,sw-normed Peak-based:
% Using exceedence Proportion Test
t_LDS_LAS_P200_pos = 0.473207;
t_LDS_LAS_P200_neg = NaN;
t_LDS_LAS_P400_pos = 0.569601;
t_LDS_LAS_P400_neg = -1.250110;

t_LDR_LAR_P200_pos = 1.068080;
t_LDR_LAR_P200_neg = NaN;
t_LDR_LAR_P400_pos = 1.219576;
t_LDR_LAR_P400_neg = -0.474245;

t_SDS_SAS_P200_pos = 1.813717;
t_SDS_SAS_P200_neg = NaN;
t_SDS_SAS_P400_pos = 1.418907;
t_SDS_SAS_P400_neg = -0.880334;

t_SDR_SAR_P200_pos = 0.405717;
t_SDR_SAR_P200_neg = NaN;
t_SDR_SAR_P400_pos = 0.872133;
t_SDR_SAR_P400_neg = -0.533816;

%%
dest = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/loreta_brainplots/Surfaces/Lat/Peaks/Urev_Arev/P400';

render_loreta_on_MNI('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/PeakAnalysis/forLoreta/Avg/stats/Rev_vs_Rev/P400-range/',...
    'LDR_LAR.slor',[t_LDR_LAR_P400_pos,t_LDR_LAR_P400_neg],2,dest)

%% Conjunction
path1='/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/PeakAnalysis/forLoreta/Avg/stats/Rev_vs_Rev/P400-range/';
name1 = 'SDR_SAR.slor';
path2='/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/PeakAnalysis/forLoreta/Avg/stats/Rev_vs_Rev/P400-range/';
name2 = 'LDR_LAR.slor';

dest = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/loreta_brainplots/Surfaces/Conjunction/Peaks/U-rev_A-rev/P400';

loreta_conjunction(path1,name1,[t_SDR_SAR_P400_pos,t_SDR_SAR_P400_neg],...
    path2,name2,[t_LDR_LAR_P400_pos,t_LDR_LAR_P400_neg],2,dest)


%% fMRI render
% 0009 = lat U>A 0010 = lat A>U
% 0011 = smi U>A 0012 = smi A>U
t_crit = 3.23; % Lat U>A uncorr
path = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/group_fullfactorial/';

file = 'spmT_0009.nii';
render_fMRI(path,file,t_crit,1)

%% fMRI render conjunction
% t_crit1 = 4.9; % Lat A>U FWE corrected 0.05
t_crit1 = 3.23;
path1 = path;
% file1 = 'spmT_0010.nii';
file1 =  'spmT_0009.nii';

%t_crit2 = 4.9; % Lat A>U FWE corrected 0.05
t_crit2 = t_crit1;
path2 = path;
% file2 = 'spmT_0012.nii';
file2 = 'spmT_0011.nii';
dest = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/fMRI_brainplots/Surfaces/Conjunction/BothInOne/';
render_fMRI_conjunction(path1,file1,t_crit1,path2,file2,t_crit2,2,dest)
