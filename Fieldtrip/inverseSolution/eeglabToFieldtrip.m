% addpath
addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/')
ft_defaults 

% create FT structure
for i = 1:length(ALLEEG)
    EEG_tmp = pop_loadset('filename', ALLEEG(i).filename, 'filepath',ALLEEG(i).filepath);
    try
        EEG_tmp_AR = pop_epoch(EEG_tmp, {'amb_reversal'},[-0.4 1]);
        ft_LAR(i) = eeglab2fieldtrip(EEG_tmp_AR,'timelockanalysis','none');

    catch
        disp('no trials here')
    end
    EEG_tmp_AS = pop_epoch(EEG_tmp, {'amb_stability'},[-0.4 1]);
    ft_LAS(i) = eeglab2fieldtrip(EEG_tmp_AS,'timelockanalysis','none');
    
    EEG_tmp_UR = pop_epoch(EEG_tmp, {'unamb_reversal'},[-0.4 1]);
    ft_LDR(i) = eeglab2fieldtrip(EEG_tmp_UR,'timelockanalysis','none');

    
    
    EEG_tmp_US = pop_epoch(EEG_tmp, {'unamb_stability'},[-0.4 1]);
    ft_LDS(i) = eeglab2fieldtrip(EEG_tmp_US,'timelockanalysis','none');
end
