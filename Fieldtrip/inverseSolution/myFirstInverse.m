addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/external/spm8/templates/')
mripath     = '/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/external/spm8/templates/';
subjectname = 'T1';
mri         = ft_read_mri(fullfile(mripath,sprintf('%s.nii',subjectname)));

mri = ft_determine_coordsys(mri, 'interactive', 'yes');

% cfg          = [];
% cfg.method   = 'spm';
% cfg.coordsys = 'ctf';
% mri          = ft_volumerealign(cfg, mri);

% Reslice to 256^3  (1mm isotropic)
cfg            = [];
cfg.resolution = 1;
cfg.dim        = [256 256 256];
mri            = ft_volumereslice(cfg, mri);

% Save coregistration matrix
transform_vox2ctf = mri.transform;
save(fullfile(mripath,sprintf('%s_transform_vox2ctf',subjectname)), 'transform_vox2ctf');


cfg             = [];
cfg.filename    = fullfile(mripath,sprintf('%sctf.mgz',subjectname));
cfg.filetype    = 'mgz';
cfg.parameter   = 'anatomy';
ft_volumewrite(cfg, mri);

% Segment data
% cfg = [];
% cfg.write      = 'no';
% [segmentedmri] = ft_volumesegment(cfg, mri);
load('/Applications/eeglab14_1_2b/myFiles/segmentedmri.mat')



% BEM Headmodel as implemented by Thom Oostendorp
cfg = [];
cfg.method = 'dipoli';
headmodel = ft_prepare_headmodel(cfg, segmentedmri);



% prepare headmodel

% Volume Conduction Model of T1
% Source Model of T1 (all possible dipole locations or smth)
% Forward solution

% Noice covariance estimation of the EEG data

% Inverse solution