function loreta_conjunction(path1,name1,t_crit1,path2,name2, t_crit2,PosOrNeg,dest)
addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/')
ft_defaults 

source1=loreta2fieldtrip([path1 char(name1)]);
source2=loreta2fieldtrip([path2 char(name2)]);
% length(source1.mom(~isnan(source1.mom)))

%% Remove sub-threshold voxels
if length(t_crit1)==1 && length(t_crit2)==1
    source1.mom(find(abs(source1.mom)<t_crit1))=NaN;
    source2.mom(find(abs(source2.mom)<t_crit2))=NaN;
end
mysize1 = size(source1.mom);
mysize2 = size(source2.mom);

%% PosOrNeg
if PosOrNeg == 0
    source1.mom(find(source1.mom<0))=NaN;
    source2.mom(find(source2.mom<0))=NaN;
    myCmap = 'hot';
    fcl = 'zeromax';
elseif PosOrNeg == 1
    source1.mom(find(source1.mom>0))=NaN;
    source2.mom(find(source2.mom>0))=NaN;
    myCmap = 'winter';
    fcl = 'minzero';
end
if PosOrNeg == 2 && length(t_crit1)==2
    for a=1:mysize1(1)
        for b=1:mysize1(2)
            for c=1:mysize1(3)
                if source1.mom(a,b,c)>0 && ~isnan(t_crit1(1)) && source1.mom(a,b,c)<t_crit1(1)
                    source1.mom(a,b,c) = NaN;
                elseif source1.mom(a,b,c)<0 && ~isnan(t_crit1(2)) && abs(source1.mom(a,b,c))<abs(t_crit1(2))
                    source1.mom(a,b,c) = NaN;
                end
                if source2.mom(a,b,c)>0 && ~isnan(t_crit2(1)) && source2.mom(a,b,c)<t_crit2(1)
                    source2.mom(a,b,c) = NaN;
                elseif source2.mom(a,b,c)<0 && ~isnan(t_crit2(2)) && abs(source2.mom(a,b,c))<abs(t_crit2(2))
                    source2.mom(a,b,c) = NaN;
                end

            end
        end
    end
    
end



%% find intersection
intersection = NaN(size(source1.mom));
if PosOrNeg == 1 || PosOrNeg == 0
    for a = 1:length(source1.mom(:,1,1))
        for b = 1:length(source1.mom(1,:,1))
            for c = 1:length(source1.mom(1,1,:))
                if ~isnan(source1.mom(a,b,c)) && ~isnan(source2.mom(a,b,c))
                    intersection(a,b,c) = 5;%mean([source1.mom(a,b,c),source2.mom(a,b,c)]);
                end
            end
        end
    end
elseif PosOrNeg == 2 
    for a = 1:length(source1.mom(:,1,1))
        for b = 1:length(source1.mom(1,:,1))
            for c = 1:length(source1.mom(1,1,:))
                if ~isnan(source1.mom(a,b,c)) && ~isnan(source2.mom(a,b,c))
                    if source1.mom(a,b,c)<0 && source2.mom(a,b,c)<0 || source1.mom(a,b,c)>0 && source2.mom(a,b,c)>0
                        intersection(a,b,c) = 5;%mean([source1.mom(a,b,c),source2.mom(a,b,c)]);
                    end
                end
            end
        end
    end
end
conjunction = source1;
conjunction.mom = intersection; 
% length(intersection(~isnan(intersection)))

%% Check if any Voxels survived
if min(min(min(isnan(conjunction.mom)))) == 1 
    disp('No supra-threshold voxels left')
    return
end



%%
cur_path_FT = '/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206';
% Read in the MNI template from SP
mri = ft_read_mri([cur_path_FT, '/external/spm8/templates/T1.nii']); 
cfg = [];
mri = ft_volumereslice(cfg, mri);

%% Downsample
cfg=[];
cfg.downsample=[6];
smooth_mri = ft_volumedownsample(cfg,mri);

% interpolate sources
cfg            = [];
cfg.downsample = [];% was 10
cfg.parameter  = 'mom';
conjunctionDiffInt  = ft_sourceinterpolate(cfg, conjunction , smooth_mri);

% stats
% ft_sourcestatistics


% Interpolate your LORETA volume on the MNI template: 
% figure;
% csg=[];
% cfg.parameter = 'avg';
% [interp_mean] = ft_sourceinterpolate(cfg, source, template);

%% plot surface
% if only pos values
if min(min(min(conjunction.mom)))>=0
    fclim = 'zeromax';
    fcmap = 'hot';
% if only neg values
elseif max(max(max(conjunction.mom)))<=0
    fclim = 'minzero';
    fcmap = 'cool';
else
    fcmap = 'jet';
    fclim = [min(min(min(conjunction.mom))) max(max(max(conjunction.mom)))];
end
fcmap = 'jet';
fclim = 'zeromax';
cfg = [];
cfg.method         = 'surface';
cfg.funparameter = 'mom';
cfg.maskparameter  = cfg.funparameter;
cfg.funcolorlim    = fclim;%'maxabs';
cfg.funcolormap    = fcmap;%myCmap;
cfg.opacitylim     = 'auto';%[0.0 1.2];
cfg.opacitymap     = 'auto';  
cfg.surfdownsample = [20];  % downsample to speed up processing
cfg.surffile       = 'surface_white_both.mat'; % Cortical sheet from canonical MNI brain
% cfg.surfinflated = 'surface_inflated_both.mat';
cfg.camlight = 'no'; % illumination of the brain
cfg.projmethod = 'sphere_avg';
cfg.sphereradius = 5;
ft_sourceplot(cfg, conjunction);
view ([90 0])             % rotate the object in the view
title(sprintf('Conjunction of %s and %s',name1, name2))
print([dest '/RightView'],'-dpng','-r300');

view ([0 90])             % rotate the object in the view
print([dest '/TopView'],'-dpng','-r300');

view ([-180 -90])             % rotate the object in the view
print([dest '/BottomView'],'-dpng','-r300');

view ([-90 0])             % rotate the object in the view
print([dest '/LeftView'],'-dpng','-r300');

view ([90 0])             % rotate the object in the view
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
print([dest '/Colorbar'],'-dpng','-r300');


cfg.surffile       = 'surface_white_right.mat'; % Cortical sheet from canonical MNI brain
ft_sourceplot(cfg, conjunction);
view ([-90 0])             % rotate the object in the view
title(sprintf('%s and %s',name1,name2))
print([dest '/LH_LeftView'],'-dpng','-r300');

cfg.surffile       = 'surface_white_left.mat'; % Cortical sheet from canonical MNI brain
ft_sourceplot(cfg, conjunction);
view ([90 0])             % rotate the object in the view
title(sprintf('%s and %s',name1,name2))
print([dest '/RH_RightView'],'-dpng','-r300');

end