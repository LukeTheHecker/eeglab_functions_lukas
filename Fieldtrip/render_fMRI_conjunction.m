function render_fMRI_conjunction(path1,name1,t_crit1,path2,name2, t_crit2,PosOrNeg,dest)
addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/')
ft_defaults 

[source1] = ft_read_mri([path1 name1]);
[sourcetst] = ft_read_mri([path1 name1],'dataformat','nifti_spm');
[source2] = ft_read_mri([path2 name2]);
if PosOrNeg == 0
    source1.anatomy(find(source1.anatomy<0))=NaN;
    source2.anatomy(find(source2.anatomy<0))=NaN;
elseif PosOrNeg == 1
    source1.anatomy(find(source1.anatomy>0))=NaN;
    source2.anatomy(find(source2.anatomy>0))=NaN;
end



source1.anatomy(find(abs(source1.anatomy)<t_crit1))=NaN;
source2.anatomy(find(abs(source2.anatomy)<t_crit2))=NaN;

%% Check if any Voxels survived
if min(min(min(isnan(source1.anatomy)))) == 1 || min(min(min(isnan(source2.anatomy)))) == 1
    disp('No supra-threshold voxels left')
    return
end

%% find intersection
if PosOrNeg == 0 || PosOrNeg == 1
    intersection = NaN(size(source1.anatomy));
    for a = 1:length(source1.anatomy(:,1,1))
        for b = 1:length(source1.anatomy(1,:,1))
            for c = 1:length(source1.anatomy(1,1,:))
                if ~isnan(source1.anatomy(a,b,c)) && ~isnan(source2.anatomy(a,b,c))
                    intersection(a,b,c) = 1;%mean([source1.anatomy(a,b,c),source2.anatomy(a,b,c)]);
                end
            end
        end
    end
elseif PosOrNeg == 2
    intersection = NaN(size(source1.anatomy));
    for a = 1:length(source1.anatomy(:,1,1))
        for b = 1:length(source1.anatomy(1,:,1))
            for c = 1:length(source1.anatomy(1,1,:))
                if ~isnan(source1.anatomy(a,b,c)) && ~isnan(source2.anatomy(a,b,c))
                    if source1.anatomy(a,b,c)<0 && source2.anatomy(a,b,c)<0 
                        intersection(a,b,c) = -1;
                    elseif source1.anatomy(a,b,c)>0 && source2.anatomy(a,b,c)>0
                        intersection(a,b,c) = 1;
                    end
                end
            end
        end
    end
end
  
conjunction = source1;
conjunction.mom = intersection; 
conjunction = rmfield(conjunction,'anatomy');
% length(intersection(~isnan(intersection)))




%%
cur_path_FT = '/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206';
% Read in the MNI template from SP
mri = ft_read_mri([cur_path_FT, '/external/spm8/templates/T1.nii']); 
cfg = [];
% cfg.dim = size(source1.anatomy);
mri = ft_volumereslice(cfg, mri);

cfg=[];
cfg.downsample=3;
smooth_mri = ft_volumedownsample(cfg,mri);
% 
% interpolate sources
cfg            = [];
cfg.downsample = [];
cfg.smooth = [];
cfg.parameter  = 'mom';
conjunctionDiffInt  = ft_sourceinterpolate(cfg, conjunction , mri);

% stats
% ft_sourcestatistics
length(conjunctionDiffInt.mom(~isnan(conjunctionDiffInt.mom)))
length(conjunction.mom(~isnan(conjunction.mom)))


%% plot surface
maxVal=max(max(max(intersection)));
myCmap = 'jet';
fcl = [min(min(min(conjunction.mom))) max(max(max(conjunction.mom)))];%'maxabs';
cfg = [];
cfg.method         = 'surface';
cfg.funparameter   = 'mom';
cfg.maskparameter  = cfg.funparameter;
cfg.funcolorlim    = 'auto';
cfg.funcolormap    = myCmap;
cfg.opacitylim     = 'auto';%[0.0 1.2];
cfg.opacitymap     = 'auto';  
% cfg.projmethod     = 'sphere_avg';
cfg.surffile       = 'surface_white_both.mat'; % Cortical sheet from canonical MNI brain
% cfg.surfdownsample = [20];%5;  % downsample to speed up processing
cfg.camlight = 'no'; % illumination of the brain
% cfg.sphereradius = 2;
ft_sourceplot(cfg, conjunction);
view ([90 0])             % rotate the object in the view
title(sprintf('Conjunction of %s and %s',name1, name2))
print([dest 'RightView'],'-dpng','-r300');

view ([0 90])             % rotate the object in the view
print([dest 'TopView'],'-dpng','-r300');

view ([-180 -90])             % rotate the object in the view
print([dest 'BottomView'],'-dpng','-r300');

view ([-90 0])             % rotate the object in the view
print([dest 'LeftView'],'-dpng','-r300');

cfg.surffile       = 'surface_white_right.mat'; % Cortical sheet from canonical MNI brain
ft_sourceplot(cfg, conjunction);
view ([-90 0])             % rotate the object in the view
title(sprintf('Conjunction of %s and %s',name1, name2))
print([dest 'RH_LeftView'],'-dpng','-r300');

cfg.surffile       = 'surface_white_left.mat'; % Cortical sheet from canonical MNI brain
ft_sourceplot(cfg, conjunction);
view ([90 0])             % rotate the object in the view
title(sprintf('Conjunction of %s and %s',name1, name2))
print([dest 'LH_RightView'],'-dpng','-r300');

%% plot multiple 2D axial slices
% cfg = [];
% cfg.slicerange = [70; 200];
% cfg.method        = 'slice';
% cfg.funparameter  = 'mom';
% cfg.maskparameter = cfg.funparameter;
% cfg.funcolorlim   = fcl;%[0.0 1.2];
% cfg.opacitylim    = 'auto';%[0.0 1.2];
% cfg.opacitymap    = 'rampup';  
% cfg.funcolormap    = myCmap;
% ft_sourceplot(cfg, conjunctionDiffInt);
% title(sprintf('Conjunction of %s and %s',name1, name2))

% %% plot 3 orthogonal slices
% cfg = [];
% cfg.method        = 'glassbrain';
% cfg.funparameter  = 'mom';
% cfg.anaparameter = 'anatomy';
% cfg.maskparameter = cfg.funparameter;
% cfg.funcolorlim   = 'zeromax';
% cfg.opacitylim    = 'auto';
% cfg.opacitymap    = 'rampup';  
% ft_sourceplot(cfg, conjunction,mri);

end