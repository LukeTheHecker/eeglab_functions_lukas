function render_fMRI(path,name, t_crit,normOrBoth)
addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/')
ft_defaults 
[source] = ft_read_mri([path name]);

%t_crit = 1.725;
if normOrBoth == 0
    source.anatomy(find(source.anatomy<0))=NaN;
end
source.anatomy(find(abs(source.anatomy)<t_crit))=NaN;

if min(min(min(isnan(source.anatomy)))) == 1
    disp('No supra-threshold voxels left')
    return
end

cur_path_FT = '/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206';
% Read in the MNI template from SP
mri = ft_read_mri([cur_path_FT, '/external/spm8/templates/T1.nii']); 
cfg = [];
mri = ft_volumereslice(cfg, mri);

cfg=[];
cfg.downsample=5;
smooth_mri = ft_volumedownsample(cfg,mri);

% interpolate sources
cfg            = [];
cfg.downsample = [];
cfg.parameter  = 'anatomy';
sourceDiffInt  = ft_sourceinterpolate(cfg, source , smooth_mri);

% stats
% ft_sourcestatistics


% Interpolate your LORETA volume on the MNI template: 
% figure;
% csg=[];
% cfg.parameter = 'avg';
% [interp_mean] = ft_sourceinterpolate(cfg, source, template);

%% plot surface
cfg = [];
cfg.method         = 'surface';
cfg.funparameter   = 'anatomy';
cfg.maskparameter  = cfg.funparameter;
cfg.funcolorlim    = 'maxabs';
cfg.funcolormap    = 'jet';
cfg.opacitylim     = 'auto';%[0.0 1.2];
cfg.opacitymap     = 'auto';  
cfg.projmethod     = 'nearest';
%cfg.surffile       = 'surface_inflated_both.mat';
cfg.surffile       = 'surface_white_both.mat'; % Cortical sheet from canonical MNI brain
cfg.surfdownsample = 2;  % downsample to speed up processing
cfg.camlight = 'no'; % illumination of the brain
% cfg.sphereradius = 15;
ft_sourceplot(cfg, source);
view ([90 0])             % rotate the object in the view
title(sprintf('%s',name))
print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/fMRI_brainplots/Surfaces/Lat/BothInOne/RightView','-dpng','-r300');

view ([0 90])             % rotate the object in the view
print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/fMRI_brainplots/Surfaces/Lat/BothInOne/TopView','-dpng','-r300');

view ([-180 -90])             % rotate the object in the view
print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/fMRI_brainplots/Surfaces/Lat/BothInOne/BottomView','-dpng','-r300');

view ([-90 0])             % rotate the object in the view
print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/fMRI_brainplots/Surfaces/Lat/BothInOne/LeftView','-dpng','-r300');

cfg.surffile       = 'surface_white_right.mat'; % Cortical sheet from canonical MNI brain
ft_sourceplot(cfg, source);
view ([-90 0])             % rotate the object in the view
title(sprintf('%s',name))
print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/fMRI_brainplots/Surfaces/Lat/BothInOne/LH_LeftView','-dpng','-r300');

cfg.surffile       = 'surface_white_left.mat'; % Cortical sheet from canonical MNI brain
ft_sourceplot(cfg, source);
view ([90 0])             % rotate the object in the view
title(sprintf('%s',name))
print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/fMRI_brainplots/Surfaces/Lat/BothInOne/RH_RightView','-dpng','-r300');

% plot multiple 2D axial slices
% cfg = [];
% cfg.method        = 'slice';
% cfg.funparameter  = 'anatomy';
% cfg.maskparameter = cfg.funparameter;
% cfg.funcolorlim   = 'maxabs';%[0.0 1.2];
% cfg.opacitylim    = 'auto';%[0.0 1.2];
% cfg.opacitymap    = 'rampup';  
% ft_sourceplot(cfg, sourceDiffInt);
% title(sprintf('%s',name))
end