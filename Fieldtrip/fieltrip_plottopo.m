% addpath
addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/')
ft_defaults 

% create FT structure
for i = 1:length(ALLEEG)
    EEG_tmp = pop_loadset('filename', ALLEEG(i).filename, 'filepath',ALLEEG(i).filepath);
    try
        EEG_tmp_A = pop_epoch(EEG_tmp, {'amb_reversal'},[-0.060 0.800]);
        ft_Amb(i) = eeglab2fieldtrip(EEG_tmp_A,'timelockanalysis','none');

    catch
        disp('no trials here')
    end
    EEG_tmp_U = pop_epoch(EEG_tmp, {'unamb_reversal'},[-0.060 0.800]);
    ft_Unamb(i) = eeglab2fieldtrip(EEG_tmp_U,'timelockanalysis','none');
end


% create grand mean
for k = 1:length(ft_Amb)
    data_amb(k,:,:) = ft_Amb(k).avg;
    data_unamb(k,:,:) = ft_Unamb(k).avg;
    var_data_amb = ft_Amb(k).var;
    var_data_unamb = ft_Unamb(k).var;
end
ftDATA_GM_Amb = ft_Amb(1);
ftDATA_GM_Amb.avg = squeeze(mean(data_amb,1));
ftDATA_GM_Amb.var = squeeze(mean(var_data_amb,1));
ftDATA_GM_Unamb = ft_Amb(1);
ftDATA_GM_Unamb.avg = squeeze(mean(data_unamb,1));
ftDATA_GM_Unamb.var = squeeze(mean(var_data_unamb,1));
ftDATA_GM_Diff = ftDATA_GM_Unamb;
ftDATA_GM_Diff.avg = (ftDATA_GM_Unamb.avg-ftDATA_GM_Amb.avg);
ftDATA_GM_Diff.var = [];

%% Single plot
figure;
plot(ftDATA_GM_Diff.time,ftDATA_GM_Diff.avg(31,:)) 
hold on
plot(ftDATA_GM_Diff.time,ftDATA_GM_Amb.avg(31,:)) 
hold on
plot(ftDATA_GM_Diff.time,ftDATA_GM_Unamb.avg(31,:)) 
hold on
plot([-0.1 0.8],[0 0])

%% perm stat
set1 = NaN(1,length(ft_Unamb));
set2 = NaN(1,length(ft_Amb));
for n = 1:length(ft_Amb)
    set1(1,n) = squeeze(mean(ft_Unamb(n).avg(31,38:66),2));
    set2(1,n) = squeeze(mean(ft_Amb(n).avg(31,38:66),2));
end

[p, obs_diff] = pairedPerm(set1, set2, 100000);
[p,h,stats] = signrank(set1,set2);
d = cohensd(set1,set2)
%% topographic ERP plot
cfg = [];
% cfg.layout = 'easycap-M1.txt';
cfg.layout = myLay;
cfg.xlim = [-0.060 0.800];
cfg.parameter = 'avg';
cfg.graphcolor = 'krb';
cfg.linewidth = 0.7;
cfg.showlabels = 'yes';
cfg.fontsize = 6;
figure; 
ft_multiplotER(cfg, ftDATA_GM_Unamb,ftDATA_GM_Amb);
% ft_multiplotER(cfg, ftDATA_GM_Diff);
axis off % this shows the actual MATLAB axes
% save high quality figure
print('/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/Results/Graphs/Results/ERP-Ambiguity-Effect/Smi/U-rev_A-rev/Smiley_UrevArev_plottopo_fieldtrip','-dpng','-r600');


%% Scalp Map Plot
cfg = [];                            
cfg.xlim = [0.23 0.24];                
% cfg.zlim = [0 6e-14];                
cfg.layout = 'easycap-M1.txt';
cfg.parameter = 'avg'; % the default 'avg' is not present in the data
figure; ft_topoplotER(cfg,ftDATA_GM_Diff); 
colorbar
