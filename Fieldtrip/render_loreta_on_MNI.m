function render_loreta_on_MNI(path,name, t_crit,PosOrNeg,dest)
addpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206/')
ft_defaults 
source=loreta2fieldtrip([path char(name)]);

mysize = size(source.mom);
if length(t_crit)==1
    if isnan(t_crit)
        return
    else
        source.mom(find(abs(source.mom)<t_crit))=NaN;
    end
elseif length(t_crit)==2
    for a=1:mysize(1)
        for b=1:mysize(2)
            for c=1:mysize(3)
                if source.mom(a,b,c)<0 && isnan(t_crit(2)) || source.mom(a,b,c)>0 && isnan(t_crit(1))
                    source.mom(a,b,c) = NaN;
                end

                if source.mom(a,b,c)>0 && ~isnan(t_crit(1)) && source.mom(a,b,c)<t_crit(1)
                    source.mom(a,b,c) = NaN;
                elseif source.mom(a,b,c)<0 && ~isnan(t_crit(2)) && abs(source.mom(a,b,c))<abs(t_crit(2))
                    source.mom(a,b,c) = NaN;
                end
            end
        end
    end
end
if length(t_crit)>1
    if isnan(t_crit(1))
        source.mom(source.mom>0)=NaN;
    elseif isnan(t_crit(2))
        source.mom(source.mom<0)=NaN;
    end
end
%t_crit = 1.725;

%% PosOrNeg
if PosOrNeg == 0
    source.mom(find(source.mom<0))=NaN;
elseif PosOrNeg == 1
    source.mom(find(source.mom>0))=NaN;
end

%% Check if voxels survived
if min(min(min(isnan(source.mom)))) == 1
    disp('No supra-threshold voxels left')
    return
end

cur_path_FT = '/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206';
% Read in the MNI template from SP
mri = ft_read_mri([cur_path_FT, '/external/spm8/templates/T1.nii']); 
cfg = [];
mri = ft_volumereslice(cfg, mri);

cfg=[];
cfg.downsample=6;
smooth_mri = ft_volumedownsample(cfg,mri);

% interpolate sources
cfg            = [];
cfg.downsample = [];
cfg.parameter  = 'mom';
sourceDiffInt  = ft_sourceinterpolate(cfg, source , smooth_mri);

% stats
% ft_sourcestatistics


% Interpolate your LORETA volume on the MNI template: 
% figure;
% csg=[];
% cfg.parameter = 'avg';
% [interp_mean] = ft_sourceinterpolate(cfg, source, template);

%% plot surface
% if only pos values
if min(min(min(source.mom)))>=0
    fclim = 'zeromax';
    fcmap = 'hot';
% if only neg values
elseif max(max(max(source.mom)))<=0
    fclim = 'minzero';
    fcmap = 'winter';
else
    fcmap = 'jet';
    fclim = [min(min(min(source.mom))) max(max(max(source.mom)))];
end

cfg = [];
cfg.method         = 'surface';
cfg.funparameter   = 'mom';
cfg.maskparameter  = cfg.funparameter;
cfg.funcolorlim    = fclim; %'maxabs';
cfg.funcolormap    = fcmap;%myCmap;
cfg.opacitylim     = 'auto';%[0.0 1.2];
cfg.opacitymap     = 'auto';  
cfg.surfdownsample = [20];  % downsample to speed up processing
cfg.surffile       = 'surface_white_both.mat'; % Cortical sheet from canonical MNI brain
% cfg.surfinflated = 'surface_inflated_both.mat';
cfg.camlight = 'no'; % illumination of the brain
cfg.projmethod = 'sphere_avg';%'sphere_avg';
cfg.sphereradius = 5;
ft_sourceplot(cfg, source);
view ([90 0])             % rotate the object in the view
title(sprintf('eLORETA of %s',name))
print([dest '/RightView'],'-dpng','-r300');

view ([0 90])             % rotate the object in the view
print([dest '/TopView'],'-dpng','-r300');

view ([-180 -90])             % rotate the object in the view
print([dest '/BottomView'],'-dpng','-r300');

view ([-90 0])             % rotate the object in the view
print([dest '/LeftView'],'-dpng','-r300');

view ([90 0])             % rotate the object in the view
cbh=colorbar;
cbh.FontSize = 18;
set(cbh, 'location', 'southoutside')
print([dest '/Colorbar'],'-dpng','-r300');

cfg.surffile       = 'surface_white_right.mat'; % Cortical sheet from canonical MNI brain
ft_sourceplot(cfg, source);
view ([-90 0])             % rotate the object in the view
title(sprintf('%s',name))
print([dest '/RH_RightView'],'-dpng','-r300');

cfg.surffile       = 'surface_white_left.mat'; % Cortical sheet from canonical MNI brain
ft_sourceplot(cfg, source);
view ([90 0])             % rotate the object in the view
title(sprintf('%s',name))
print([dest '/LH_LeftView'],'-dpng','-r300');

end