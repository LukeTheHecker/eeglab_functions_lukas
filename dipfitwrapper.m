addpath('C:\Program Files (x86)\eeglab14_1_2b\myfunctions')
% mkdir('C:\Program Files (x86)\eeglab14_1_2b\functions')
addpath(genpath('C:\Program Files (x86)\eeglab14_1_2b\functions'))


sublist = ["AA", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV"];
%sublist = ["AC" "AD"];
root = char(strcat("C:\\Users\\Lukas Hecker\\Data_Analysis\\Attention\\processed\\CheckerLargeRare\\"));

for i=1:length(sublist)
    d = dir([char(root), char(strcat(sublist(i), '_CL_0*_filtered_eventcorr_epochs_noartifact_ICA.set'))]);
    for j = 1:length(d)
        if sublist(i)== "AJ"
            file = strcat(char(sublist(i)), '_CL_02_filtered_eventcorr_epochs_noartifact_ICA.set')
        else
            file = strcat(char(sublist(i)), '_CL_0',num2str(j),'_filtered_eventcorr_epochs_noartifact_ICA.set')
        end
        dipfit_prepro(root, file)
    end
end
