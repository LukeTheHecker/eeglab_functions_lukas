function induced_itcChange_STUDYETC(STUDY,ALLEEG, tRange,method, alphacrit, name)
% induced_itcChange_STUDYETC(STUDY,ALLEEG, [-100 1100],'param',0.05,'gamma')
% induced_itcChange_STUDYETC(STUDY,ALLEEG, [-100 1100],'param',0.05,'logamma')
% induced_itcChange_STUDYETC(STUDY,ALLEEG, [-100 1100],'param',0.05,'higamma')
% induced_itcChange_STUDYETC(STUDY,ALLEEG, [-100 1100],'param',0.05,'beta')
% induced_itcChange_STUDYETC(STUDY,ALLEEG, [-100 1100],'param',0.05,'alpha')

STUDY = pop_statparams(STUDY, 'method',method,'alpha',NaN,'condstats','on');

[STUDY, erspdata, ersptimes, erspfreqs, pgroup, ersppcond, pinter] = std_itcplot(STUDY,ALLEEG,'channels',{ALLEEG(1).chanlocs.labels}, 'plotsubjects', 'on','noplot','on');

if isequal(name, 'gamma')
    minFreq=30;
    maxFreq=120;
elseif isequal(name, 'higamma')
    minFreq=50;
    maxFreq=120;
elseif isequal(name, 'logamma')
    minFreq=30;
    maxFreq=50;
elseif isequal(name, 'beta')
    minFreq=13;
    maxFreq=30;
elseif isequal(name, 'alpha')
    minFreq=7;
    maxFreq=13;
elseif isequal(name, 'theta')
    minFreq=4;
    maxFreq=7;
elseif isequal(name, 'delta')
    minFreq=1;
    maxFreq=4;    
else
    disp('This band is probably not in the STUDY structure')
    error('Breaking out of function');
end
[~, mintRange] = min(abs(ersptimes-tRange(1)));
[~, maxtRange] = min(abs(ersptimes-tRange(2)));
[~, minFreqInd] = min(abs(erspfreqs-minFreq));
[~, maxFreqInd] = min(abs(erspfreqs-maxFreq));
ersppcond= permute(cell2mat(ersppcond),[3 2 1]);
ersppcond = ersppcond(:,mintRange:maxtRange,minFreqInd:maxFreqInd);

unamb_stab = permute(abs(cell2mat(erspdata(2))), [4 3 2 1]);
unamb_stab = squeeze(mean(unamb_stab(:,:,mintRange:maxtRange,minFreqInd:maxFreqInd),1));
amb_stab = permute(abs(cell2mat(erspdata(1))), [4 3 2 1]);
amb_stab = squeeze(mean(amb_stab(:,:,mintRange:maxtRange,minFreqInd:maxFreqInd),1));
unamb_sig = unamb_stab;
unamb_sig(ersppcond>alphacrit)=0;
amb_sig = amb_stab;
amb_sig(ersppcond>alphacrit)=0;

bandpowerTR = mean(unamb_stab-amb_stab,3);
bandpowerTR_sig = mean(unamb_sig-amb_sig,3);
% extract positive changes
bandpowerTR_sig_pos = bandpowerTR_sig;
bandpowerTR_sig_pos(bandpowerTR_sig_pos<0)=0;
% extract negative changes
bandpowerTR_sig_neg = bandpowerTR_sig;
bandpowerTR_sig_neg(bandpowerTR_sig_pos>0)=0;

% global field power
figure;
% plot global field power overall
subplot(4,1,1)
gfp=std(bandpowerTR_sig);
plot(ersptimes(mintRange:maxtRange),gfp)
title(sprintf('Sig. GFP of U-A ITC in %s band',name));
% POS
subplot(4,1,2)
gfp=std(bandpowerTR_sig_pos);
plot(ersptimes(mintRange:maxtRange),gfp)
title(sprintf('Sig. GFP of POS U-A ITC in %s band',name));
% NEG
subplot(4,1,3)
gfp=std(bandpowerTR_sig_neg);
plot(ersptimes(mintRange:maxtRange),gfp)
title(sprintf('Sig. GFP of NEG U-A ITC in %s band',name));

% plot Frequency Band Power U-A at each electrode
figure;
plottopo(bandpowerTR, 'chanlocs', ALLEEG(1).chanlocs,'title',sprintf('%s Bandpower U-A' ,name))

plotmaps=-200:200:1000;
cnt=0;
figure;
for i = plotmaps
    cnt=cnt+1;
    subplot(2,length(plotmaps),cnt)
    tmp = bandpowerTR_sig_pos(:,knnsearch(ersptimes(1,mintRange:maxtRange)',i):knnsearch(ersptimes(1,mintRange:maxtRange)',i+200));
    topoplot(mean(tmp,2), ALLEEG(1).chanlocs)
    title(sprintf('%d - %d ms',i, i+200))
    if cnt == length(plotmaps)
        colorbar 
    end
end
subplot(2,length(plotmaps),[8 14])
gfp=std(bandpowerTR_sig_pos);
plot(ersptimes(mintRange:maxtRange),gfp)
title(sprintf('Sig. GFP of POS U-A in %s band',name));
suptitle(sprintf('Time series of %s U>A',name))

cnt=0;
figure;
title('Line Plot of Sine and Cosine Between -2\pi and 2\pi')
for i = plotmaps
    cnt=cnt+1;
    subplot(2,length(plotmaps),cnt)
    tmp = bandpowerTR_sig_neg(:,knnsearch(ersptimes(1,mintRange:maxtRange)',i):knnsearch(ersptimes(1,mintRange:maxtRange)',i+200));
    topoplot(mean(tmp,2), ALLEEG(1).chanlocs)
    title(sprintf('%d - %d ms',i, i+200))
    if cnt == length(plotmaps)
        colorbar 
    end
end
subplot(2,length(plotmaps),[8 14])
gfp=std(bandpowerTR_sig_neg);
plot(ersptimes(mintRange:maxtRange),gfp)
title(sprintf('Neg. GFP of POS U-A in %s band',name));
suptitle(sprintf('Time series of %s U>A',name))
suptitle(sprintf('Time series of %s A>U',name))

% mean Gamma topo
% figure;
% subplot(3,2,1)
% topoplot(mean(bandpowerTR,2), ALLEEG(1).chanlocs)%, 'maplimits','maxmin')
% title(sprintf('Mean %s Change at each electrode',name))
% colorbar
% % Significant Topo
% subplot(3,2,2)
% topoplot(mean(bandpowerTR_sig,2),ALLEEG(1).chanlocs)%,'maplimits','maxmin')
% title(sprintf('Significant %s Change at each electrode',name))
% colorbar
% % pos  topo
% subplot(3,2,3)
% tmp = bandpowerTR;
% tmp(find(tmp<=0))=0;
% trap=zeros(1,32);
% for i = 1:32
%     trap(i)=trapz(tmp(i,:));
% end
% norm=numel(find(tmp~=0))/numel(tmp);
% if norm==0
%     norm=1;
% end
% topoplot(trap/norm, ALLEEG(1).chanlocs)
% title(sprintf('Pos %s Change at each electrode',name))
% colorbar
% 
% % sig pos
% tmp = bandpowerTR_sig;
% tmp(find(tmp<=0))=0;
% trap=zeros(1,32);
% for i = 1:32
%     trap(i)=trapz(tmp(i,:));
% end
% norm=numel(find(tmp~=0))/numel(tmp);
% if norm==0
%     norm = 1;
% end
% subplot(3,2,4)
% topoplot(trap/norm, ALLEEG(1).chanlocs)
% title(sprintf('Sig Pos %s Change at each electrode',name))
% colorbar
% 
% % neg topo
% subplot(3,2,5)
% tmp = bandpowerTR;
% tmp(find(tmp>=0))=0;
% trap=zeros(1,32);
% for i = 1:32
%     trap(i)=trapz(tmp(i,:));
% end
% norm=numel(find(tmp~=0))/numel(tmp);
% if norm==0
%     norm=1;
% end
% topoplot(trap/norm, ALLEEG(1).chanlocs)
% title(sprintf('Neg %s Change at each electrode',name))
% colorbar
% 
% % sig neg
% subplot(3,2,6)
% tmp = bandpowerTR_sig;
% tmp(find(tmp>=0))=0;
% trap=zeros(1,32);
% for i = 1:32
%     trap(i)=trapz(tmp(i,:));
% end
% norm=numel(find(tmp~=0))/numel(tmp);
% if norm==0
%     norm = 1;
% end
% topoplot(trap/norm,ALLEEG(1).chanlocs)
% title(sprintf('Sig Neg %s Change at each electrode',name))
% colorbar
% end
