induced_itcChange_STUDYETC(STUDY,ALLEEG, [-400 1000],'param',0.05,'gamma')
induced_itcChange_STUDYETC(STUDY,ALLEEG, [-400 1000],'param',0.05,'logamma')
induced_itcChange_STUDYETC(STUDY,ALLEEG, [-400 1000],'param',0.05,'higamma')

induced_itcChange_STUDYETC(STUDY,ALLEEG, [-400 1000],'param',0.05,'beta')
induced_itcChange_STUDYETC(STUDY,ALLEEG, [-400 1000],'param',0.05,'alpha')
induced_itcChange_STUDYETC(STUDY,ALLEEG, [-400 1000],'param',0.05,'theta')
induced_itcChange_STUDYETC(STUDY,ALLEEG, [-400 1000],'param',0.05,'delta')


induced_bandPowerChange_STUDYETC(STUDY,ALLEEG, [-100 1100],'param',0.05,'gamma')

   
induced_bandPowerChange_STUDYETC(STUDY,ALLEEG, [-400 1000],'param',0.05,'beta')
induced_bandPowerChange_STUDYETC(STUDY,ALLEEG, [-400 1000],'param',0.05,'alpha')

induced_bandPowerChange_STUDYETC(STUDY,ALLEEG, [-400 1000],'param',0.05,'delta')
induced_bandPowerChange_STUDYETC(STUDY,ALLEEG, [-400 1000],'param',0.05,'theta')

induced_bandPowerChange_STUDYETC(STUDY,ALLEEG, [-400 1000],'param',0.05,'loalpha')
induced_bandPowerChange_STUDYETC(STUDY,ALLEEG, [-400 1000],'param',0.05,'hialpha')
induced_bandPowerChange_STUDYETC(STUDY,ALLEEG, [-400 1000],'param',0.05,'lobeta')
induced_bandPowerChange_STUDYETC(STUDY,ALLEEG, [-400 1000],'param',0.05,'hibeta')
induced_bandPowerChange_STUDYETC(STUDY,ALLEEG, [-400 1000],'param',0.05,'logamma')
induced_bandPowerChange_STUDYETC(STUDY,ALLEEG, [-400 1000],'param',0.05,'higamma')

FolderName = 'C:\Users\Lukas Hecker\Dropbox\Masterarbeit\PräsiBerndFeige\tst\';   % Your destination folder
FigList = findobj(allchild(0), 'flat', 'Type', 'figure');
for iFig = 1:length(FigList)
  FigHandle = FigList(iFig);
  FigName   = get(FigHandle, 'Name');
  savefig(FigHandle, fullfile(FolderName, FigName, '.fig'));
end