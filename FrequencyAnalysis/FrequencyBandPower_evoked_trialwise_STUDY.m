%% This function calculates frequency band power for delta, theta, alpha,
% beta (13-30) & gamma across the whole data set.

function [diff_subs, sub_sig_arr_perm, sub_sig_arr_wilc] = FrequencyBandPower_evoked_trialwise_STUDY(STUDY,ALLEEG, EEG)
global STUDY ALLEEG EEG

% Check if data is already there and if it shall be overwritten:
if isfield(STUDY.etc.FreqAnalysis, 'diff_UA_evoked')
    answer = questdlg('Do you want to overwrite STUDY.etc.FreqAnalysis.diff_UA?', ...
        'Variable Conflict', ...
        'Yes', 'No', 'No');
    % Handle response
    switch answer
        case 'Yes'
            disp('Continuing')
        case 'No'
            error('Not overwriting existing data.');
    end
end

STUDY.etc.FreqAnalysis.bands = ["Delta", "Theta", "Alpha", "Beta", "Gamma"];
diff_subs = zeros(length(ALLEEG), ALLEEG(1).nbchan, 5);
sub_sig_arr_perm=zeros(length(ALLEEG),ALLEEG(1).nbchan,5);
% loop through subjects
for m = 1:length(ALLEEG)
    
    % load current subject m:
    EEG = pop_loadset('filename', ALLEEG(m).filename, 'filepath',ALLEEG(m).filepath);

    % prepare data by extracting epochs:
    Amb_set = pop_epoch(EEG, {'amb_stability','amb_reversal'}, [-0.3 1.1], 'newname', strcat(EEG.setname, '_STAB'), 'epochinfo', 'yes');
    Unamb_set = pop_epoch(EEG, {'unamb_stability','unamb_reversal'}, [-0.3 1.1], 'newname', strcat(EEG.setname, '_STAB'), 'epochinfo', 'yes');
    % average ("evoked")
    Amb_set.data=mean(Amb_set.data,3);
    Unamb_set.data=mean(Unamb_set.data,3);
    unamb_power = zeros(32,5);
    amb_power = zeros(32,5);
    % loop through electrodes
    for i = 1:Amb_set.nbchan
            [spectra_a,freqs_a] = spectopo(Amb_set.data(i,:), 0, ALLEEG(1).srate, 'plot', 'off','freqrange',[1 120]);
            [spectra_u,freqs_u] = spectopo(Unamb_set.data(i,:), 0, ALLEEG(1).srate, 'plot', 'off','freqrange',[1 120]);
            % delta=1-4, theta=4-8, alpha=8-13, beta=13-30, gamma=30-120
            deltaIdx_a = find(freqs_a>1 & freqs_a<4);
            thetaIdx_a = find(freqs_a>4 & freqs_a<8);
            alphaIdx_a = find(freqs_a>8 & freqs_a<13);
            betaIdx_a  = find(freqs_a>13 & freqs_a<30);
            gammaIdx_a = find(freqs_a>30 & freqs_a<120);
            % delta=1-4, theta=4-8, alpha=8-13, beta=13-30, gamma=30-80
            deltaIdx_u = find(freqs_u>1 & freqs_u<4);
            thetaIdx_u = find(freqs_u>4 & freqs_u<8);
            alphaIdx_u = find(freqs_u>8 & freqs_u<13);
            betaIdx_u  = find(freqs_u>13 & freqs_u<30);
            gammaIdx_u = find(freqs_u>30 & freqs_u<120);

            amb_power(i,1) = mean(10.^(spectra_a(deltaIdx_a)/10));
            amb_power(i,2) = mean(10.^(spectra_a(thetaIdx_a)/10));
            amb_power(i,3) = mean(10.^(spectra_a(alphaIdx_a)/10));
            amb_power(i,4) = mean(10.^(spectra_a(betaIdx_a)/10));
            amb_power(i,5) = mean(10.^(spectra_a(gammaIdx_a)/10));

            unamb_power(i,1) = mean(10.^(spectra_u(deltaIdx_u)/10));
            unamb_power(i,2) = mean(10.^(spectra_u(thetaIdx_u)/10));
            unamb_power(i,3) = mean(10.^(spectra_u(alphaIdx_u)/10));
            unamb_power(i,4) = mean(10.^(spectra_u(betaIdx_u)/10));
            unamb_power(i,5) = mean(10.^(spectra_u(gammaIdx_u)/10));
    end

    % calculate difference of the means (across trials) seperately for each
    % electrode & freq band
    diff_power=zeros(ALLEEG(1).nbchan,5);
    for i = 1:5
        diff_power(:,i) =unamb_power(:,i)-amb_power(:,i);
    end
    for n = 1:5
        diff_subs(m,:,n) = diff_power(:,n);
    end   
end

STUDY.etc.FreqAnalysis.diff_UA_evoked=diff_subs;

% Permutation Test Group Level
electrode_sig_perm = zeros(ALLEEG(1).nbchan, 5);
electrode_sig_wilc = zeros(ALLEEG(1).nbchan, 5);
for k = 1:5    
    for d = 1:ALLEEG(1).nbchan
            [electrode_sig_perm(d,k), obs_diff]=permutationTest(STUDY.etc.FreqAnalysis.diff_UA_evoked(:, d, k), zeros(length(STUDY.etc.FreqAnalysis.diff_UA_evoked(:, d, k)),1), 5000);
            [electrode_sig_wilc(d,k)]=signrank(STUDY.etc.FreqAnalysis.diff_UA_evoked(:, d, k));
    end
end
STUDY.etc.FreqAnalysis.significant_channels_perm_evoked = electrode_sig_perm;
STUDY.etc.FreqAnalysis.significant_channels_wilc_evoked = electrode_sig_wilc;

STUDY = pop_savestudy(STUDY, EEG, 'savemode', 'resave');

end

