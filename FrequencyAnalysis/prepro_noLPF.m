%This script will apply all necessary preprocessing steps to all subjects
function prepro_noLPF(mypath, dest, datastruct, trigs_amb, trigs_unamb)

addpath('D:\Programme\eeglab_current\eeglab14_1_2b\myfunctions')

mypath=char(mypath);

Amb_names = [];
Unamb_names = [];
for i=1:length(datastruct)
    if datastruct(i).name(1)=='M' || datastruct(i).name(1)=='A' 
        [datastruct(i).Ambiguity] = deal('A');
        Amb_names = [Amb_names, convertCharsToStrings(datastruct(i).name)];
    elseif datastruct(i).name(1)=='E' || datastruct(i).name(1)=='U' 
        [datastruct(i).Ambiguity] = deal('U');
        Unamb_names = [Unamb_names, convertCharsToStrings(datastruct(i).name)];
    end
end


EEG1_A = pop_loadbv(mypath,char(Amb_names(1)));
EEG2_A = pop_loadbv(mypath,char(Amb_names(2)));
EEG1_A = eeg_checkset( EEG1_A );
EEG2_A = eeg_checkset( EEG2_A );
EEG1_A.setname= strcat(EEG1_A.comments(19:21),EEG1_A.comments(16:18), EEG1_A.comments(22:23));
EEG1_A.filepath = mypath;
EEG2_A.filepath = mypath;

EEG_A = pop_mergeset(EEG1_A, EEG2_A);
EEG_A.setname = EEG1_A.setname(1:5);
EEG_A = eeg_checkset(EEG_A);
EEG_A.setname = char(strcat(EEG_A(1).setname, '_mrge1'));
EEG_A = eeg_checkset(EEG_A);

EEG1_U = pop_loadbv(mypath,char(Unamb_names(1)));
EEG2_U = pop_loadbv(mypath,char(Unamb_names(2)));
EEG1_U = eeg_checkset( EEG1_U );
EEG2_U = eeg_checkset( EEG2_U );
EEG1_U.setname= strcat(EEG1_U.comments(19:21),EEG1_U.comments(16:18), EEG1_U.comments(22:23));
EEG1_U.filepath = mypath;
EEG2_U.filepath = mypath;

EEG_U = pop_mergeset(EEG1_U, EEG2_U);
EEG_U.setname = EEG1_U.setname(1:5);
EEG_U = eeg_checkset(EEG_U);
EEG_U.setname = char(strcat(EEG_U(1).setname, '_mrge1'));
EEG_U = eeg_checkset(EEG_U);

%event correction
for i=1:numel(EEG_A.event)
    if isequal(EEG_A.event(i).type, 'S  1')
        for n=i:numel(EEG_A.event)
            if isequal(EEG_A.event(n).type, 'S  2')
                EEG_A.event(i-1).type = num2str(50*round((EEG_A.event(n).latency - EEG_A.event(i).latency)/50));
                break;
            end
        end
    end
end

for i=1:numel(EEG_U.event)
    if isequal(EEG_U.event(i).type, 'S  1')
        for n=i:numel(EEG_U.event)
            if isequal(EEG_U.event(n).type, 'S  2')
                EEG_U.event(i-1).type = num2str(50*round((EEG_U.event(n).latency - EEG_U.event(i).latency)/50));
                break;
            end
        end
    end
end


% Stab/Rev Unterteilung 
% (in D:\Bibliotheken\Attention\processed\Unambig_Smileys\AMICA\stabRev)

for i=numel(EEG_U.event):-1:10
    if i>15 && isequal(EEG_U.event(i).type , char(trigs_unamb(1))) || isequal(EEG_U.event(i).type , char(trigs_unamb(3))) 
        for b=i-1:-1:i-10
            if isequal(EEG_U.event(b).type , char(trigs_unamb(2))) || isequal(EEG_U.event(i).type , char(trigs_unamb(4)))
                EEG_U.event(i).type = 'unamb_reversal';
                break;
            elseif isequal(EEG_U.event(b).type , char(trigs_unamb(1))) || isequal(EEG_U.event(i).type , char(trigs_unamb(3)))
                EEG_U.event(i).type = 'unamb_stability';
                break;
            end
        end
    elseif i>15 && isequal(EEG_U.event(i).type , char(trigs_unamb(2))) || isequal(EEG_U.event(i).type , char(trigs_unamb(4))) 
        for b=i-1:-1:i-10
            if isequal(EEG_U.event(b).type , char(trigs_unamb(1))) || isequal(EEG_U.event(b).type , char(trigs_unamb(3)))
                EEG_U.event(i).type = 'unamb_reversal';
                break;
            elseif isequal(EEG_U.event(b).type , char(trigs_unamb(2))) || isequal(EEG_U.event(b).type , char(trigs_unamb(4)))
                EEG_U.event(i).type = 'unamb_stability';
                break;
            end
        end   
    end
end
if trigs_amb == ["600";"750"; "600";"750"]; % if LA:
    cnt_R1 = 0;
    cnt_R128 = 0;
    for j = 1:length(EEG_A.event)
        if isequal(EEG_A.event(j).type,'R  1')
            cnt_R1=cnt_R1+1;
        elseif isequal(EEG_A.event(j).type, 'R128')
            cnt_R128=cnt_R128+1;
        end
    end

    if cnt_R1>cnt_R128
        trig_stab='R  1';
        trig_rev = 'R128';
    elseif cnt_R1<cnt_R128
        trig_stab='R128';
        trig_rev = 'R  1';
    end
    
    % Stab/Rev Unterteilung 
    % (in D:\Bibliotheken\Attention\processed\Unambig_Smileys\AMICA\stabRev)
    for i=numel(EEG_A.event):-1:10
        if i>15 && isequal(EEG_A.event(i).type , char(trig_stab))
            for b=i-1:-1:i-10
                if isequal(EEG_A.event(b).type , char(trigs_amb(1))) || isequal(EEG_A.event(i).type , char(trigs_amb(2)))
                    EEG_A.event(b).type = 'amb_stability';
                    break;
                end
            end
        elseif i>15 && isequal(EEG_A.event(i).type , char(trig_rev))
            for b=i-1:-1:i-10
                if isequal(EEG_A.event(b).type , char(trigs_amb(1))) || isequal(EEG_A.event(b).type , char(trigs_amb(2)))
                    EEG_A.event(b).type = 'amb_reversal';
                    break;
                end
            end   
        end
    end
else
    for i=numel(EEG_A.event):-1:10
    %         if i>15 && EEG_A.event(i).epoch > 1 && isequal(EEG_A.event(i).type , char(trigs_amb(1))) || isequal(EEG_A.event(i).type , char(trigs_amb(3))) 
        if i>15 && isequal(EEG_A.event(i).type , char(trigs_amb(1))) || isequal(EEG_A.event(i).type , char(trigs_amb(3))) 
            for b=i-1:-1:i-10
                if isequal(EEG_A.event(b).type , char(trigs_amb(2))) || isequal(EEG_A.event(i).type , char(trigs_amb(4)))
                    EEG_A.event(i).type = 'amb_reversal';
                    break;
                elseif isequal(EEG_A.event(b).type , char(trigs_amb(1))) || isequal(EEG_A.event(i).type , char(trigs_amb(3)))
                    EEG_A.event(i).type = 'amb_stability';
                    break;
                end
            end
        elseif i>15 && isequal(EEG_A.event(i).type , char(trigs_amb(2))) || isequal(EEG_A.event(i).type , char(trigs_amb(4))) 
            for b=i-1:-1:i-10
                if isequal(EEG_A.event(b).type , char(trigs_amb(1))) || isequal(EEG_A.event(b).type , char(trigs_amb(3)))
                    EEG_A.event(i).type = 'amb_reversal';
                    break;
                elseif isequal(EEG_A.event(b).type , char(trigs_amb(2))) || isequal(EEG_A.event(b).type , char(trigs_amb(4)))
                    EEG_A.event(i).type = 'amb_stability';
                    break;
                end
            end   
        end
    end
end

%Delete VEOG since it is not initially referenced to the same electrode
EEG_U_STAB = EEG_U;
EEG_A_STAB = EEG_A;
EEG_U_STAB = pop_select(EEG_U_STAB,'nochannel',33);
EEG_U_STAB = eeg_checkset(EEG_U_STAB);
%Load the data again for the more original data
EEG_U_STAB_ORI = EEG_U_STAB;
EEG_U_STAB_ORI = eeg_checkset(EEG_U_STAB_ORI);

EEG_A_STAB = pop_select(EEG_A_STAB,'nochannel',33);
EEG_A_STAB = eeg_checkset(EEG_A_STAB);
%Load the data again for the more original data
EEG_A_STAB_ORI = EEG_A_STAB;
EEG_A_STAB_ORI = eeg_checkset(EEG_A_STAB_ORI);


%set subject code, condition and session
EEG_U_STAB = pop_editset(EEG_U_STAB, 'subject', EEG_U_STAB.setname(1:2), 'condition', EEG_U_STAB.setname(4:5));
EEG_U_STAB_ORI = pop_editset(EEG_U_STAB_ORI, 'subject', EEG_U_STAB_ORI.setname(1:2), 'condition', EEG_U_STAB_ORI.setname(4:5));

EEG_A_STAB = pop_editset(EEG_A_STAB, 'subject', EEG_A_STAB.setname(1:2), 'condition', EEG_A_STAB.setname(4:5));
EEG_A_STAB_ORI = pop_editset(EEG_A_STAB_ORI, 'subject', EEG_A_STAB_ORI.setname(1:2), 'condition', EEG_A_STAB_ORI.setname(4:5));

%Edit Channels
EEG_U_STAB = pop_chanedit(EEG_U_STAB, 'lookup','D:\Programme\eeglab_current\eeglab14_1_2b\plugins\dipfit2.3\standard_BEM\elec\standard_1005.elc','eval','chans = pop_chancenter( chans, [],[]);');
EEG_U_STAB_ORI = pop_chanedit(EEG_U_STAB_ORI, 'lookup','D:\Programme\eeglab_current\eeglab14_1_2b\plugins\dipfit2.3\standard_BEM\elec\standard_1005.elc','eval','chans = pop_chancenter( chans, [],[]);');

EEG_A_STAB = pop_chanedit(EEG_A_STAB, 'lookup','D:\Programme\eeglab_current\eeglab14_1_2b\plugins\dipfit2.3\standard_BEM\elec\standard_1005.elc','eval','chans = pop_chancenter( chans, [],[]);');
EEG_A_STAB_ORI = pop_chanedit(EEG_A_STAB_ORI, 'lookup','D:\Programme\eeglab_current\eeglab14_1_2b\plugins\dipfit2.3\standard_BEM\elec\standard_1005.elc','eval','chans = pop_chancenter( chans, [],[]);');


% filter HPF: 0.01Hz (EEG_ORI)
% note that we input the pass-band edge, not the cutoff frequency!
% transition band width is 25% of the lower passband edge, but not lower than 2 Hz

EEG_U_STAB_ORI = pop_eegfiltnew(EEG_U_STAB_ORI, 0.2,[], [], 0, 0, 0);
EEG_U_STAB_ORI = eeg_checkset( EEG_U_STAB_ORI );

EEG_A_STAB_ORI = pop_eegfiltnew(EEG_A_STAB_ORI, 0.2,[], [], 0, 0, 0);
EEG_A_STAB_ORI = eeg_checkset( EEG_A_STAB_ORI );

% Clean line hum
EEG_U_STAB_ORI = pop_cleanline(EEG_U_STAB_ORI, 'bandwidth', 2,'chanlist', [1:EEG_U_STAB_ORI.nbchan], 'computepower', 0, 'linefreqs', [50 100 150 200 250],...
    'normSpectrum', 0, 'p', 0.01, 'pad', 2, 'plotfigures', 0, 'scanforlines', 1, 'sigtype', 'Channels', 'tau', 100,...
    'verb', 1, 'winsize', 4, 'winstep', 4);
EEG_A_STAB_ORI = pop_cleanline(EEG_A_STAB_ORI, 'bandwidth', 2,'chanlist', [1:EEG_A_STAB_ORI.nbchan], 'computepower', 0, 'linefreqs', [50 100 150 200 250],...
    'normSpectrum', 0, 'p', 0.01, 'pad', 2, 'plotfigures', 0, 'scanforlines', 1, 'sigtype', 'Channels', 'tau', 100,...
    'verb', 1, 'winsize', 4, 'winstep', 4);

% Detect & interpolate bad channels
% originalEEG_U = EEG_U_STAB_ORI;
% EEG_U_STAB_ORI = clean_rawdata(EEG_U_STAB_ORI, 5, -1, 0.85, 4, 20, 0.25);
% 
% originalEEG_A = EEG_A_STAB_ORI;
% EEG_A_STAB_ORI = clean_rawdata(EEG_A_STAB_ORI, 5, -1, 0.85, 4, 20, 0.25);


% Step 8: Interpolate all the removed channels
% EEG_U_STAB_ORI = pop_interp(EEG_U_STAB_ORI, originalEEG_U.chanlocs, 'spherical');
% EEG_A_STAB_ORI = pop_interp(EEG_A_STAB_ORI, originalEEG_A.chanlocs, 'spherical');
% 
% EEG_A_STAB_ORI.setname=strcat(EEG_A_STAB_ORI.setname, '_filtered_eventcorr');
% EEG_U_STAB_ORI.setname=strcat(EEG_U_STAB_ORI.setname, '_filtered_eventcorr');

%reref to avg 
EEG_U_STAB_ORI.nbchan = EEG_U_STAB_ORI.nbchan+1;
EEG_U_STAB_ORI.data(end+1,:) = zeros(1, EEG_U_STAB_ORI.pnts*EEG_U_STAB_ORI.trials);
EEG_U_STAB_ORI.chanlocs(1,EEG_U_STAB_ORI.nbchan).labels = 'initialReference';
EEG_U_STAB_ORI = pop_reref(EEG_U_STAB_ORI, []);
EEG_U_STAB_ORI = pop_select( EEG_U_STAB_ORI,'nochannel',{'initialReference'});
EEG_U_STAB_ORI.setname=strcat(EEG_U_STAB_ORI.setname, '_avgref_ORI'); 

EEG_A_STAB_ORI.nbchan = EEG_A_STAB_ORI.nbchan+1;
EEG_A_STAB_ORI.data(end+1,:) = zeros(1, EEG_A_STAB_ORI.pnts*EEG_A_STAB_ORI.trials);
EEG_A_STAB_ORI.chanlocs(1,EEG_A_STAB_ORI.nbchan).labels = 'initialReference';
EEG_A_STAB_ORI = pop_reref(EEG_A_STAB_ORI, []);
EEG_A_STAB_ORI = pop_select( EEG_A_STAB_ORI,'nochannel',{'initialReference'});
EEG_A_STAB_ORI.setname=strcat(EEG_A_STAB_ORI.setname, '_avgref_ORI');


% Epoching
EEG_U_STAB_ORI = pop_epoch(EEG_U_STAB_ORI, {'unamb_stability', 'unamb_reversal'}, [-1 2], 'newname', strcat(EEG_A_STAB_ORI.setname, '_STAB'), 'epochinfo', 'yes');
EEG_U_STAB_ORI = eeg_checkset( EEG_U_STAB_ORI );

EEG_A_STAB_ORI = pop_epoch(EEG_A_STAB_ORI, {'amb_stability', 'amb_reversal'}, [-1 2], 'newname', strcat(EEG_A_STAB_ORI.setname, '_STAB'), 'epochinfo', 'yes');
EEG_A_STAB_ORI = eeg_checkset( EEG_A_STAB_ORI );


%Artifact rejection
EEG_A_STAB_ORI = pop_autorej(EEG_A_STAB_ORI, 'nogui','on','threshold',100,'eegplot','off','electrodes', 1:32);
EEG_A_STAB_ORI.setname=strcat(EEG_A_STAB_ORI.setname, '_noartifact');
EEG_U_STAB_ORI = pop_autorej(EEG_U_STAB_ORI, 'nogui','on','threshold',100,'eegplot','off','electrodes', 1:32);
EEG_U_STAB_ORI.setname=strcat(EEG_U_STAB_ORI.setname, '_noartifact');

EEG_UA_STAB_ORI = pop_mergeset(EEG_U_STAB_ORI, EEG_A_STAB_ORI);
EEG_UA_STAB_ORI.setname = EEG_U_STAB_ORI.setname(1:5);
EEG_UA_STAB_ORI = eeg_checkset(EEG_UA_STAB_ORI);
EEG_UA_STAB_ORI = pop_saveset(EEG_UA_STAB_ORI, 'filename',EEG_UA_STAB_ORI.setname,'filepath',dest);


clear('EEG')
clear('EEG_ORI')
end