conds = {{'unamb_stability','unamb_reversal'},{'amb_stability','amb_reversal'}};
nIC = size(EEG.icaact,1);

plotDim = ceil(nIC^(1/2));
freqRange = 1:40;

fROI = {[8,13],[13,30]};
categories = {'pure_alphagenerator','pure_betagenerator', 'alpha_and_beta'};

alphacrit = 0.05;

EEG.etc.ICfreq.categories = categories;
EEG.etc.ICfreq.categories = categories;

EEG.etc.ICfreq.ICsUgrA = cell(1,3);
EEG.etc.ICfreq.ICsAgrU = cell(1,3);


figure;
for i = 1:nIC
    subplot(plotDim,plotDim,i)
    
    EEG_1 = pop_epoch(EEG,conds{1}, [-0.4 1]);
    EEG_2 = pop_epoch(EEG,conds{2}, [-0.4 1]);
    
    spectra_1 = NaN(EEG_1.trials, 126);
    spectra_2 = NaN(EEG_2.trials, 126);
    freqs = NaN(1, 126);

    
    for k = 1:EEG_1.trials
        [spectra_1(k,:),freqs] = spectopo(EEG_1.icaact(i,:,k), 0, EEG_1.srate, 'plot', 'off');
    end
    for l = 1:EEG_2.trials
        [spectra_2(l,:),~] = spectopo(EEG_2.icaact(i,:,l), 0, EEG_2.srate, 'plot', 'off');
    end
    
    sig_arr_r = NaN(1,length(freqRange));
    sig_arr_l = NaN(1,length(freqRange));
    mintr = min(EEG_1.trials,EEG_2.trials);
    for m = 1:length(freqRange)
        [~,sig_arr_r(1,m)] = signrank(spectra_1(1:mintr,m),spectra_2(1:mintr,m),'alpha',alphacrit,'tail','right');
        [~,sig_arr_l(1,m)] = signrank(spectra_1(1:mintr,m),spectra_2(1:mintr,m),'alpha',alphacrit,'tail','left');
    end
    sig_arr_r(sig_arr_r==0) = NaN;
    sig_arr_l(sig_arr_l==0) = NaN;
    
    mean_1 = squeeze(mean(spectra_1(:,freqRange),1));
    mean_2 = squeeze(mean(spectra_2(:,freqRange),1));
    
    plot(freqs(freqRange), mean_1,'r')
    hold on
    plot(freqs(freqRange), mean_2,'k')
    hold on
    sigPos = max([mean_1,mean_2]);
    scatter(freqs(freqRange), sig_arr_r*sigPos+0.5,'r')
    hold on
    scatter(freqs(freqRange), sig_arr_l*sigPos+0.5,'k')
    title(sprintf('IC %d',i))  
   
    
    % save U>A...
    % alpha generators:
    if any(sig_arr_r(1,fROI{1}(1):fROI{1}(2))) && ~any(sig_arr_r(1,fROI{2}(1):fROI{2}(2)))
        EEG.etc.ICfreq.ICsUgrA{1} = [EEG.etc.ICfreq.ICsUgrA{1}, i];
    % beta generators:
    elseif ~any(sig_arr_r(1,fROI{1}(1):fROI{1}(2))) && any(sig_arr_r(1,fROI{2}(1):fROI{2}(2)))
        EEG.etc.ICfreq.ICsUgrA{2} = [EEG.etc.ICfreq.ICsUgrA{2}, i];
    % mixed generators:
    elseif any(sig_arr_r(1,fROI{1}(1):fROI{1}(2))) && any(sig_arr_r(1,fROI{2}(1):fROI{2}(2)))
        EEG.etc.ICfreq.ICsUgrA{3} = [EEG.etc.ICfreq.ICsUgrA{3}, i];
    end
    % save A>U...
    % alpha generators:
    if any(sig_arr_l(1,fROI{1}(1):fROI{1}(2))) && ~any(sig_arr_l(1,fROI{2}(1):fROI{2}(2)))
        EEG.etc.ICfreq.ICsAgrU{1} = [EEG.etc.ICfreq.ICsAgrU{1}, i];
    % beta generators:
    elseif ~any(sig_arr_l(1,fROI{1}(1):fROI{1}(2))) && any(sig_arr_l(1,fROI{2}(1):fROI{2}(2)))
        EEG.etc.ICfreq.ICsAgrU{2} = [EEG.etc.ICfreq.ICsAgrU{2}, i];
    % mixed generators:
    elseif any(sig_arr_l(1,fROI{1}(1):fROI{1}(2))) && any(sig_arr_l(1,fROI{2}(1):fROI{2}(2)))
        EEG.etc.ICfreq.ICsAgrU{3} = [EEG.etc.ICfreq.ICsAgrU{3}, i];
    end


end



