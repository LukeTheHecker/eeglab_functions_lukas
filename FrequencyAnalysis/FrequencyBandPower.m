% calculates frequency band power for delta, theta, alpha, beta (13-25)
% across the whole data set.

%Unamb_power = zeros(32,4);
amb_power = zeros(32,4);
concat_data = reshape(EEG.data, length(EEG.data(:,1,1)) ,length(EEG.data(1,:,1))*length(EEG.data(1,1,:)));
for i = 1:length(amb_power(:,1))
    % [spectra,freqs] = spectopo(EEG.data(i,:,:), 0, EEG.srate);
    [spectra,freqs] = spectopo(concat_data(i,:), 0, EEG.srate);
    % delta=1-4, theta=4-8, alpha=8-13, beta=13-30, gamma=30-80
    deltaIdx = find(freqs>1 & freqs<4);
    thetaIdx = find(freqs>4 & freqs<8);
    alphaIdx = find(freqs>8 & freqs<13);
    betaIdx  = find(freqs>13 & freqs<25);
    %gammaIdx = find(freqs>30 & freqs<80);
    amb_power(i,1) = mean(10.^(spectra(deltaIdx)/10));
    amb_power(i,2) = mean(10.^(spectra(thetaIdx)/10));
    amb_power(i,3) = mean(10.^(spectra(alphaIdx)/10));
    amb_power(i,4) = mean(10.^(spectra(betaIdx)/10));
    %gammaPower = mean(10.^(spectra(gammaIdx)/10));
end

% figure;
% plot(1:32, unamb_power(:,1))
% hold on
% plot(1:32, amb_power(:,1))
% diff_power=zeros(32,4);
% hold on
% for i = 1:4
%     diff_power(:,i) =unamb_power(:,i)-amb_power(:,i);
% end
% plot(1:32, diff_power(:,1))
% legend(sprintf('Unambiguous Figures'),sprintf('Ambiguous Figures'))
% xlabel('Frequency')
% ylabel('Power')

figure; 
topoplot(diff_power(:,3), EEG.chanlocs)
hold on
colorbar;
title('Alpha U-A')
figure;
topoplot(diff_power(:,4), EEG.chanlocs)
hold on
colorbar;
title('Beta U-A')
figure;
topoplot(diff_power(:,1), EEG.chanlocs)
hold on
colorbar;
title('Delta U-A')
figure;
topoplot(diff_power(:,2), EEG.chanlocs)
hold on
colorbar;
title('Theta U-A')

