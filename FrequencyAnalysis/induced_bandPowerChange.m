[STUDY, erspdata, ersptimes, erspfreqs, pgroup, pcond, pinter] = std_erspplot(STUDY,ALLEEG,'channels',{ALLEEG(1).chanlocs.labels}, 'plotsubjects', 'on','noplot','on');

minFreq = 30; %hz
maxFreq = 120; 
[~, minFreqInd] = min(abs(erspfreqs-minFreq));
[~, maxFreqInd] = min(abs(erspfreqs-maxFreq));
unamb_stab = permute(cell2mat(erspdata(2)), [3 2 1]);
unamb_stab = unamb_stab(:,:,minFreqInd:maxFreqInd);
[unamb_stab_blc]=Baseline_corr(unamb_stab, [-200 0], 32, length(unamb_stab(1,1,:)), ersptimes);
amb_stab = permute(cell2mat(erspdata(1)), [3 2 1]);
amb_stab = amb_stab(:,:,minFreqInd:maxFreqInd);
[amb_stab_blc]=Baseline_corr(amb_stab, [-200 0], 32, length(amb_stab(1,1,:)), ersptimes);


gamma_bandpowerTR = mean(unamb_stab,3)-mean(amb_stab,3);
beta_bandpowerTR = mean(amb_stab,3)-mean(amb_stab,3);

% gamma_bandpowerTR = mean(unamb_stab_blc,3)-mean(amb_stab_blc,3);
% beta_bandpowerTR = mean(amb_stab_blc,3)-mean(amb_stab_blc,3);

% plot Frequency Band Power U-A at each electrode
figure;
plottopo(gamma_bandpowerTR, 'chanlocs', ALLEEG(1).chanlocs,'title','Gamma Bandpower U-A' )
figure;
topoplot(mean(gamma_bandpowerTR,2), ALLEEG(1).chanlocs)
title('Mean Gamma Change at each electrode')
colorbar
figure;
topoplot(mean(std(unamb_stab,0,3),2)-mean(std(amb_stab,0,3),2), ALLEEG(1).chanlocs)
title('Gamma Variance at each electrode')
colorbar

