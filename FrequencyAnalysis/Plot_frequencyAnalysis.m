%% Plot results of Frequency Analysis

%loop through all subjects in each freq band to check for significant spectral differences (U-A) 
H_reject = zeros(32,5);
a_crit=0.01;
for k = 1:length(ALLEEG)
    for h = 5
        h
        
        'jo'
        figure('NumberTitle', 'off', 'Name', ALLEEG(k).setname);
        subplot(1,3,1)
        topoplot(STUDY.etc.FreqAnalysis.diff_UA(k, :,h), ALLEEG(1).chanlocs)
        colorbar
        title(sprintf('Subject %s U-A %s', ALLEEG(k).subject, STUDY.etc.FreqAnalysis.bands(h)));
        subplot(1,3,2)
        H_reject = zeros(32,5);
        H_reject(find(STUDY.etc.FreqAnalysis.sub_sig_arr_wilc(k,:,h)<0.0000001),h)=1;
        pVals = STUDY.etc.FreqAnalysis.sub_sig_arr_wilc(k,:,h).^-1;
        topoplot(pVals, ALLEEG(1).chanlocs)
        colorbar;
        title('H0, alpha 0.00001% Perm')
        subplot(1,3,3)
        H_reject = zeros(32,5);
        H_reject(find(STUDY.etc.FreqAnalysis.sub_sig_arr_wilc(k,:,h)<0.00000005),h)=1;
        topoplot(H_reject(:,h), ALLEEG(1).chanlocs)
        colorbar;
        title('H0, alpha 0.000005% Perm')
    end
end
% plot induced
a_crit=0.01;
for k = 1:length(ALLEEG)
    for h = 5:5
        H_reject = zeros(32,5);
        figure('NumberTitle', 'off', 'Name', ALLEEG(k).setname);
        topoplot(STUDY.etc.FreqAnalysis.diff_UA(k, :,h), ALLEEG(1).chanlocs)
        colorbar
        title(sprintf('Induced Subject %s U-A %s', ALLEEG(k).subject, STUDY.etc.FreqAnalysis.bands(h)));
    end
end

% plot evoked
H_reject = zeros(32,5);
a_crit=0.01;
for k = 1:length(ALLEEG)
    for h = 5:5
        H_reject = zeros(32,5);
        figure('NumberTitle', 'off', 'Name', ALLEEG(k).setname);
        topoplot(STUDY.etc.FreqAnalysis.diff_UA_evoked(k, :,h), ALLEEG(1).chanlocs)
        colorbar
        title(sprintf('Evoked Subject %s U-A %s', ALLEEG(k).subject, STUDY.etc.FreqAnalysis.bands(h)));
    end
end

%% loop through bands of grand mean subject + pvalues
% 
H_reject = zeros(32,5);
a_crit=0.01;
for i = 1:5
    figure; 
    subplot(1,2,1)
    topoplot(mean(STUDY.etc.FreqAnalysis.diff_UA(:,:,i),1), EEG(1).chanlocs)
    colorbar;
    colormap(flipud(jet))
    title(sprintf('%s U-A All Subs', char(STUDY.etc.FreqAnalysis.bands(i))))
%     thr = abs(median(mean(STUDY.etc.FreqAnalysis.diff_UA(:,:,i),1))+3*std(mean(STUDY.etc.FreqAnalysis.diff_UA(:, :, i),1)));
%     fprintf('Threshold at band nr. %d : %d',i,thr)
%     caxis([-thr thr])

    
    % write one if spectral power at given electrode is significantly
    % different
    H_reject(find(STUDY.etc.FreqAnalysis.significant_channels_perm(:,i)<a_crit),i)=1;
    subplot(1,2,2)
    topoplot(H_reject(:,i), EEG(1).chanlocs)
    hold on
    colorbar;
    caxis([0 1])
    title(sprintf('H0 rejection (=1), p< %d', round(a_crit,2)))
    hold off
end

%% loop through bands of grand mean subject + pvalues
% 
H_reject = zeros(32,5);
a_crit=0.05;
for i = 1:5
    figure; 
    subplot(1,2,1)
    topoplot(mean(STUDY.etc.FreqAnalysis.diff_UA_evoked(:,:,i),1), EEG(1).chanlocs)
    colorbar;
    colormap(flipud(jet))
    title(sprintf('%s U-A All Subs', char(STUDY.etc.FreqAnalysis.bands(i))))
%     thr = abs(median(mean(STUDY.etc.FreqAnalysis.diff_UA(:,:,i),1))+3*std(mean(STUDY.etc.FreqAnalysis.diff_UA(:, :, i),1)));
%     fprintf('Threshold at band nr. %d : %d',i,thr)
%     caxis([-thr thr])

    
    % write one if spectral power at given electrode is significantly
    % different
    H_reject(find(STUDY.etc.FreqAnalysis.significant_channels_perm_evoked(:,i)<a_crit),i)=1;
    subplot(1,2,2)
    topoplot(H_reject(:,i), EEG(1).chanlocs)
    hold on
    colorbar;
    caxis([0 1])
    title(sprintf('H0 rejection (=1), p< %d', round(a_crit,2)))
    hold off
end