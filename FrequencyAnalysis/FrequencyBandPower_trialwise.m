% calculates frequency band power for delta, theta, alpha, beta (13-25)
% across the whole data set.

% [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 2,'retrieve',1,'study',0); 


Amb_set = pop_epoch(EEG, {'amb_stability', 'amb_reversal'}, [-0.3 1.1], 'newname', strcat(EEG.setname, '_STAB'), 'epochinfo', 'yes');
Unamb_set = pop_epoch(EEG, {'unamb_stability', 'unamb_reversal'}, [-0.3 1.1], 'newname', strcat(EEG.setname, '_STAB'), 'epochinfo', 'yes');
trials_u = length(Unamb_set.data(1,1,:));
trials_a = length(Amb_set.data(1,1,:));
Unamb_power = zeros(32,trials_u,4);
amb_power = zeros(32,trials_a,4);
% loop through trials
for k = 1:length(EEG.data(1,1,:))
    % loop through electrodes
    for i = 1:length(amb_power(:,1))
        % [spectra,freqs] = spectopo(EEG.data(i,:,:), 0, EEG.srate);
        if k<= trials_a
            [spectra_a,freqs_a] = spectopo(Amb_set.data(i,:,k), 0, EEG.srate, 'plot', 'off');
        elseif k <= trials_u
            [spectra_u,freqs_u] = spectopo(Unamb_set.data(i,:,k), 0, EEG.srate, 'plot', 'off');
        else
            break;
        end
        % delta=1-4, theta=4-8, alpha=8-13, beta=13-30, gamma=30-80
        deltaIdx_a = find(freqs_a>1 & freqs_a<4);
        thetaIdx_a = find(freqs_a>4 & freqs_a<8);
        alphaIdx_a = find(freqs_a>8 & freqs_a<13);
        betaIdx_a  = find(freqs_a>13 & freqs_a<25);

        % delta=1-4, theta=4-8, alpha=8-13, beta=13-30, gamma=30-80
        deltaIdx_u = find(freqs_u>1 & freqs_u<4);
        thetaIdx_u = find(freqs_u>4 & freqs_u<8);
        alphaIdx_u = find(freqs_u>8 & freqs_u<13);
        betaIdx_u  = find(freqs_u>13 & freqs_u<25);
        
        amb_power(i,k,1) = mean(10.^(spectra_a(deltaIdx_a)/10));
        amb_power(i,k,2) = mean(10.^(spectra_a(thetaIdx_a)/10));
        amb_power(i,k,3) = mean(10.^(spectra_a(alphaIdx_a)/10));
        amb_power(i,k,4) = mean(10.^(spectra_a(betaIdx_a)/10));
        
        unamb_power(i,k,1) = mean(10.^(spectra_u(deltaIdx_u)/10));
        unamb_power(i,k,2) = mean(10.^(spectra_u(thetaIdx_u)/10));
        unamb_power(i,k,3) = mean(10.^(spectra_u(alphaIdx_u)/10));
        unamb_power(i,k,4) = mean(10.^(spectra_u(betaIdx_u)/10));
    end
end

% calculate difference of the means (across trials) seperately for each
% electrode & freq band
diff_power=zeros(32,4);
for i = 1:4
    diff_power(:,i) =mean(unamb_power(:,:,i),2)-mean(amb_power(:,:,i),2);
end

figure; 
topoplot(diff_power(:,3), EEG.chanlocs)
hold on
colorbar;
title('Alpha U-A')
figure;
topoplot(diff_power(:,4), EEG.chanlocs)
hold on
colorbar;
title('Beta U-A')
figure;
topoplot(diff_power(:,1), EEG.chanlocs)
hold on
colorbar;
title('Delta U-A')
figure;
topoplot(diff_power(:,2), EEG.chanlocs)
hold on
colorbar;
title('Theta U-A')

