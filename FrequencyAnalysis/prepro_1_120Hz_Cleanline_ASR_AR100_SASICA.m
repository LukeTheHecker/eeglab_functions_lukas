%This script will apply all necessary preprocessing steps to all subjects
function prepro_1_120Hz_Cleanline_ASR_AR100_SASICA(mypath, dest, datastruct, trigs_amb, trigs_unamb)


mypath=char(mypath);
% load it
[EEG_A, EEG_U]=load_prepro(datastruct,mypath);

% get EEG.event
[EEG_A, EEG_U]=event_function(EEG_A,EEG_U, trigs_amb, trigs_unamb);

%Delete VEOG since it is not initially referenced to the same electrode
EEG_UA_STAB = pop_mergeset(EEG_U,EEG_A);
EEG_UA_STAB.setname = EEG_U.setname(1:2);
EEG_UA_STAB = pop_select(EEG_UA_STAB,'nochannel',33);
EEG_UA_STAB = eeg_checkset(EEG_UA_STAB);

% resample to 250 Hz
EEG_UA_STAB = pop_resample(EEG_UA_STAB, 250);

%set subject code, condition and session
EEG_UA_STAB = pop_editset(EEG_UA_STAB, 'subject', EEG_UA_STAB.setname(1:2), 'condition', 'UA');

%Edit Channels
EEG_UA_STAB = pop_chanedit(EEG_UA_STAB, 'lookup','/Applications/eeglab14_1_2b/plugins/dipfit2.3/standard_BEM/elec/standard_1005.elc','eval','chans = pop_chancenter( chans, [],[]);');

% filter HPF: 0.01 Hz for later and 0.2 Hz for ICA LPF: 120Hz
% note that we input the pass-band edge, not the cutoff frequency!
% transition band width is 25% of the lower passband edge, but not lower than 2 Hz

EEG_UA_STAB = pop_eegfiltnew(EEG_UA_STAB, 0.02,[120], [], 0, 0, 0);
EEG_UA_STAB = eeg_checkset( EEG_UA_STAB );

EEG_UA_STAB_ORI = EEG_UA_STAB;
EEG_UA_STAB_ORI = pop_eegfiltnew(EEG_UA_STAB_ORI, 0.4,[120], [], 0, 0, 0);

% Clean line hum
addpath(genpath('/Applications/eeglab14_1_2b/plugins/Cleanline1.03'))
EEG_UA_STAB = pop_cleanline(EEG_UA_STAB, 'bandwidth', 2,'chanlist', [1:EEG_UA_STAB.nbchan], 'computepower', 0, 'linefreqs', [50 100 150 200 250],...
'normSpectrum', 0, 'p', 0.01, 'pad', 2, 'plotfigures', 0, 'scanforlines', 1, 'sigtype', 'Channels', 'tau', 100,...
'verb', 1, 'winsize', 4, 'winstep', 4);
EEG_UA_STAB_ORI = pop_cleanline(EEG_UA_STAB_ORI, 'bandwidth', 2,'chanlist', [1:EEG_UA_STAB_ORI.nbchan], 'computepower', 0, 'linefreqs', [50 100 150 200 250],...
'normSpectrum', 0, 'p', 0.01, 'pad', 2, 'plotfigures', 0, 'scanforlines', 1, 'sigtype', 'Channels', 'tau', 100,...
'verb', 1, 'winsize', 4, 'winstep', 4);
rmpath(genpath('/Applications/eeglab14_1_2b/plugins/Cleanline1.03'))

originalEEG_UA=EEG_UA_STAB;
% Apply soft ASR (SD=20)
EEG_UA_STAB = clean_rawdata(EEG_UA_STAB, 5, [], 0.85, 4, 20, -1);
EEG_UA_STAB_ORI = clean_rawdata(EEG_UA_STAB, 5, [], 0.85, 4, 20, -1);

% Step 8: Interpolate all the removed channels
EEG_UA_STAB = pop_interp(EEG_UA_STAB, originalEEG_UA.chanlocs, 'spherical');
EEG_UA_STAB_ORI = pop_interp(EEG_UA_STAB_ORI, originalEEG_UA.chanlocs, 'spherical');

EEG_UA_STAB.setname=strcat(EEG_UA_STAB.setname, '_filtered_eventcorr');
EEG_UA_STAB_ORI.setname=strcat(EEG_UA_STAB_ORI.setname, 'ORI_filtered_eventcorr');

%reref to avg 
EEG_UA_STAB = reref_CAR(EEG_UA_STAB);
EEG_UA_STAB_ORI = reref_CAR(EEG_UA_STAB_ORI);

% Epoching
EEG_UA_STAB_ORI = pop_epoch(EEG_UA_STAB_ORI, {'unamb_stability', 'unamb_reversal'}, [-0.4 1], 'newname', strcat(EEG_UA_STAB_ORI.setname, '_STAB'), 'epochinfo', 'yes');
EEG_UA_STAB_ORI = eeg_checkset( EEG_UA_STAB_ORI );

EEG_UA_STAB = pop_epoch(EEG_UA_STAB, {'unamb_stability', 'unamb_reversal'}, [-1 2], 'newname', strcat(EEG_UA_STAB.setname, '_STAB'), 'epochinfo', 'yes');
EEG_UA_STAB = pop_rmbase( EEG_UA_STAB, [-60  40]);
EEG_UA_STAB = eeg_checkset( EEG_UA_STAB );

EEG_UA_STAB = pop_saveset(EEG_UA_STAB, 'filename',EEG_UA_STAB.setname,'filepath',dest);

%AMICA
if isfield(EEG_UA_STAB_ORI.etc, 'clean_channel_mask')
    dataRank = min([rank(double(EEG_UA_STAB_ORI.data(:,:,1)')) sum(EEG_UA_STAB_ORI.etc.clean_channel_mask)]);
else
    dataRank = rank(double(EEG_UA_STAB_ORI.data(:,:,1)'));
end

%AMICA 
directory = char(strcat(dest, 'amicaout'));
%needs to be done since we epoched the data and runamica15() only takes
%continuous data shape
concat_data = reshape(EEG_UA_STAB_ORI.data, length(EEG_UA_STAB_ORI.data(:,1,1)) ,length(EEG_UA_STAB_ORI.data(1,:,1))*length(EEG_UA_STAB_ORI.data(1,1,:)));
% datarange = 32*32*1;
%Don't use PCA ('pcakeep'), see Artoni et al 2018
[W, S, ~] = runamica15(concat_data, 'outdir',directory,...
    'num_chans', EEG_UA_STAB_ORI.nbchan,...
    'num_models', 1,'max_threads', 4,'do_reject', 1, 'numrej', 15,...
    'rejsig', 3,'rejint', 1 );
 
EEG_UA_STAB_ORI.etc.amica  = loadmodout15([directory]);
% EEG_UA_STAB.etc.amica.S = EEG_UA_STAB.etc.amica.S(1:EEG_UA_STAB.etc.amica.num_pcs, :); % Weirdly, I saw size(S,1) be larger than rank. This process does not hurt anyway.
 
EEG_UA_STAB_ORI.icaweights = W;
 
EEG_UA_STAB_ORI.icasphere  = S;
EEG_UA_STAB_ORI = eeg_checkset(EEG_UA_STAB_ORI, 'ica');


 
%EEG_UA_STABRAW.icachansind = EEG_UA_STAB.icachansind;
EEG_UA_STAB.icaweights = EEG_UA_STAB_ORI.icaweights;
EEG_UA_STAB.icasphere = EEG_UA_STAB_ORI.icasphere(1:EEG_UA_STAB_ORI.etc.amica.num_pcs, :);
EEG_UA_STAB = eeg_checkset( EEG_UA_STAB, 'ica' );
EEG_UA_STAB.setname = char(strcat(EEG_UA_STAB.setname, '_AMICA'));

% Remove artifactual components using SASICA
EEG_UA_STAB_SASICA=EEG_UA_STAB;
[EEG_UA_STAB_SASICA] = eeg_SASICA(EEG_UA_STAB_SASICA,'MARA_enable',0,'FASTER_enable',0,...
    'FASTER_blinkchanname','No channel','ADJUST_enable',1,...
    'chancorr_enable',0,'chancorr_channames','No channel',...
    'chancorr_corthresh','auto 4','EOGcorr_enable',0,...
    'EOGcorr_Heogchannames','No channel','EOGcorr_corthreshH','auto 4',...
    'EOGcorr_Veogchannames','No channel','EOGcorr_corthreshV','auto 4',...
    'resvar_enable',0,'resvar_thresh',15,'SNR_enable',0,'SNR_snrcut',1,...
    'SNR_snrBL',[-Inf 0] ,'SNR_snrPOI',[0 Inf] ,'trialfoc_enable',1,...
    'trialfoc_focaltrialout',15,'focalcomp_enable',1,...
    'focalcomp_focalICAout','auto 2','autocorr_enable',1,...
    'autocorr_autocorrint',20,'autocorr_dropautocorr','auto',...
    'opts_noplot',1,'opts_nocompute',0,'opts_FontSize',14);
EEG_UA_STAB_SASICA=pop_subcomp(EEG_UA_STAB_SASICA,[find(EEG_UA_STAB_SASICA.reject.gcompreject==1)]);

%Fit Dipoles
coordinateTransformParameters = [0.922221 -18.3794 0.696875 9.09909e-08 -1.51952e-07 -1.5708 1 1 1];
templateChannelFilePath = '/Applications/eeglab14_1_2b/plugins/dipfit2.3/standard_BEM/elec/standard_1005.elc';
hdmFilePath             = '/Applications/eeglab14_1_2b/plugins/dipfit2.3/standard_BEM/standard_vol.mat';

EEG_UA_STAB = pop_dipfit_settings( EEG_UA_STAB, 'hdmfile', hdmFilePath, 'coordformat', 'MNI',...
    'mrifile', '/Applications/eeglab14_1_2b/plugins/dipfit2.3/standard_BEM/standard_mri.mat',...
    'chanfile', templateChannelFilePath, 'coord_transform', coordinateTransformParameters,...
    'chansel', 1:EEG_UA_STAB.nbchan);
EEG_UA_STAB = pop_multifit(EEG_UA_STAB, 1:EEG_UA_STAB.nbchan,'threshold', 100, 'dipplot','off','plotopt',{'normlen' 'on'});
    
% Search for and estimate symmetrically constrained bilateral dipoles
EEG_UA_STAB = fitTwoDipoles(EEG_UA_STAB, 'LRR', 35);

EEG_UA_STAB_SASICA = pop_dipfit_settings( EEG_UA_STAB_SASICA, 'hdmfile', hdmFilePath, 'coordformat', 'MNI',...
    'mrifile', '/Applications/eeglab14_1_2b/plugins/dipfit2.3/standard_BEM/standard_mri.mat',...
    'chanfile', templateChannelFilePath, 'coord_transform', coordinateTransformParameters,...
    'chansel', 1:EEG_UA_STAB_SASICA.nbchan);
EEG_UA_STAB_SASICA = pop_multifit(EEG_UA_STAB_SASICA, 1:EEG_UA_STAB_SASICA.nbchan,'threshold', 100, 'dipplot','off','plotopt',{'normlen' 'on'});
    
% Search for and estimate symmetrically constrained bilateral dipoles
EEG_UA_STAB_SASICA = fitTwoDipoles(EEG_UA_STAB_SASICA, 'LRR', 35);
 
EEG_UA_STAB_SASICA.setname = char(strcat(EEG_UA_STAB_SASICA.setname, '_SASICA_Dipfit'));
EEG_UA_STAB_SASICA = pop_saveset(EEG_UA_STAB_SASICA, 'filename',EEG_UA_STAB_SASICA.setname,'filepath',[dest '/SASICA/']);
 
EEG_UA_STAB.setname = char(strcat(EEG_UA_STAB.setname, '_Dipfit'));
EEG_UA_STAB = pop_saveset(EEG_UA_STAB, 'filename',EEG_UA_STAB.setname,'filepath',dest);

clear('EEG')
clear('EEG_ORI')
end