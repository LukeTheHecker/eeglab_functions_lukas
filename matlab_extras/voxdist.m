function [d] = voxdist(pnts_1, pnts_2)

d = sqrt(sum((pnts_1-pnts_2).^2));

end

