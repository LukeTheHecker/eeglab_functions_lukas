function [EEG_A, EEG_U]=load_prepro(datastruct,mypath)
Amb_names = [];
Unamb_names = [];
for i=1:length(datastruct)
    if datastruct(i).name(1)=='M' || datastruct(i).name(1)=='A' 
        [datastruct(i).Ambiguity] = deal('A');
        Amb_names = [Amb_names, convertCharsToStrings(datastruct(i).name)];
    elseif datastruct(i).name(1)=='E' || datastruct(i).name(1)=='U' 
        [datastruct(i).Ambiguity] = deal('U');
        Unamb_names = [Unamb_names, convertCharsToStrings(datastruct(i).name)];
    end
end


EEG1_A = pop_loadbv(mypath,char(Amb_names(1)));
EEG2_A = pop_loadbv(mypath,char(Amb_names(2)));
EEG1_A = eeg_checkset( EEG1_A );
EEG2_A = eeg_checkset( EEG2_A );
EEG1_A.setname= strcat(EEG1_A.comments(19:21),EEG1_A.comments(16:18), EEG1_A.comments(22:23));
EEG1_A.filepath = mypath;
EEG2_A.filepath = mypath;

EEG_A = pop_mergeset(EEG1_A, EEG2_A);
EEG_A.setname = EEG1_A.setname(1:5);
EEG_A = eeg_checkset(EEG_A);
EEG_A.setname = char(strcat(EEG_A(1).setname, '_mrge1'));
EEG_A = eeg_checkset(EEG_A);

EEG1_U = pop_loadbv(mypath,char(Unamb_names(1)));
EEG2_U = pop_loadbv(mypath,char(Unamb_names(2)));
EEG1_U = eeg_checkset( EEG1_U );
EEG2_U = eeg_checkset( EEG2_U );
EEG1_U.setname= strcat(EEG1_U.comments(19:21),EEG1_U.comments(16:18), EEG1_U.comments(22:23));
EEG1_U.filepath = mypath;
EEG2_U.filepath = mypath;

EEG_U = pop_mergeset(EEG1_U, EEG2_U);
EEG_U.setname = EEG1_U.setname(1:5);
EEG_U = eeg_checkset(EEG_U);
EEG_U.setname = char(strcat(EEG_U(1).setname, '_mrge1'));
EEG_U = eeg_checkset(EEG_U);
end