%% initial parameters
%Subject prefixes
sublist = ["AA", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV"];

% define your triggers for epoching: 
% IMPORTANT: make sure first and third entry are corresponding stimuli,
% since they later will be used to determine stability/reversal trials.

% for LD: 
% triggers = ["500"; "550"; "650"; "700"];
% for LA:
% triggers = ["600";"750"; "600";"750"];
% for SD:
% triggers = ["100"; "150"; "300"; "350"];
% for SA:
% triggers = ["200"; "250"; "400"; "450"];
% for MD:
% triggers = ["500"; "550"; "500"; "550"];
% for MA:
%initialize root vector containing the path of subjects
root= strings(1,length(sublist));
for i=1:length(sublist)
    %Where is your raw data folder containing one folder per subject?
    root(1,i) = strcat('/Users/lukashecker/Desktop/Lukas_Hecker/Attention_Necker_Faces/Igor/data_auswertung/data/EEG/3SplitAll/',sublist(i));
end

%% lattices UA zusammen PREP ASR 10 new params
trigs_amb = ["600";"750"; "600";"750"];
trigs_unamb = ["500"; "550"; "650"; "700"];
%Where do you want to save your finished prepro files?
destination = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/processed/CleaningForGamma/PREP_on_Fullfreq_newparams10/Lat/';
for i=1:length(sublist)
   % try
        disp("starting with: ")
        disp(sublist(i))
        d_amb = dir([char(root(i)), '/AO_*.vhdr']);
        d_unamb = dir([char(root(i)), '/UO_*.vhdr']);
        datastruct=d_amb;
        for j=1:length(d_unamb)
            datastruct(end+1) = d_unamb(j);
        end
        prepro_amica_UAzusammen_gamma_PREP_newparams_20std(root(1,i), destination,datastruct, trigs_amb,trigs_unamb );
  %  catch
  %      disp('skip because of error')
  %  end
end

%% lattices UA zusammen PREP ASR 20 new params
trigs_amb = ["600";"750"; "600";"750"];
trigs_unamb = ["500"; "550"; "650"; "700"];
%Where do you want to save your finished prepro files?
destination = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/processed/CleaningForGamma/PREP_on_Fullfreq_newparams20/Lat/';
for i=1:length(sublist)
   % try
        disp("starting with: ")
        disp(sublist(i))
        d_amb = dir([char(root(i)), '/AO_*.vhdr']);
        d_unamb = dir([char(root(i)), '/UO_*.vhdr']);
        datastruct=d_amb;
        for j=1:length(d_unamb)
            datastruct(end+1) = d_unamb(j);
        end
        prepro_amica_UAzusammen_gamma_PREP_newparams_20std(root(1,i), destination,datastruct, trigs_amb,trigs_unamb );
  %  catch
  %      disp('skip because of error')
  %  end
end

%% lattices UA zusammen Cleanline, AR100 + MARA
trigs_amb = ["600";"750"; "600";"750"];
trigs_unamb = ["500"; "550"; "650"; "700"];
%Where do you want to save your finished prepro files?
destination = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/processed/UA_Lattices_fullfreq/MARA/';
for i=1:length(sublist)
   % try
        disp("starting with: ")
        disp(sublist(i))
        d_amb = dir([char(root(i)), '/AO_*.vhdr']);
        d_unamb = dir([char(root(i)), '/UO_*.vhdr']);
        datastruct=d_amb;
        for j=1:length(d_unamb)
            datastruct(end+1) = d_unamb(j);
        end
        prepro_amica_UAzusammen_gamma(root(1,i), destination,datastruct, trigs_amb,trigs_unamb );
  %  catch
  %      disp('skip because of error')
  %  end
end
%% smileys UA zusammen
trigs_amb = ["200"; "250"; "400"; "450"];
trigs_unamb = ["100"; "150"; "300"; "350"];
%Where do you want to save your finished prepro files?
destination = '/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/processed/CleaningForGamma/PREP_on_Fullfreq/Smi/';
for i=1:length(sublist)
    try
        disp("starting with: ")
        disp(sublist(i))
        d_amb = dir([char(root(i)), '/MO_*.vhdr']);
        d_unamb = dir([char(root(i)), '/EO_*.vhdr']);
        datastruct=d_amb;
        for j=1:length(d_unamb)
            datastruct(end+1) = d_unamb(j);
        end
        prepro_amica_UAzusammen_gamma_PREP(root(1,i), destination,datastruct, trigs_amb,trigs_unamb );
    catch
        disp('skip because of error')
    end
end

