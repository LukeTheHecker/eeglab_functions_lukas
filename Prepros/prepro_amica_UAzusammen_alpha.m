%This script will apply all necessary preprocessing steps to all subjects
function prepro_amica_UAzusammen_alpha(mypath, dest, datastruct, trigs_amb, trigs_unamb)

addpath('/Applications/eeglab14_1_2b/myfunctions/')

mypath=char(mypath);

Amb_names = [];
Unamb_names = [];
for i=1:length(datastruct)
    if datastruct(i).name(1)=='M' || datastruct(i).name(1)=='A'
        [datastruct(i).Ambiguity] = deal(datastruct(i).name(1));
        Amb_names = [Amb_names, convertCharsToStrings(datastruct(i).name)];
    elseif datastruct(i).name(1)=='E' || datastruct(i).name(1)=='U'
        [datastruct(i).Ambiguity] = deal(datastruct(i).name(1));
        Unamb_names = [Unamb_names, convertCharsToStrings(datastruct(i).name)];
    end
end


EEG1_A = pop_loadbv(mypath,char(Amb_names(1)));
EEG2_A = pop_loadbv(mypath,char(Amb_names(2)));
EEG1_A = eeg_checkset( EEG1_A );
EEG2_A = eeg_checkset( EEG2_A );
EEG1_A.setname= strcat(EEG1_A.comments(19:21),EEG1_A.comments(16:18), EEG1_A.comments(22:23));
EEG1_A.filepath = mypath;
EEG2_A.filepath = mypath;

EEG_A = pop_mergeset(EEG1_A, EEG2_A);
EEG_A.setname = EEG1_A.setname(1:5);
EEG_A = eeg_checkset(EEG_A);
EEG_A.setname = char(strcat(EEG_A(1).setname, '_mrge1'));
EEG_A = eeg_checkset(EEG_A);

EEG1_U = pop_loadbv(mypath,char(Unamb_names(1)));
EEG2_U = pop_loadbv(mypath,char(Unamb_names(2)));
EEG1_U = eeg_checkset( EEG1_U );
EEG2_U = eeg_checkset( EEG2_U );
EEG1_U.setname= strcat(EEG1_U.comments(19:21),EEG1_U.comments(16:18), EEG1_U.comments(22:23));
EEG1_U.filepath = mypath;
EEG2_U.filepath = mypath;

EEG_U = pop_mergeset(EEG1_U, EEG2_U);
EEG_U.setname = EEG1_U.setname(1:5);
EEG_U = eeg_checkset(EEG_U);
EEG_U.setname = char(strcat(EEG_U(1).setname, '_mrge1'));
EEG_U = eeg_checkset(EEG_U);

%event correction
for i=1:numel(EEG_A.event)
    if isequal(EEG_A.event(i).type, 'S  1')
        for n=i:numel(EEG_A.event)
            if isequal(EEG_A.event(n).type, 'S  2')
                EEG_A.event(i-1).type = num2str(50*round((EEG_A.event(n).latency - EEG_A.event(i).latency)/50));
                break;
            end
        end
    end
end

for i=1:numel(EEG_U.event)
    if isequal(EEG_U.event(i).type, 'S  1')
        for n=i:numel(EEG_U.event)
            if isequal(EEG_U.event(n).type, 'S  2')
                EEG_U.event(i-1).type = num2str(50*round((EEG_U.event(n).latency - EEG_U.event(i).latency)/50));
                break;
            end
        end
    end
end


% Stab/Rev Unterteilung 

for i=numel(EEG_U.event):-1:10
%         if i>15 && EEG_U.event(i).epoch > 1 && isequal(EEG_U.event(i).type , char(trigs_unamb(1))) || isequal(EEG_U.event(i).type , char(trigs_unamb(3))) 
    if i>15 && isequal(EEG_U.event(i).type , char(trigs_unamb(1))) || isequal(EEG_U.event(i).type , char(trigs_unamb(3))) 
        for b=i-1:-1:i-10
            if isequal(EEG_U.event(b).type , char(trigs_unamb(2))) || isequal(EEG_U.event(i).type , char(trigs_unamb(4)))
                EEG_U.event(i).type = 'reversal';
                break;
            elseif isequal(EEG_U.event(b).type , char(trigs_unamb(1))) || isequal(EEG_U.event(i).type , char(trigs_unamb(3)))
                EEG_U.event(i).type = 'unamb_stability';
                break;
            end
        end
    elseif i>15 && isequal(EEG_U.event(i).type , char(trigs_unamb(2))) || isequal(EEG_U.event(i).type , char(trigs_unamb(4))) 
        for b=i-1:-1:i-10
            if isequal(EEG_U.event(b).type , char(trigs_unamb(1))) || isequal(EEG_U.event(b).type , char(trigs_unamb(3)))
                EEG_U.event(i).type = 'reversal';
                break;
            elseif isequal(EEG_U.event(b).type , char(trigs_unamb(2))) || isequal(EEG_U.event(b).type , char(trigs_unamb(4)))
                EEG_U.event(i).type = 'unamb_stability';
                break;
            end
        end   
    end
end
if trigs_amb == ["600";"750"; "600";"750"]; % if LA:
    cnt_R1 = 0;
    cnt_R128 = 0;
    for j = 1:length(EEG_A.event)
        if isequal(EEG_A.event(j).type,'R  1')
            cnt_R1=cnt_R1+1;
        elseif isequal(EEG_A.event(j).type, 'R128')
            cnt_R128=cnt_R128+1;
        end
    end

    if cnt_R1>cnt_R128
        trig_stab='R  1';
        trig_rev = 'R128';
    elseif cnt_R1<cnt_R128
        trig_stab='R128';
        trig_rev = 'R  1';
    end
    
    % Stab/Rev Unterteilung 
    for i=numel(EEG_A.event):-1:10
        if i>15 && isequal(EEG_A.event(i).type , char(trig_stab))
            for b=i-1:-1:i-10
                if isequal(EEG_A.event(b).type , char(trigs_amb(1))) || isequal(EEG_A.event(i).type , char(trigs_amb(2)))
                    EEG_A.event(b).type = 'amb_stability';
                    break;
                end
            end
        elseif i>15 && isequal(EEG_A.event(i).type , char(trig_rev))
            for b=i-1:-1:i-10
                if isequal(EEG_A.event(b).type , char(trigs_amb(1))) || isequal(EEG_A.event(b).type , char(trigs_amb(2)))
                    EEG_A.event(b).type = 'reversal';
                    break;
                end
            end   
        end
    end
else
    for i=numel(EEG_A.event):-1:10
    %         if i>15 && EEG_A.event(i).epoch > 1 && isequal(EEG_A.event(i).type , char(trigs_amb(1))) || isequal(EEG_A.event(i).type , char(trigs_amb(3))) 
        if i>15 && isequal(EEG_A.event(i).type , char(trigs_amb(1))) || isequal(EEG_A.event(i).type , char(trigs_amb(3))) 
            for b=i-1:-1:i-10
                if isequal(EEG_A.event(b).type , char(trigs_amb(2))) || isequal(EEG_A.event(i).type , char(trigs_amb(4)))
                    EEG_A.event(i).type = 'reversal';
                    break;
                elseif isequal(EEG_A.event(b).type , char(trigs_amb(1))) || isequal(EEG_A.event(i).type , char(trigs_amb(3)))
                    EEG_A.event(i).type = 'amb_stability';
                    break;
                end
            end
        elseif i>15 && isequal(EEG_A.event(i).type , char(trigs_amb(2))) || isequal(EEG_A.event(i).type , char(trigs_amb(4))) 
            for b=i-1:-1:i-10
                if isequal(EEG_A.event(b).type , char(trigs_amb(1))) || isequal(EEG_A.event(b).type , char(trigs_amb(3)))
                    EEG_A.event(i).type = 'reversal';
                    break;
                elseif isequal(EEG_A.event(b).type , char(trigs_amb(2))) || isequal(EEG_A.event(b).type , char(trigs_amb(4)))
                    EEG_A.event(i).type = 'amb_stability';
                    break;
                end
            end   
        end
    end
end

EEG_U_STAB = pop_epoch(EEG_U, {'unamb_stability'}, [-0.3 1.1], 'newname', strcat(EEG_U.setname, '_STAB'), 'epochinfo', 'yes');
EEG_U_STAB = eeg_checkset( EEG_U_STAB );
EEG_U_STAB = pop_editset(EEG_U_STAB, 'condition', char(strcat(EEG_U.condition,'S' )));

EEG_U_STAB_ORI = pop_epoch(EEG_U, {'unamb_stability'}, [-1 2], 'newname', strcat(EEG_U.setname, '_STAB'), 'epochinfo', 'yes');
EEG_U_STAB_ORI = eeg_checkset( EEG_U_STAB_ORI );
EEG_U_STAB_ORI = pop_editset(EEG_U_STAB_ORI, 'condition', char(strcat(EEG_U.condition,'S' )));


EEG_A_STAB = pop_epoch(EEG_A, {'amb_stability'}, [-0.3 1.1], 'newname', strcat(EEG_A.setname, '_STAB'), 'epochinfo', 'yes');
EEG_A_STAB = eeg_checkset( EEG_A_STAB );
EEG_A_STAB = pop_editset(EEG_A_STAB, 'condition', char(strcat(EEG_A.condition,'S' )));

EEG_A_STAB_ORI = pop_epoch(EEG_A, {'amb_stability'}, [-1 2], 'newname', strcat(EEG_A.setname, '_STAB'), 'epochinfo', 'yes');
EEG_A_STAB_ORI = eeg_checkset( EEG_A_STAB_ORI );
EEG_A_STAB_ORI = pop_editset(EEG_A_STAB_ORI, 'condition', char(strcat(EEG_A.condition,'S')));

%Delete VEOG since it is not initially referenced to the same electrode
EEG_U_STAB = pop_select(EEG_U_STAB,'nochannel',33);
EEG_U_STAB = eeg_checkset(EEG_U_STAB);
EEG_U_STAB_ORI = pop_select(EEG_U_STAB_ORI,'nochannel',33);
EEG_U_STAB_ORI = eeg_checkset(EEG_U_STAB_ORI);

EEG_A_STAB = pop_select(EEG_A_STAB,'nochannel',33);
EEG_A_STAB = eeg_checkset(EEG_A_STAB);
EEG_A_STAB_ORI = pop_select(EEG_A_STAB_ORI,'nochannel',33);
EEG_A_STAB_ORI = eeg_checkset(EEG_A_STAB_ORI);


% set subject code, condition and session
EEG_U_STAB = pop_editset(EEG_U_STAB, 'subject', EEG_U_STAB.setname(1:2), 'condition', EEG_U_STAB.setname(4:5));
EEG_U_STAB_ORI = pop_editset(EEG_U_STAB_ORI, 'subject', EEG_U_STAB_ORI.setname(1:2), 'condition', EEG_U_STAB_ORI.setname(4:5));

EEG_A_STAB = pop_editset(EEG_A_STAB, 'subject', EEG_A_STAB.setname(1:2), 'condition', EEG_A_STAB.setname(4:5));
EEG_A_STAB_ORI = pop_editset(EEG_A_STAB_ORI, 'subject', EEG_A_STAB_ORI.setname(1:2), 'condition', EEG_A_STAB_ORI.setname(4:5));


% Downsample to 250 for the ICA-dataset and to 500 for the ORI set
tmpname = EEG_U_STAB.setname;
EEG_U_STAB = pop_resample( EEG_U_STAB, 250);
EEG_U_STAB.setname=tmpname;
tmpname = EEG_U_STAB_ORI.setname;
EEG_U_STAB_ORI = pop_resample( EEG_U_STAB_ORI, 500);
EEG_U_STAB_ORI.setname=tmpname;

tmpname = EEG_A_STAB.setname;
EEG_A_STAB = pop_resample( EEG_A_STAB, 250);
EEG_A_STAB.setname=tmpname;
tmpname = EEG_A_STAB_ORI.setname;
EEG_A_STAB_ORI = pop_resample( EEG_A_STAB_ORI, 500);
EEG_A_STAB_ORI.setname=tmpname;

% Cutoffs: HPF: 8Hz LPF: 13 Hz (both)
% note that we input the pass-band edge, not the cutoff frequency!
% transition band width is 25% of the lower passband edge, but not lower than 2 Hz

EEG_U_STAB = pop_eegfiltnew(EEG_U_STAB, 8+1.125, [], [], 0, [], 0);
EEG_U_STAB = pop_eegfiltnew(EEG_U_STAB, [], 13-1.5, [], 0, [], 0);
EEG_U_STAB = eeg_checkset( EEG_U_STAB );

EEG_A_STAB = pop_eegfiltnew(EEG_A_STAB, 8+1.125, [], [], 0, [], 0);
EEG_A_STAB = pop_eegfiltnew(EEG_A_STAB, [], 13-1.5, [], 0, [], 0);
EEG_A_STAB = eeg_checkset( EEG_A_STAB );


EEG_U_STAB_ORI = pop_eegfiltnew(EEG_U_STAB_ORI, [], 13-(2.75/2), [], 0, [], 0);
EEG_U_STAB_ORI = pop_eegfiltnew(EEG_U_STAB_ORI, 8+1.125,[], [], 0, [], 0);
EEG_U_STAB_ORI = eeg_checkset( EEG_U_STAB_ORI );

EEG_A_STAB_ORI = pop_eegfiltnew(EEG_A_STAB_ORI, [], 13-(2.75/2), [], 0, [], 0);
EEG_A_STAB_ORI = pop_eegfiltnew(EEG_A_STAB_ORI, 8+1.125,[], [], 0, [], 0);
EEG_A_STAB_ORI = eeg_checkset( EEG_A_STAB_ORI );

EEG_A_STAB.setname=strcat(EEG_A_STAB.setname, '_filtered_eventcorr_dwnsmpld');
EEG_A_STAB_ORI.setname=strcat(EEG_A_STAB_ORI.setname, '_filtered_eventcorr');

EEG_U_STAB.setname=strcat(EEG_U_STAB.setname, '_filtered_eventcorr_dwnsmpld');
EEG_U_STAB_ORI.setname=strcat(EEG_U_STAB_ORI.setname, '_filtered_eventcorr');

% Edit Channels
EEG_U_STAB = pop_chanedit(EEG_U_STAB, 'lookup','/Applications/eeglab14_1_2b/plugins/dipfit2.3/standard_BEM/elec/standard_1005.elc','eval','chans = pop_chancenter( chans, [],[]);');
EEG_U_STAB_ORI = pop_chanedit(EEG_U_STAB_ORI, 'lookup','/Applications/eeglab14_1_2b/plugins/dipfit2.3/standard_BEM/elec/standard_1005.elc','eval','chans = pop_chancenter( chans, [],[]);');

EEG_A_STAB = pop_chanedit(EEG_A_STAB, 'lookup','/Applications/eeglab14_1_2b/plugins/dipfit2.3/standard_BEM/elec/standard_1005.elc','eval','chans = pop_chancenter( chans, [],[]);');
EEG_A_STAB_ORI = pop_chanedit(EEG_A_STAB_ORI, 'lookup','/Applications/eeglab14_1_2b/plugins/dipfit2.3/standard_BEM/elec/standard_1005.elc','eval','chans = pop_chancenter( chans, [],[]);');

% Reref to avg
EEG_U_STAB.nbchan = EEG_U_STAB.nbchan+1;
EEG_U_STAB.data(end+1,:) = zeros(1, EEG_U_STAB.pnts*EEG_U_STAB.trials);
EEG_U_STAB.chanlocs(1,EEG_U_STAB.nbchan).labels = 'initialReference';
EEG_U_STAB = pop_reref( EEG_U_STAB, []);
EEG_U_STAB = pop_select( EEG_U_STAB,'nochannel',{'initialReference'});
EEG_U_STAB.setname=strcat(EEG_U_STAB.setname, '_avgref');
 
EEG_U_STAB_ORI.nbchan = EEG_U_STAB_ORI.nbchan+1;
EEG_U_STAB_ORI.data(end+1,:) = zeros(1, EEG_U_STAB_ORI.pnts*EEG_U_STAB_ORI.trials);
EEG_U_STAB_ORI.chanlocs(1,EEG_U_STAB_ORI.nbchan).labels = 'initialReference';
EEG_U_STAB_ORI = pop_reref(EEG_U_STAB_ORI, []);
EEG_U_STAB_ORI = pop_select( EEG_U_STAB_ORI,'nochannel',{'initialReference'});
EEG_U_STAB_ORI.setname=strcat(EEG_U_STAB_ORI.setname, '_avgref_ORI');

EEG_A_STAB.nbchan = EEG_A_STAB.nbchan+1;
EEG_A_STAB.data(end+1,:) = zeros(1, EEG_A_STAB.pnts*EEG_A_STAB.trials);
EEG_A_STAB.chanlocs(1,EEG_A_STAB.nbchan).labels = 'initialReference';
EEG_A_STAB = pop_reref( EEG_A_STAB, []);
EEG_A_STAB = pop_select( EEG_A_STAB,'nochannel',{'initialReference'});
EEG_A_STAB.setname=strcat(EEG_A_STAB.setname, '_avgref');
 
EEG_A_STAB_ORI.nbchan = EEG_A_STAB_ORI.nbchan+1;
EEG_A_STAB_ORI.data(end+1,:) = zeros(1, EEG_A_STAB_ORI.pnts*EEG_A_STAB_ORI.trials);
EEG_A_STAB_ORI.chanlocs(1,EEG_A_STAB_ORI.nbchan).labels = 'initialReference';
EEG_A_STAB_ORI = pop_reref(EEG_A_STAB_ORI, []);
EEG_A_STAB_ORI = pop_select( EEG_A_STAB_ORI,'nochannel',{'initialReference'});
EEG_A_STAB_ORI.setname=strcat(EEG_A_STAB_ORI.setname, '_avgref_ORI');

% Artifact rejection
EEG_A_STAB = pop_autorej(EEG_A_STAB, 'nogui','on','threshold',100,'eegplot','off','electrodes', 1:32);
EEG_A_STAB.setname=strcat(EEG_A_STAB.setname, '_noartifact');
EEG_A_STAB_ORI = pop_autorej(EEG_A_STAB_ORI, 'nogui','on','threshold',100,'eegplot','off','electrodes', 1:32);
EEG_A_STAB_ORI.setname=strcat(EEG_A_STAB_ORI.setname, '_noartifact');
EEG_A_STAB = pop_saveset( EEG_A_STAB, 'filename',EEG_A_STAB.setname,'filepath',dest);
EEG_A_STAB_ORI = pop_saveset(EEG_A_STAB_ORI, 'filename',EEG_A_STAB_ORI.setname,'filepath',dest);

EEG_U_STAB = pop_autorej(EEG_U_STAB, 'nogui','on','threshold',100,'eegplot','off','electrodes', 1:32);
EEG_U_STAB.setname=strcat(EEG_U_STAB.setname, '_noartifact');
EEG_U_STAB_ORI = pop_autorej(EEG_U_STAB_ORI, 'nogui','on','threshold',100,'eegplot','off','electrodes', 1:32);
EEG_U_STAB_ORI.setname=strcat(EEG_U_STAB_ORI.setname, '_noartifact');
EEG_U_STAB = pop_saveset( EEG_U_STAB, 'filename',EEG_U_STAB.setname,'filepath',dest);
EEG_U_STAB_ORI = pop_saveset(EEG_U_STAB_ORI, 'filename',EEG_U_STAB_ORI.setname,'filepath',dest);

% merge Unambiguous and Ambiguous
EEG_UA_STAB = pop_mergeset(EEG_U_STAB, EEG_A_STAB);
EEG_UA_STAB.setname = EEG_U_STAB.setname(1:5);
EEG_UA_STAB = eeg_checkset(EEG_UA_STAB);
EEG_UA_STAB.setname = char(strcat(EEG_UA_STAB(1).setname, '_mrge2'));
EEG_UA_STAB = eeg_checkset(EEG_UA_STAB);

% AMICA
if isfield(EEG_UA_STAB.etc, 'clean_channel_mask')
    dataRank = min([rank(double(EEG_UA_STAB.data(:,:,1)')) sum(EEG_UA_STAB.etc.clean_channel_mask)]);
else
    dataRank = rank(double(EEG_UA_STAB.data(:,:,1)'));
end


directory = char(strcat(dest, 'amicaout'));
%needs to be done since we epoched the data and runamica15() only takes
%continuous data shape
concat_data = reshape(EEG_UA_STAB.data, length(EEG_UA_STAB.data(:,1,1)) ,length(EEG_UA_STAB.data(1,:,1))*length(EEG_UA_STAB.data(1,1,:)));
%datarange = 32*32*30;
datarange = 32*32;
%Don't use PCA ('pcakeep'), see Artoni et al 2018
[W, S, mods] = runamica15(concat_data, 'outdir',directory,...
    'num_chans', EEG_UA_STAB.nbchan,'pcakeep',dataRank,'num_chans', EEG_UA_STAB.nbchan,...
    'num_models', 1,'max_threads', 4,'do_reject', 1, 'numrej', 15,...
    'rejsig', 3,'rejint', 1 );

EEG_UA_STAB.etc.amica  = loadmodout15([directory]);
% EEG_UA_STAB.etc.amica.S = EEG_UA_STAB.etc.amica.S(1:EEG_UA_STAB.etc.amica.num_pcs, :); % Weirdly, I saw size(S,1) be larger than rank. This process does not hurt anyway.
 
EEG_UA_STAB.icaweights = W;
 
EEG_UA_STAB.icasphere  = S;
EEG_UA_STAB = eeg_checkset(EEG_UA_STAB, 'ica');
EEG_UA_STAB = pop_saveset(EEG_UA_STAB, 'filename',EEG_UA_STAB.setname,'filepath',char(strcat(dest,'/AMICA/merged/')));

 
%EEG_UA_STABRAW.icachansind = EEG_UA_STAB.icachansind;
EEG_UA_STAB_ORI = pop_mergeset(EEG_U_STAB_ORI,EEG_A_STAB_ORI);

EEG_UA_STAB_ORI.icaweights = EEG_UA_STAB.icaweights;
EEG_UA_STAB_ORI.icasphere = EEG_UA_STAB.icasphere(1:EEG_UA_STAB.etc.amica.num_pcs, :);
EEG_UA_STAB_ORI = eeg_checkset( EEG_UA_STAB_ORI, 'ica' );

EEG_UA_STAB.setname = char(strcat(EEG_UA_STAB.setname, '_AMICA'));
EEG_UA_STAB_ORI.setname = char(strcat(EEG_U_STAB_ORI.setname, '_AMICA'));
EEG_UA_STAB = pop_saveset(EEG_UA_STAB, 'filename',EEG_UA_STAB.setname,'filepath',char(strcat(dest, '/AMICA/Downsamp/')));


%Fit Dipole
coordinateTransformParameters = [0 0 0 0 0 -1.5708 1 1 1];
templateChannelFilePath = '/Applications/eeglab14_1_2b/plugins/dipfit2.3/standard_BEM/elec/standard_1005.elc';
hdmFilePath             = '/Applications/eeglab14_1_2b/plugins/dipfit2.3/standard_BEM/standard_vol.mat';

EEG_UA_STAB_ORI = pop_dipfit_settings( EEG_UA_STAB_ORI, 'hdmfile', hdmFilePath, 'coordformat', 'MNI',...
    'mrifile', '/Applications/eeglab14_1_2b/plugins/dipfit2.3/standard_BEM/standard_mri.mat',...
    'chanfile', templateChannelFilePath, 'coord_transform', coordinateTransformParameters,...
    'chansel', 1:EEG_UA_STAB_ORI.nbchan);
EEG_UA_STAB_ORI = pop_multifit(EEG_UA_STAB_ORI, 1:EEG_UA_STAB_ORI.nbchan,'threshold', 100, 'dipplot','off','plotopt',{'normlen' 'on'});
    
% Search for and estimate symmetrically constrained bilateral dipoles
EEG_UA_STAB_ORI = fitTwoDipoles(EEG_UA_STAB_ORI, 'LRR', 35);
 
EEG_UA_STAB_ORI.setname = char(strcat(EEG_UA_STAB_ORI.setname, '_Dipfit'));
EEG_UA_STAB_ORI = pop_saveset(EEG_UA_STAB_ORI, 'filename',EEG_UA_STAB_ORI.setname,'filepath',char(strcat(dest, '/AMICA/')));

clear('EEG')
clear('EEG_ORI')
end