function [EEG_A,EEG_U]=event_function(EEG_A,EEG_U, trigs_amb, trigs_unamb)
stabLefts = ['AA', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AU', 'AV' ];
stabRights = ['AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AQ', 'AR', 'AS', 'AT'];
%event correction
for i=1:numel(EEG_A.event)
    if isequal(EEG_A.event(i).type, 'S  1')
        for n=i:numel(EEG_A.event)
            if isequal(EEG_A.event(n).type, 'S  2')
                EEG_A.event(i-1).type = num2str(50*round((EEG_A.event(n).latency - EEG_A.event(i).latency)/50));
                break;
            end
        end
    end
end

for i=1:numel(EEG_U.event)
    if isequal(EEG_U.event(i).type, 'S  1')
        for n=i:numel(EEG_U.event)
            if isequal(EEG_U.event(n).type, 'S  2')
                EEG_U.event(i-1).type = num2str(50*round((EEG_U.event(n).latency - EEG_U.event(i).latency)/50));
                break;
            end
        end
    end
end


% Stab/Rev Unterteilung 
% (in D:\Bibliotheken\Attention\processed\Unambig_Smileys\AMICA\stabRev)

for i=numel(EEG_U.event):-1:10
    if i>15 && isequal(EEG_U.event(i).type , char(trigs_unamb(1))) || isequal(EEG_U.event(i).type , char(trigs_unamb(3))) 
        for b=i-1:-1:i-10
            if isequal(EEG_U.event(b).type , char(trigs_unamb(2))) || isequal(EEG_U.event(b).type , char(trigs_unamb(4)))
                EEG_U.event(i).type = 'unamb_reversal';
                break;
            elseif isequal(EEG_U.event(b).type , char(trigs_unamb(1))) || isequal(EEG_U.event(b).type , char(trigs_unamb(3)))
                EEG_U.event(i).type = 'unamb_stability';
                break;
            end
        end
    elseif i>15 && isequal(EEG_U.event(i).type , char(trigs_unamb(2))) || isequal(EEG_U.event(i).type , char(trigs_unamb(4))) 
        for b=i-1:-1:i-10
            if isequal(EEG_U.event(b).type , char(trigs_unamb(1))) || isequal(EEG_U.event(b).type , char(trigs_unamb(3)))
                EEG_U.event(i).type = 'unamb_reversal';
                break;
            elseif isequal(EEG_U.event(b).type , char(trigs_unamb(2))) || isequal(EEG_U.event(b).type , char(trigs_unamb(4)))
                EEG_U.event(i).type = 'unamb_stability';
                break;
            end
        end   
    end
end
if trigs_amb == ["600";"750"; "600";"750"]; % if LA:

if ismember(EEG_A.setname(1:2), stabLefts)
    trig_stab='R  1';
    trig_rev = 'R128';
elseif ismember(EEG_A.setname(1:2), stabRights)
    trig_stab='R128';
    trig_rev = 'R  1';
end
    
    for i=numel(EEG_A.event):-1:10
        if i>15 && isequal(EEG_A.event(i).type , char(trig_stab))
            for b=i-1:-1:i-10
                if isequal(EEG_A.event(b).type , char(trigs_amb(1))) || isequal(EEG_A.event(b).type , char(trigs_amb(2)))
                    EEG_A.event(b).type = 'amb_stability';
                    break;
                end
            end
        elseif i>15 && isequal(EEG_A.event(i).type , char(trig_rev))
            for b=i-1:-1:i-10
                if isequal(EEG_A.event(b).type , char(trigs_amb(1))) || isequal(EEG_A.event(b).type , char(trigs_amb(2)))
                    EEG_A.event(b).type = 'amb_reversal';
                    break;
                end
            end   
        end
    end
else
    for i=numel(EEG_A.event):-1:10
    %         if i>15 && EEG_A.event(i).epoch > 1 && isequal(EEG_A.event(i).type , char(trigs_amb(1))) || isequal(EEG_A.event(i).type , char(trigs_amb(3))) 
        if i>15 && isequal(EEG_A.event(i).type , char(trigs_amb(1))) || isequal(EEG_A.event(i).type , char(trigs_amb(3))) 
            for b=i-1:-1:i-10
                if isequal(EEG_A.event(b).type , char(trigs_amb(2))) || isequal(EEG_A.event(i).type , char(trigs_amb(4)))
                    EEG_A.event(i).type = 'amb_reversal';
                    break;
                elseif isequal(EEG_A.event(b).type , char(trigs_amb(1))) || isequal(EEG_A.event(i).type , char(trigs_amb(3)))
                    EEG_A.event(i).type = 'amb_stability';
                    break;
                end
            end
        elseif i>15 && isequal(EEG_A.event(i).type , char(trigs_amb(2))) || isequal(EEG_A.event(i).type , char(trigs_amb(4))) 
            for b=i-1:-1:i-10
                if isequal(EEG_A.event(b).type , char(trigs_amb(1))) || isequal(EEG_A.event(b).type , char(trigs_amb(3)))
                    EEG_A.event(i).type = 'amb_reversal';
                    break;
                elseif isequal(EEG_A.event(b).type , char(trigs_amb(2))) || isequal(EEG_A.event(b).type , char(trigs_amb(4)))
                    EEG_A.event(i).type = 'amb_stability';
                    break;
                end
            end   
        end
    end
end
end