%This script will apply all necessary preprocessing steps to all subjects
function prepro(mypath, myfile, trigs, dest)

disp(trigs)
addpath('C:\Program Files (x86)\eeglab14_1_2b\myfunctions')
mypath=char(mypath);
myfile=char(myfile);
%Load the sub
EEG = pop_loadbv(mypath, myfile,[],[]);
EEG.setname= strcat(EEG.comments(19:21),EEG.comments(16:18), EEG.comments(22:23));
EEG.filepath = strcat(('C:\\Users\\Lukas Hecker\\Data_Analysis\\Attention\\processed\\'),EEG.comments(19:20));
disp(EEG.filepath)

%event correction
for i=1:numel(EEG.event)
    if isequal(EEG.event(i).type, 'S  1')
        for n=i:numel(EEG.event)
            if isequal(EEG.event(n).type, 'S  2')
                EEG.event(i-1).type = num2str(50*round((EEG.event(n).latency - EEG.event(i).latency)/50));
                break;
            end
        end
    end
end
EEG.setname=strcat(EEG.setname, '_filtered_eventcorr');


%Downsample
tmpname = EEG.setname;
EEG = pop_resample( EEG, 250);
EEG.setname=tmpname;

%filter 1Hz HPF & 25 Hz LPF
EEG = pop_eegfiltnew(EEG, [],1,[],true, [], 0);
EEG = pop_eegfiltnew(EEG, [],25,[], false,[],0);

EEG = eeg_checkset( EEG );

%Edit Channels
EEG=pop_chanedit(EEG, 'load',{'C:\\Program Files (x86)\\eeglab14_1_2b\\mylocs\\32electrodes_and_VEOG.ced' 'filetype' 'autodetect'});
%figure; topoplot([],EEG.chanlocs, 'style', 'blank', 'electrodes', 'labelpoint');

%set subject code, condition and session
EEG = pop_editset(EEG, 'subject', EEG.setname(end-7:end-6), 'condition', EEG.setname(end-4:end-3), 'session', str2num(EEG.setname(end)));

%save it
EEG = pop_saveset( EEG, 'filename',EEG.setname,'filepath',dest);    

%reref to avg
EEG = pop_reref( EEG, []);
%Rereference to mastoid TP9 TP10
%EEG = pop_reref( EEG, [17 22] );
EEG.setname=strcat(EEG.setname, '_avgref');

%epoching
%EEG = pop_epoch(EEG, {char(trigs(1)) char(trigs(2)) char(trigs(3)) char(trigs(4))}, [-0.06 1], 'newname', strcat(EEG.setname, '_epochs'), 'epochinfo', 'yes');
EEG = pop_epoch(EEG, {char(trigs(1)) char(trigs(2)) char(trigs(3)) char(trigs(4))}, [-0.06 1], 'newname', strcat(EEG.setname, '_epochs'), 'epochinfo', 'yes');
EEG = eeg_checkset( EEG );
%EEG = pop_rmbase( EEG, [-60  40]);
EEG = eeg_checkset( EEG );
%EEG = pop_saveset( EEG, 'filename',EEG.setname,'filepath',dest);    

%Artifact rejection
EEG = pop_autorej(EEG, 'nogui','on','threshold',100,'eegplot','off','electrodes', [1:33]);
EEG.setname=strcat(EEG.setname, '_noartifact');
EEG = pop_saveset( EEG, 'filename',EEG.setname,'filepath',dest);    
end