% get grand mean erps from study
[STUDY erpdata erptimes pgroup pcond pinter] = std_erpplot(STUDY, ALLEEG);

% sub mean
%subMean = mean(EEG.data,3);

% create data matrices
data_unamb=permute(squeeze(mean(cell2mat(erpdata(2)),3)),[2 1]);
data_amb=permute(squeeze(mean(cell2mat(erpdata(1)),3)),[2 1]);
diff = data_amb-data_unamb;
% find timepoints of time range
x=round(ALLEEG(1).xmin:(1/ALLEEG(1).srate):ALLEEG(1).xmax,3);
start=0.18;
stop=0.22;
range=find(x==start):find(x==stop);


% scalp map at full range
figure; topoplot(mean(data_unamb(:,:),2), ALLEEG(1).chanlocs);
title('Scalp Map at full range'); 
colorbar;
% scalp map at given range
figure; topoplot(mean(data_unamb(:,range),2), ALLEEG(1).chanlocs);
title(sprintf('Scalp Map at %d to %d ms', start*1000, stop*1000)); 
colorbar;
% whole data mean laplacian
figure; topoplot(del2map(mean(data_unamb(:,:),2), ALLEEG(1).chanlocs), ALLEEG(1).chanlocs);
title('laplacian at full range'); colorbar
% laplacian at given range
figure; topoplot(del2map(mean(data_unamb(:,range),2), ALLEEG(1).chanlocs), ALLEEG(1).chanlocs);
title(sprintf('laplacian at %d to %d ms', start*1000, stop*1000)); 
colorbar
% amb-unamb Scalp Map at given range
figure; topoplot(mean(diff(:,range),2), ALLEEG(1).chanlocs);
title(sprintf('Scalp Map A-U at %d to %d ms', start*1000, stop*1000)); 
colorbar
% amb-unamb laplacian at given range
figure; topoplot(del2map(mean(diff(:,range),2), ALLEEG(1).chanlocs), ALLEEG(1).chanlocs);
title(sprintf('laplacian A-U at %d to %d ms', start*1000, stop*1000)); 
colorbar
% amb-unamb laplacian at full range
figure; topoplot(del2map(mean(diff(:,:),2), ALLEEG(1).chanlocs), ALLEEG(1).chanlocs);
title('laplacian amb-unamb laplacian at full range'); colorbar
colorbar


% Freq stuff
[spectra_unamb,freqs_unamb,speccomp,contrib,specstd] = spectopo(data_unamb(:,range), [0], ALLEEG(1).srate,'plot','off');
[spectra_amb,freqs_amb,speccomp,contrib,specstd] = spectopo(data_amb(:,range), [0], ALLEEG(1).srate,'plot','off');
delta = [2 4];
theta = [5 7];
alpha = [8 13];
beta = [13 30];
gamma = [30 120];
% Delta Topo & laplacian
figure; 
subplot(2,3,1)
deltaSpecUnamb=mean(spectra_unamb(:,find(freqs_unamb==Delta(1)):find(freqs_unamb==Delta(2))),2);
deltaSpecAmb=mean(spectra_amb(:,find(freqs_amb==Delta(1)):find(freqs_amb==Delta(2))),2);
topoplot(deltaSpecUnamb, ALLEEG(1).chanlocs);
title(sprintf('Unamb Delta Topo %d to %d hz', delta(1), delta(2))); 
colorbar
subplot(2,3,4)
topoplot(del2map(deltaSpecUnamb, ALLEEG(1).chanlocs), ALLEEG(1).chanlocs);
title(sprintf('Unamb CSD Delta %d to %d hz', delta(1), delta(2))); 
colorbar
subplot(2,3,2)
topoplot(deltaSpecAmb, ALLEEG(1).chanlocs);
title(sprintf('Amb Delta Topo %d to %d hz', delta(1), delta(2))); 
colorbar
subplot(2,3,5)
topoplot(del2map(deltaSpecAmb, ALLEEG(1).chanlocs), ALLEEG(1).chanlocs);
title(sprintf('Amb CSD Delta %d to %d hz', delta(1), delta(2))); 
colorbar
subplot(2,3,3)
topoplot(deltaSpecAmb-deltaSpecUnamb, ALLEEG(1).chanlocs);
title(sprintf('Amb-Unamb Delta Topo %d to %d hz', delta(1), delta(2))); 
colorbar
subplot(2,3,6)
topoplot(del2map(deltaSpecAmb-deltaSpecUnamb, ALLEEG(1).chanlocs), ALLEEG(1).chanlocs);
title(sprintf('Amb-Unamb CSD Delta %d to %d hz', delta(1), delta(2))); 
colorbar

% theta Topo & laplacian
figure; 
subplot(2,3,1)
thetaSpecUnamb=mean(spectra_unamb(:,find(freqs_unamb==theta(1)):find(freqs_unamb==theta(2))),2);
thetaSpecAmb=mean(spectra_amb(:,find(freqs_amb==theta(1)):find(freqs_amb==theta(2))),2);
topoplot(thetaSpecUnamb, ALLEEG(1).chanlocs);
title(sprintf('Unamb theta Topo %d to %d hz', theta(1), theta(2))); 
colorbar
subplot(2,3,4)
topoplot(del2map(thetaSpecUnamb, ALLEEG(1).chanlocs), ALLEEG(1).chanlocs);
title(sprintf('Unamb CSD theta %d to %d hz', theta(1), theta(2))); 
colorbar
subplot(2,3,2)
topoplot(thetaSpecAmb, ALLEEG(1).chanlocs);
title(sprintf('Amb theta Topo %d to %d hz', theta(1), theta(2))); 
colorbar
subplot(2,3,5)
topoplot(del2map(thetaSpecAmb, ALLEEG(1).chanlocs), ALLEEG(1).chanlocs);
title(sprintf('Amb CSD theta %d to %d hz', theta(1), theta(2))); 
colorbar
subplot(2,3,3)
topoplot(thetaSpecAmb-thetaSpecUnamb, ALLEEG(1).chanlocs);
title(sprintf('Amb-Unamb theta Topo %d to %d hz', theta(1), theta(2))); 
colorbar
subplot(2,3,6)
topoplot(del2map(thetaSpecAmb-thetaSpecUnamb, ALLEEG(1).chanlocs), ALLEEG(1).chanlocs);
title(sprintf('Amb-Unamb CSD theta %d to %d hz', theta(1), theta(2))); 
colorbar

% alpha Topo & laplacian
figure; 
subplot(2,3,1)
alphaSpecUnamb=mean(spectra_unamb(:,find(freqs_unamb==alpha(1)):find(freqs_unamb==alpha(2))),2);
alphaSpecAmb=mean(spectra_amb(:,find(freqs_amb==alpha(1)):find(freqs_amb==alpha(2))),2);
topoplot(alphaSpecUnamb, ALLEEG(1).chanlocs);
title(sprintf('Unamb alpha Topo %d to %d hz', alpha(1), alpha(2))); 
colorbar
subplot(2,3,4)
topoplot(del2map(alphaSpecUnamb, ALLEEG(1).chanlocs), ALLEEG(1).chanlocs);
title(sprintf('Unamb CSD alpha %d to %d hz', alpha(1), alpha(2))); 
colorbar
subplot(2,3,2)
topoplot(alphaSpecAmb, ALLEEG(1).chanlocs);
title(sprintf('Amb alpha Topo %d to %d hz', alpha(1), alpha(2))); 
colorbar
subplot(2,3,5)
topoplot(del2map(alphaSpecAmb, ALLEEG(1).chanlocs), ALLEEG(1).chanlocs);
title(sprintf('Amb CSD alpha %d to %d hz', alpha(1), alpha(2))); 
colorbar
subplot(2,3,3)
topoplot(alphaSpecAmb-alphaSpecUnamb, ALLEEG(1).chanlocs);
title(sprintf('Amb-Unamb alpha Topo %d to %d hz', alpha(1), alpha(2))); 
colorbar
subplot(2,3,6)
topoplot(del2map(alphaSpecAmb-alphaSpecUnamb, ALLEEG(1).chanlocs), ALLEEG(1).chanlocs);
title(sprintf('Amb-Unamb CSD alpha %d to %d hz', alpha(1), alpha(2))); 
colorbar

% beta Topo & laplacian
figure; 
subplot(2,3,1)
betaSpecUnamb=mean(spectra_unamb(:,find(freqs_unamb==beta(1)):find(freqs_unamb==beta(2))),2);
betaSpecAmb=mean(spectra_amb(:,find(freqs_amb==beta(1)):find(freqs_amb==beta(2))),2);
topoplot(betaSpecUnamb, ALLEEG(1).chanlocs);
title(sprintf('Unamb beta Topo %d to %d hz', beta(1), beta(2))); 
colorbar
subplot(2,3,4)
topoplot(del2map(betaSpecUnamb, ALLEEG(1).chanlocs), ALLEEG(1).chanlocs);
title(sprintf('Unamb CSD beta %d to %d hz', beta(1), beta(2))); 
colorbar
subplot(2,3,2)
topoplot(betaSpecAmb, ALLEEG(1).chanlocs);
title(sprintf('Amb beta Topo %d to %d hz', beta(1), beta(2))); 
colorbar
subplot(2,3,5)
topoplot(del2map(betaSpecAmb, ALLEEG(1).chanlocs), ALLEEG(1).chanlocs);
title(sprintf('Amb CSD beta %d to %d hz', beta(1), beta(2))); 
colorbar
subplot(2,3,3)
topoplot(betaSpecAmb-betaSpecUnamb, ALLEEG(1).chanlocs);
title(sprintf('Amb-Unamb beta Topo %d to %d hz', beta(1), beta(2))); 
colorbar
subplot(2,3,6)
topoplot(del2map(betaSpecAmb-betaSpecUnamb, ALLEEG(1).chanlocs), ALLEEG(1).chanlocs);
title(sprintf('Amb-Unamb CSD beta %d to %d hz', beta(1), beta(2))); 
colorbar

% gamma Topo & laplacian
figure; 
subplot(2,3,1)
gammaSpecUnamb=mean(spectra_unamb(:,find(freqs_unamb==gamma(1)):find(freqs_unamb==gamma(2))),2);
gammaSpecAmb=mean(spectra_amb(:,find(freqs_amb==gamma(1)):find(freqs_amb==gamma(2))),2);
topoplot(gammaSpecUnamb, ALLEEG(1).chanlocs);
title(sprintf('Unamb gamma Topo %d to %d hz', gamma(1), gamma(2))); 
colorbar
subplot(2,3,4)
topoplot(del2map(gammaSpecUnamb, ALLEEG(1).chanlocs), ALLEEG(1).chanlocs);
title(sprintf('Unamb CSD gamma %d to %d hz', gamma(1), gamma(2))); 
colorbar
subplot(2,3,2)
topoplot(gammaSpecAmb, ALLEEG(1).chanlocs);
title(sprintf('Amb gamma Topo %d to %d hz', gamma(1), gamma(2))); 
colorbar
subplot(2,3,5)
topoplot(del2map(gammaSpecAmb, ALLEEG(1).chanlocs), ALLEEG(1).chanlocs);
title(sprintf('Amb CSD gamma %d to %d hz', gamma(1), gamma(2))); 
colorbar
subplot(2,3,3)
topoplot(gammaSpecAmb-gammaSpecUnamb, ALLEEG(1).chanlocs);
title(sprintf('Amb-Unamb gamma Topo %d to %d hz', gamma(1), gamma(2))); 
colorbar
subplot(2,3,6)
topoplot(del2map(gammaSpecAmb-gammaSpecUnamb, ALLEEG(1).chanlocs), ALLEEG(1).chanlocs);
title(sprintf('Amb-Unamb CSD gamma %d to %d hz', gamma(1), gamma(2))); 
colorbar