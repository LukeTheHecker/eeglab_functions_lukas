%% Scalp Maps + CSD f�r P200

[STUDY erpdata erptimes pgroup pcond pinter] = std_erpplot(STUDY, ALLEEG);

% lat_amb = permute(erpdata{1},[3 2 1]);
% lat_unamb = permute(erpdata{2},[3 2 1]);
smi_amb =  permute(erpdata{1},[3 2 1]);
smi_unamb =  permute(erpdata{2},[3 2 1]);
% CSD stuff
for i = 1:length(ALLEEG(1).chanlocs)
    chans(i) = {ALLEEG(1).chanlocs(i).labels};
end
chans=chans.';
pause(1)
[M] = ExtractMontage ('10-5-System_Mastoids_EGI129.csd', chans);
% MapMontage(M)
pause(1)
[G,H] = GetGH(M); %default: 4, otherwise e.g.: GetGH(M, 3)


figure;
plot(erptimes.', squeeze(mean(lat_amb(:,14,:),1)),'r')
hold on
plot(erptimes.', squeeze(mean(lat_unamb(:,14,:),1)),'k')
hold on
plot(erptimes.', squeeze(mean(smi_amb(:,14,:),1)),'r--')
hold on
plot(erptimes.', squeeze(mean(smi_unamb(:,14,:),1)),'k--')

lat_amb_gm = squeeze(mean(lat_amb,1));
smi_amb_gm = squeeze(mean(smi_amb,1));
lat_unamb_gm = squeeze(mean(lat_unamb,1));
smi_unamb_gm = squeeze(mean(smi_unamb,1));

myrange=find(erptimes==180):find(erptimes==220);

figure;
suptitle('Common Average Referenced Maps')
subplot(2,3,1)
topoplot(squeeze(mean(lat_unamb_gm(:,myrange),2)),ALLEEG(1).chanlocs)
title('Lat Unamb')
subplot(2,3,2)
topoplot(squeeze(mean(lat_amb_gm(:,myrange),2)),ALLEEG(1).chanlocs)
title('Lat Amb')
subplot(2,3,3)
topoplot(squeeze(mean(lat_unamb_gm(:,myrange),2))-squeeze(mean(lat_amb_gm(:,myrange),2)),ALLEEG(1).chanlocs)
title('Lat U-A')
subplot(2,3,4)
topoplot(squeeze(mean(smi_unamb_gm(:,myrange),2)),ALLEEG(1).chanlocs)
title('Smi Unamb')
subplot(2,3,5)
topoplot(squeeze(mean(smi_amb_gm(:,myrange),2)),ALLEEG(1).chanlocs)
title('Smi Amb')
subplot(2,3,6)
topoplot(squeeze(mean(smi_unamb_gm(:,myrange),2))-squeeze(mean(smi_amb_gm(:,myrange),2)),ALLEEG(1).chanlocs)
title('Smi U-A')
print('/Applications/eeglab14_1_2b/P200_CommonAvgRef','-dpng','-r300');


figure;
suptitle('Current Source Density Maps')

subplot(2,3,1)
CSD_tmp = CSD(squeeze(mean(lat_unamb_gm(:,myrange),2)),G,H);
topoplot(CSD_tmp,ALLEEG(1).chanlocs)
title('Lat Unamb')

subplot(2,3,2)
CSD_tmp = CSD(squeeze(mean(lat_amb_gm(:,myrange),2)),G,H);
topoplot(CSD_tmp,ALLEEG(1).chanlocs)
title('Lat Amb')

subplot(2,3,3)
CSD_tmp = CSD(squeeze(mean(lat_unamb_gm(:,myrange),2))-squeeze(mean(lat_amb_gm(:,myrange),2)),G,H);
topoplot(CSD_tmp,ALLEEG(1).chanlocs)
title('Lat U-A')

subplot(2,3,4)
CSD_tmp = CSD(squeeze(mean(smi_unamb_gm(:,myrange),2)),G,H);
topoplot(CSD_tmp,ALLEEG(1).chanlocs)
title('Smi Unamb')

subplot(2,3,5)
CSD_tmp = CSD(squeeze(mean(smi_amb_gm(:,myrange),2)),G,H);
topoplot(CSD_tmp,ALLEEG(1).chanlocs)
title('Smi Amb')

subplot(2,3,6)
CSD_tmp = CSD(squeeze(mean(smi_unamb_gm(:,myrange),2))-squeeze(mean(smi_amb_gm(:,myrange),2)),G,H);
topoplot(CSD_tmp,ALLEEG(1).chanlocs)
title('Smi U-A')

print('/Applications/eeglab14_1_2b/P200_CSD','-dpng','-r300');

