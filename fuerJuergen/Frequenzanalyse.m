% frequenzanalyse j�rgen
% Baseline f�r Frequenzanalyse ben�tigt? Baseline by division/subtraction?
%% Create random data sets
myTime = -60:4:800;
nsub = 20;
nbchan = 32;
ntrial = 100;
mydata = NaN(nsub,nbchan,length(myTime),ntrial);
for k = 1:nsub
    for l = 1:ntrial
        mydata(k,:,:,l) = randn(32,length(myTime))+randn(1,1);
    end
end

%% Do discrete fft using hamming windows:
% EEGLABs "spectopo()" function uses matlabs "pwelch()" function for fft

% pre-allocate empty matrix filled with NaN
spectra = NaN(nsub,nbchan,109,ntrial);
for k = 1:nsub
    for l = 1:ntrial
        [spectra(k,:,:,l),freqs] = spectopo(squeeze(mydata(k,:,:,l)), 0, 250,'plot','off','freqrange',[1 120],'wintype','hamming');
        fprintf('sub %d trial %d', k, l)
    end
end

% mean across respective trials subjects
subMeanSpec = squeeze(mean(spectra,4));
% mean across all subjects
allMeanSpec = squeeze(mean(subMeanSpec,2));

% Plot it
figure;
plot(freqs,allMeanSpec(14,:),'k')
title('My title')
legend('All-averaged Spectra')
