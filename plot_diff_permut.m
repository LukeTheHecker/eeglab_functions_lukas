% 
% 
% pVals_LD_latrev_Cz_2(1:10,:) = pVals_LDF_latrev_Cz(1:10);
% diffs_LD_latrev_Cz_2 = diffs_LDF_latrev_Cz([1:10 13:21],:);
% effects_LD_latrev_Cz_2 = effects_LD_latrev_Cz_2([1:10 13:21],:);

figure;
ax1=subplot(2,1,1);
imagesc([-300:1099],[1:19],pVals_SD_latrev_Cz_2);
title(ax1, 'pVals LD latrev Cz');
colorbar;colormap(flipud(jet));
caxis([0 0.1]);

ax2=subplot(2,1,2);
plot([-300:2:1099],mean(pVals_SD_latrev_Cz_2,1))
title(ax2, 'Grand Mean');

figure;
ax1=subplot(2,1,1);
imagesc([-300:1099],[1:19],diffs_SD_latrev_Cz_2);
title(ax1, 'diffs LD latrev Cz');
colorbar;colormap(jet);

ax2=subplot(2,1,2);
plot([-300:2:1099],mean(diffs_SD_latrev_Cz_2,1))
title(ax2, 'Grand Mean');

figure;
ax1=subplot(2,1,1);
imagesc([-300:1099],[1:19],effects_SD_latrev_Cz_2);
title(ax1, 'effects LD latrev Cz');
colorbar;colormap(jet);

ax2=subplot(2,1,2);
plot([-300:2:1099],mean(effects_SD_latrev_Cz_2,1))
title(ax2, 'Grand Mean');

figure;
norm_diffs = zeros(19,700);
for i = 1:19
    norm_diffs(i,:) = diffs_SD_latrev_Cz(i,:)/max(abs(diffs_SD_latrev_Cz(i,:)));
end
ax1=subplot(2,1,1);
imagesc([-300:1099],[1:19],norm_diffs);
title(ax1, 'Normierte Differenzen');
colorbar;colormap(jet);

ax2=subplot(2,1,2);
plot([-300:2:1099],mean(norm_diffs,1))
title(ax2, 'Grand Mean');

figure;

varToPlot = -1*diffs_LDF_latrev_Cz;
e1 = zeros(size(mean(varToPlot,1)));
for i=1:length(varToPlot(1,:)); e1(i) = std(varToPlot(:,i))/sqrt(length(varToPlot(:,i)));end
[l,p] = boundedline(-300:2:1099, mean(varToPlot,1), e1, '-r');
outlinebounds(l,p);
title('diffs_LDF_latrev_Cz_2');

hold on
varToPlot = -1*diffs_SDF_latrev_Cz;
e1 = zeros(size(mean(varToPlot,1)));
for i=1:length(varToPlot(1,:)); e1(i) = std(varToPlot(:,i))/sqrt(length(varToPlot(:,i)));end
[l,p] = boundedline(-300:2:1099, mean(varToPlot,1), e1, '-b');
outlinebounds(l,p);
title('diffs_SDF_latrev_Cz');

hold on

plot([-300 1099],[0 0],'k',...
    'LineWidth',2)
axis([-300 1099 -inf inf]);

% % smoothed erp image:
% 
% [X,Y] = meshgrid(1:size(pVals_LD_latrev_Cz_2(:,:),2), 1:size(pVals_LD_latrev_Cz_2(:,:),1));
% 
% %// Define a finer grid of points
% [X2,Y2] = meshgrid(1:0.01:size(pVals_LD_latrev_Cz_2(:,:),2), 1:0.01:size(pVals_LD_latrev_Cz_2(:,:),1));
% 
% %// Interpolate the data and show the output
% outData = interp2(X, Y, pVals_LD_latrev_Cz_2(:,:), X2, Y2, 'linear');
% figure;imagesc(outData);
% 
% % // Cosmetic changes for the axes
% % set(gca, 'XTick', linspace(1,size(X2,2),size(X,2))); 
% % set(gca, 'YTick', linspace(1,size(X2,1),size(X,1)));
% % set(gca, 'XTickLabel', 1:size(X,2));
% % set(gca, 'YTickLabel', 1:size(X,1));
% 
% % // Add colour bar
% colorbar;
% colormap(jet)

