%STUDY = pop_statparams(STUDY, 'condstats','on','statistics','perm','alpha',0.001);
STUDY = pop_statparams(STUDY, 'condstats','on','alpha',0.01);

for i = 3
    [STUDY erspdata ersptimes erspfreqs pgroup pcond pinter] = std_erspplot(STUDY,ALLEEG,'clusters',i, 'plotsubjects', 'on','noplot','on');
    [STUDY itcdata itctimes itcfreqs pgroup pcond pinter] = std_itcplot(STUDY,ALLEEG,'clusters',i, 'plotsubjects', 'on','noplot','on');
    meanERSPcond1 = mean(erspdata{1},3);
    meanERSPcond2 = mean(erspdata{2},3);
    meanITCcond1 = mean(itcdata{1},3);
    meanITCcond2 = mean(itcdata{2},3);
    maskedImageERSP = (meanERSPcond1-meanERSPcond2).*(cell2mat(pcond));
    maskedImageITC = (meanITCcond1-meanITCcond2).*(cell2mat(pcond));
    std_plottf(ersptimes, erspfreqs, {maskedImageERSP},'titles',{['Cls' num2str(i) ' ERSP, Condition1 - Condition2, p < .05']});
    std_plottf(itctimes, itcfreqs, {maskedImageITC},'titles',{['Cls' num2str(i) ' ITC, Condition1 - Condition2, p < .05']});

end