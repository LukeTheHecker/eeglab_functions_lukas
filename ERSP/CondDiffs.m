STUDY = pop_statparams(STUDY, 'condstats','on','alpha',0.01);

% for i = allClusters
figure;
[STUDY erspdata ersptimes erspfreqs pgroup ersppcond pinter] = ...
    std_erspplot(STUDY,ALLEEG,'channels',{ALLEEG(1).chanlocs.labels}, ...
    'plotsubjects', 'on','noplot','on');

meanERSPcond1 = squeeze(mean(erspdata{1},4));
meanERSPcond2 = squeeze(mean(erspdata{2},4));

maskedImage = (meanERSPcond1-meanERSPcond2).*(cell2mat(ersppcond));

for k = 1:ALLEEG(1).nbchan
    std_plottf(ersptimes, erspfreqs, {maskedImage(:,:,k)},'titles',{['ERSP, Condition1 - Condition2, p < .05']});
end

timefreqs = [50 100 20 30;...
    200 500 20 30;...
    450 700 25 35;...
    350 450 5 10];


figure
tftopo(maskedImage,ersptimes,erspfreqs,'chanlocs',ALLEEG(1).chanlocs,'timefreqs',timefreqs)

figure
erpimage(permute(maskedImage,[2 1 3]),[],ersptimes);

figure; 
metaplottopo( permute(maskedImage,[3 1 2]), 'plotfunc', 'newtimef', 'chanlocs', ...
    ALLEEG(1).chanlocs, 'plotargs', {ALLEEG(1).pnts, [EEG.xmin EEG.xmax]*1000, ...
    EEG.srate, [0], 'plotitc', 'off', 'ntimesout', 50, 'padratio', 1});

figure; 
metaplottopo(EEG.data, 'plotfunc', 'erpimage', 'plotargs',...
    { eeg_getepochevent( EEG, {'rt'},[],'latency'), ...
    linspace(EEG.xmin*1000, EEG.xmax*1000, EEG.pnts), '', 10, 1 ,...
    'erp','off','cbar','off' }, 'chanlocs', EEG.chanlocs);


