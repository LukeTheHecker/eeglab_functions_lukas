function [outdata]=Baseline_corr(data, range, nchans, ntrials, x_scale)
%data = channel x time x trials
outdata=data;
[~,loInd] = min(abs(x_scale-range(1)));
[~,hiInd] = min(abs(x_scale-range(2)));

for i=1:nchans
    for h = 1:ntrials
        bl_tmp = mean(data(i, loInd:hiInd,h));
        outdata(i, :, h) = data(i, :, h)-bl_tmp;
    end
end