LAF_subAvg = zeros(32, 700, 20);
LDF_subAvg = zeros(32, 700, 20);

for i = 1:length(ALLEEG)/2
    EEG = pop_loadset(ALLEEG(i).filename, ALLEEG(i).filepath);
    LAF_subAvg(:,:,i) = mean(EEG.data,3);
    clear EEG
end
for i = 1:length(ALLEEG)/2
    EEG = pop_loadset(ALLEEG(i+20).filename, ALLEEG(i+20).filepath);
    LDF_subAvg(:,:,i) = mean(EEG.data,3);
    clear EEG
end
GM_LAF = ALLEEG(1);
GM_LAF.data = LAF_subAvg;
GM_LAF.trials = 20;

GM_LDF = ALLEEG(1);
GM_LDF.data = LDF_subAvg;
GM_LDF.trials = 20;
%E_LDF = std(LDF_subAvg,2)./sqrt(size(LDF_subAvg,2));
for i=1:20
    figure;
    plot(GM_LDF.times, LDF_subAvg(14,:,i))
    plot(GM_LAF.times, LAF_subAvg(14,:,i))
end

GM_diff = ALLEEG(i);
GM_diff.data = LDF_subAvg- LAF_subAvg;
GM_diff.trials = 20;
figure; pop_timtopo(GM_LDF, [-300  1098], [200], 'ERP data and scalp maps of AD_AF_STAB_filtered_eventcorr_avgref_noartifact');
title('Lattice Unamb FF at 200 ms')
% plot(GM_LDF.times, mean_LDF(14,:), 'LineWidth', 4)
%boundedline(GM_LDF.times, mean(GM_LDF(14,:)), E_LDF)

figure; pop_timtopo(GM_LDF, [-300  1098], [380], 'ERP data and scalp maps of AD_AF_STAB_filtered_eventcorr_avgref_noartifact');
title('Lattice Unamb FF at 380 ms')
figure; pop_timtopo(GM_LAF, [-300  1098], [200], 'ERP data and scalp maps of AD_AF_STAB_filtered_eventcorr_avgref_noartifact');
title('Lattice Amb FF at 200 ms')
figure; pop_timtopo(GM_LAF, [-300  1098], [380], 'ERP data and scalp maps of AD_AF_STAB_filtered_eventcorr_avgref_noartifact');
title('Lattice Amb FF at 380 ms')
figure; pop_timtopo(GM_diff, [-300  1098], [200], 'ERP data and scalp maps of AD_AF_STAB_filtered_eventcorr_avgref_noartifact');
title('Lattice U-A FF at 200 ms')
figure; pop_timtopo(GM_diff, [-300  1098], [380], 'ERP data and scalp maps of AD_AF_STAB_filtered_eventcorr_avgref_noartifact');
title('Lattice U-A FF at 380 ms')

mean_LAF = mean(GM_LAF.data,3);
mean_LDF = mean(GM_LDF.data,3);
mean_diff = mean(GM_diff.data,3);

figure; plot(GM_LDF.times, mean_LDF(14,:))
figure; plot(GM_LAF.times, mean_LAF(14,:))
figure; plot(GM_diff.times, mean_diff(14,:))