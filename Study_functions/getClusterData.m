function [nameOut erpOut scalpOut specOut dipOut] = getClusterData(varargin)

global STUDY
global ALLEEG

dest = "/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/Cluster_Correlation/";

nameMat = [];
erpMat = [];
scalpMat = [];
specMat = [];
dipMat = [];
ClsArr = cell2mat(varargin);



if isempty(ClsArr)
    disp("Taking all Clusters")
    clsRange=3:length(STUDY.cluster);
else
    clsRange=1:length(ClsArr);
end

for i=clsRange
    disp(["Cluster no. ", i])
    % Create cell array of cluster names for later
    nameMat = [nameMat, convertCharsToStrings(STUDY.cluster(i).name)];
    [STUDY, erpdata] = std_erpplot(STUDY,ALLEEG, 'clusters', i, 'comps', 'all', 'noplot', 'on');
    erpMat = [erpMat, mean(cell2mat(erpdata(2)),2)];
    
    scalpMat = [scalpMat, reshape(STUDY.cluster(i).topo, [67*67,1])];
    
    [STUDY, specdata] = std_specplot(STUDY, ALLEEG, 'clusters', i, 'comps', 'all', 'noplot', 'on');
    specMat = [specMat, mean(cell2mat(specdata(2)),2)]; % reshape(mean(cell2mat(specdata),2), [30,1])];
    
    %[STUDY] = std_dipplot(STUDY, ALLEEG, 'clusters',i,'figure','off');
    dipMat = [dipMat, reshape(STUDY.cluster(i).dipole.posxyz,[3,1])];
    
end
nameMat = nameMat';
nameOut = nameMat;
erpOut = erpMat;
scalpOut = scalpMat;
specOut = specMat;
dipOut = dipMat;

save(char(strcat(dest, "dipLattices.mat")), 'dipOut');
save(char(strcat(dest, "erpLattices.mat")), 'erpOut');
save(char(strcat(dest, "specLattices.mat")), 'specOut');
save(char(strcat(dest, "scalpLattices.mat")), 'scalpOut');
save(char(strcat(dest, "namesLattices.mat")), 'nameOut');


end