% This function calculates the area under curve of your selected
% IC-cluster/s backprojected to time*electrodes and its proportion to a complete backprojection of all
% IC-clusters.


% todo: baseline correction: baseline =mean(allclust_erp_amb(channel,find(x_scale==-0.06):find(x_scale==0.04)));
% plot(x_scale, allclust_erp_m_unamb(14,:)-baseline)


function [auc_prop_amb, auc_prop_unamb, sig_arr, allclust_erp_amb, allclust_erp_unamb] = cluster_auc(STUDY, ALLEEG, EEG, channel,ClusterInclude, ClusterExclude, aucRange)
% usage: cluster_auc(STUDY, ALLEEG, EEG, 14,[16], [17],[0.1 0.6])
% means: show me how much of the grand mean erp at Cz between 100 and 600 ms
% can be explained by Cluster 16. Additionally, exclude cluster 17 from the
% analysis (e.g. because it's eye artifacts.

global STUDY
global ALLEEG
x_scale = ALLEEG(1).xmin:(1/ALLEEG(1).srate):ALLEEG(1).xmax; 
x_scale = round(x_scale,3);

% set design:
% Amibguous Stimuli:
STUDY = std_makedesign(STUDY, ALLEEG, 1, 'variable1','type','variable2','','name','STUDY.design 1','pairing1','on','pairing2','on','delfiles','off','defaultdesign','off','values1',{'amb_stability'},'subjselect',{'AA' 'AC' 'AD' 'AE' 'AF' 'AG' 'AH' 'AI' 'AJ' 'AK' 'AL' 'AM' 'AN' 'AO' 'AP' 'AQ' 'AR' 'AS' 'AT' 'AU' 'AV'});

% get backprojected ERP of the Clusters of interest:
% backproject selected IC Clusters and store Cluster-Erps in variable:
std_selectICsByCluster(STUDY, ALLEEG, EEG, ClusterInclude,[], '');
coi_erp_amb = STUDY.selectICsByCluster.selectICsByClusterErp;
% backproject all IC Clusters (except for excluded ones) and store ERPs in
% another variable.

fileExists1 = exist('allclust_erp_amb');

if fileExists1 == 0
    disp('GOES IN GOES IN GOES IN GOES IN GOES IN')
    std_selectICsByCluster(STUDY, ALLEEG, EEG, [2:length(STUDY.cluster)],ClusterExclude, '');
    allclust_erp_amb = STUDY.selectICsByCluster.selectICsByClusterErp;
end
% baseline correction for grand mean and...
for h = 1:length(allclust_erp_amb(channel,1,:)) 
    bl_tmp = mean(allclust_erp_amb(channel, find(x_scale==-0.06):find(x_scale==0.04),h));
    allclust_erp_amb(channel, :, h) = allclust_erp_amb(channel, :, h)-bl_tmp;
end
% ...for COI
for h = 1:length(coi_erp_amb(channel,1,:)) 
    bl_tmp = mean(coi_erp_amb(channel, find(x_scale==-0.06):find(x_scale==0.04),h));
    coi_erp_amb(channel, :, h) = coi_erp_amb(channel, :, h)-bl_tmp;
end
% calculate avg over all subjects:
coi_erp_m_amb = mean(coi_erp_amb,3);
allclust_erp_m_amb = mean(allclust_erp_amb,3);
% calculate area under curve and get proportion:
coi_auc_amb = trapz(coi_erp_m_amb(channel,find(x_scale==aucRange(1)):find(x_scale==aucRange(2))));
allclust_auc_amb = trapz(allclust_erp_m_amb(channel,find(x_scale==aucRange(1)):find(x_scale==aucRange(2))));
auc_prop_amb = coi_auc_amb/allclust_auc_amb;

% set design:
% Unambiguous Stimuli:
STUDY = std_makedesign(STUDY, ALLEEG, 1, 'variable1','type','variable2','','name','STUDY.design 1','pairing1','on','pairing2','on','delfiles','off','defaultdesign','off','values1',{'unamb_stability'},'subjselect',{'AA' 'AC' 'AD' 'AE' 'AF' 'AG' 'AH' 'AI' 'AJ' 'AK' 'AL' 'AM' 'AN' 'AO' 'AP' 'AQ' 'AR' 'AS' 'AT' 'AU' 'AV'});
std_selectICsByCluster(STUDY, ALLEEG, EEG, ClusterInclude,[], '');
coi_erp_unamb = STUDY.selectICsByCluster.selectICsByClusterErp;
%coi_erp_unamb = 
% backproject all IC Clusters (except for excluded ones) and store ERPs in
% another variable.
fileExists2 = exist('allclust_erp_unamb');
if fileExists2 == 0
    disp('GOES IN GOES IN GOES IN GOES IN GOES IN')
    std_selectICsByCluster(STUDY, ALLEEG, EEG, [2:length(STUDY.cluster)],ClusterExclude, '');
    allclust_erp_unamb = STUDY.selectICsByCluster.selectICsByClusterErp;
end
% baseline correction:
for h = 1:length(allclust_erp_unamb(channel,1,:)) 
    bl_tmp = mean(allclust_erp_unamb(channel, find(x_scale==-0.06):find(x_scale==0.04),h));
    allclust_erp_unamb(channel, :, h) = allclust_erp_unamb(channel, :, h)-bl_tmp;
end
% baseline correction:
for h = 1:length(coi_erp_unamb(channel,1,:)) 
    bl_tmp = mean(coi_erp_unamb(channel, find(x_scale==-0.06):find(x_scale==0.04),h));
    coi_erp_unamb(channel, :, h) = coi_erp_unamb(channel, :, h)-bl_tmp;
end
% calculate avg over all subjects:
coi_erp_m_unamb = mean(coi_erp_unamb,3);
allclust_erp_m_unamb = mean(allclust_erp_unamb,3);
% calculate area under curve and get proportion:
coi_auc_unamb = trapz(coi_erp_m_unamb(channel,find(x_scale==aucRange(1)):find(x_scale==aucRange(2))));
allclust_auc_unamb = trapz(allclust_erp_m_unamb(channel,find(x_scale==aucRange(1)):find(x_scale==aucRange(2))));
auc_prop_unamb = coi_auc_unamb/allclust_auc_unamb;

% test for significant differences between Amb cluster ERP and Unamb
% cluster ERP at given electrode.
sig_arr = NaN(1,length(x_scale));
for i = find(x_scale==aucRange(1)):find(x_scale==aucRange(2))
    a_tmp=[];
    u_tmp=[];
    for n=1:length(coi_erp_amb(channel,i,:))
        a_tmp = [a_tmp, coi_erp_amb(channel,i,n)];
        u_tmp = [u_tmp, coi_erp_unamb(channel,i,n)];
    end
    [p, ~, ~]=permutationTest(a_tmp, u_tmp, 5000 );%, 'plotresult',1, 'showprogress', 250 );
    if p<0.05
        sig_arr(i) = 1;
    end
%     pVals_LD_latrev_Cz(i, i)=p;
%     diffs_LD_latrev_Cz(i, i)=observeddifference;
%     effects_LD_latrev_Cz(i, i)=effectsize;
    i
end



f1 = figure;
title(sprintf('Cluster/s %d on channel %d ',ClusterInclude, channel))
plot(x_scale,coi_erp_m_unamb(channel,:), 'Color', [0 0.4510 0.7412],'LineWidth',3)
hold on
plot(x_scale,coi_erp_m_amb(channel,:), 'Color', [0.3020 0.7490 0.9294],'LineWidth',3)
hold on
plot(x_scale,allclust_erp_m_unamb(channel,:), 'Color', [1  0  0],'LineWidth',3)
hold on
plot(x_scale,allclust_erp_m_amb(channel,:), 'Color', [0.9020 0.5020 0.2000],'LineWidth',3)
legend(sprintf('Cluster/s %d on channel %d Unamb',ClusterInclude, channel),sprintf('Cluster/s %d on channel %d Amb',ClusterInclude, channel),sprintf('Grand Mean Unamb', channel),sprintf('Grand Mean Amb', channel))
hold on
% vertical lines temporal ROI
plot( [aucRange(1) aucRange(1)], [min(allclust_erp_m_unamb(channel,:))*2 max(allclust_erp_m_unamb(channel,:))*2],'--','Color',[0 0 0]+0.05*10,'HandleVisibility','off')
hold on
plot( [aucRange(2) aucRange(2)], [min(allclust_erp_m_unamb(channel,:))*2 max(allclust_erp_m_unamb(channel,:))*2],'--','Color',[0 0 0]+0.05*10,'HandleVisibility','off')
hold on
% horizontal line
plot([-1 2],[0 0], '-k','HandleVisibility','off')
hold on
% plot testresults
scatter(x_scale,sig_arr*0.15, 'bs','HandleVisibility','off')

% Text Box
% dim = [.2 .5 .3 .3];
% str = sprintf('Clusters explains %d%% in Amb condition and %d%% in Unamb condition.',auc_prop_amb*100, auc_prop_unamb*100);
% annotation('textbox',dim,'String',str,'FitBoxToText','on');

% set axis properties
axis([-0.3 1.1 min(allclust_erp_m_unamb(channel,:))*1.1 max(allclust_erp_m_unamb(channel,:))*1.1 ]);
% get cluster name and store file
clustername = STUDY.cluster(ClusterInclude).name(find(~isspace(STUDY.cluster(ClusterInclude).name)));
saveas(f1, ['/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/COI-Analyse/' clustername '.png']);
saveas(f1, ['/Users/lukashecker/Desktop/Lukas_Hecker/fMRI/DataAnalysis/COI-Analyse/' clustername '.fig']);

end



