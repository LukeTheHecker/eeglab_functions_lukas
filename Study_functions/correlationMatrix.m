function erpMat = correlationMatrix(varargin)
% Returns the correlation matrix of mean Cluster ERPs.
% Example:
% 
% myMatrix = correlationMatrix([14 15]) --> saves correlation matrix for
% Cluster 14 and 15 to variable myMatrix in workspace
% 
% If no cluster are given as input it returns the correlation matrix for
% all clusters available.
% colNames = {'Parietal P2', 'Occipital P2', 'Central P4', 'Temporal N170', 'Central P3'};
global STUDY
global ALLEEG
erpMat = [];
% topoMat = [];
ClsArr=cell2mat(varargin);

if isempty(ClsArr)
    disp("Taking all Cluster")
    ClsArr=1:length(STUDY.cluster);
end

for i=1:length(ClsArr)
    [STUDY, erpdata] = std_erpplot(STUDY,ALLEEG, 'clusters', ClsArr(i), 'comps', 'all', 'noplot', 'on');
    erpMat = [erpMat, mean(cell2mat(erpdata),2)];
end

% [Rtable, Ptable]=corrcoef(erpMat);
% rMat = correlation matrix cotaining correlation coefficient 'r' (not
% 'r�' !!!)
% pMat = p values for testing the hypothesis that there is no relationship
% between the observed phenomena.

end