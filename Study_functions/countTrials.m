%% Count amount of trials in datasets
function countTrials(FileList, Conditions,Dest)
% FileList = dir(['/Users/lukashecker/Documents/Meditation/EEG_Meditation/processed/Kontrolle/N*H_*.set']);
% Conditions = {'go-stab','go-rev'};
% Dest=[];

if isempty(Dest)
    Dest = FileList(1).folder;
elseif ~isdir(Dest)
    return
end

fid = fopen( [Dest,'/nTrials.txt'], 'wt' );



for i = 1:length(FileList)
    fprintf(fid,'Sub %s\n', FileList(i).name(1:3))
    for j = 1:length(Conditions)
        
        EEG = pop_loadset(FileList(i).name,FileList(i).folder);
        try
            EEG = pop_epoch(EEG, Conditions(j), [0 0.5]);
            ntrials = EEG.trials;
        catch
            ntrials = 0;
        end
        fprintf(fid,'Trials in %s: %d\n', Conditions{j}, ntrials)
    end
end
fclose(fid);
end