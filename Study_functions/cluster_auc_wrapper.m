% backproject each cluster
for i = 9%1:length(STUDY.cluster)
    cluster_auc_lean(STUDY, ALLEEG,EEG,14, i, 5, [0.1 0.6], 'unamb_stability', 'amb_stability');    
end

% final COI analysis
cnt=0;
%Criterion: Explains at least 10% variance
pvafCrit = 0.1;
pValCrit = 0.05;
minSigPoints=40;
% electrode
pvaf_electrode = 14; % Cz

for i = 8%1:length(STUDY.etc.cluster_pvaf)
    STUDY.etc.cluster_pvaf(i).explainsEnoughVariance = 'No';
    STUDY.etc.cluster_pvaf(i).U_A_SigDiff = 'No';
    if nanmean(STUDY.etc.cluster_pvaf(i).pvaf_U(:,pvaf_electrode)) >= pvafCrit
        STUDY.etc.cluster_pvaf(i).explainsEnoughVariance = 'Yes';
        for j = STUDY.etc.cluster_pvaf(i).sig_arr_perm
            if j <= pValCrit
                cnt=cnt+1;
            elseif j > pValCrit
                cnt = 0;
            end
            if cnt >= minSigPoints
                STUDY.etc.cluster_pvaf(i).U_A_SigDiff = 'Yes';
            end
        end
    end
end


% plot all scalp maps
Clss=1:length(STUDY.AUC);
% save this by hand:
std_topoplot(STUDY,ALLEEG, 'clusters', Clss, 'mode', 'together');
% save table with all the info 
clusterTable = struct2table(STUDY.AUC);
clusterTable.Properties.RowNames = clusterTable.clusterNames;
filename = 'clustTable.mat'; 
mystruc = STUDY.AUC;
save(filename, 'mystruc');
% save the STUDY
[STUDY, EEG] = pop_savestudy( STUDY, EEG, 'savemode','resave');


        
        
        