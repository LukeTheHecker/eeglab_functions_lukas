% wrapper for cluster_auc_lean
auc_U = zeros(length(ALLEEG), length(STUDY.cluster)-2);
auc_A = zeros(length(ALLEEG), length(STUDY.cluster)-2);
for i = 3:length(STUDY.cluster)
    [tmp_U, tmp_A]=cluster_auc_lean(STUDY, ALLEEG, EEG, 14,[i], [5],[0.1 0.6], 'unamb_stability', 'amb_stability');
    auc_U(:,i-2) = tmp_U;
    auc_A(:,i-2) = tmp_A;
end