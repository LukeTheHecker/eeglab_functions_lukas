% [R, PValue]=corrplot([LatTab, SmlTab], 'alpha', 0.01,  'testR', 'on');
% [R, PValue]=corrplot([LatTab(250:700,:), SmlTab(250:700,:)], 'alpha', 0.01,  'testR', 'on');

latArr = [];
for i = 1:length(lat)
    if isequal(lat(i).U_A_SigDiff, 'Yes')
        latArr = [latArr lat(i)];
    end
end
smiArr = [];
for i = 1:length(smi)
    if isequal(smi(i).U_A_SigDiff, 'Yes')
        smiArr = [smiArr smi(i)];
    end
end
mouArr = [];
for i = 1:length(mou)
    if isequal(mou(i).U_A_SigDiff, 'Yes')
        mouArr = [mouArr mou(i)];
    end
end


allWinvLat = zeros(length(latArr(1).meanWinv),length(latArr));
latNames = strings(length(latArr),1);
for n = 1:length(latArr)
    tmp = convertCharsToStrings(latArr(n).clusterNames);
    latNames(n) = tmp;
    for i = 1:length(latArr(1).meanWinv)
        allWinvLat(i,n) = latArr(n).meanWinv(i);
    end
end

allWinvSmi = zeros(length(smiArr(1).meanWinv),length(smiArr));
smiNames = strings(length(smiArr),1);
for n = 1:length(smiArr)
    tmp = convertCharsToStrings(smiArr(n).clusterNames);
    smiNames(n) = tmp;
    for i = 1:length(smiArr(1).meanWinv)
        allWinvSmi(i,n) = smiArr(n).meanWinv(i);
    end
end

allWinvMou = zeros(length(mouArr(1).meanWinv),length(mouArr));
mouNames = strings(length(mouArr),1);
for n = 1:length(mouArr)
    tmp = convertCharsToStrings(mouArr(n).clusterNames);
    mouNames(n) = tmp;
    for i = 1:length(mouArr(1).meanWinv)
        allWinvMou(i,n) = mouArr(n).meanWinv(i);
    end
end


myCorr=corr(allWinvSmi,allWinvMou);

L=latNames;
S=smiNames;
M=mouNames;
imagesc(myCorr)
set(gca, 'Layer', 'top');
set(gca, 'XTick', 1:length(allWinvMou));
set(gca, 'YTick', 1:length(allWinvSmi)); % center y-axis ticks on bins
set(gca, 'XTickLabel', M, 'FontSize', 11); % set x-axis labels
set(gca, 'YTickLabel', S, 'FontSize', 11); % set y-axis labels
title('Smileys vs. Mouths Independent Component Cluster', 'FontSize', 14); % set title
xlabel('Mouths');
ylabel('Smileys');
colormap('jet');
colorbar; % enable colorbar

        
