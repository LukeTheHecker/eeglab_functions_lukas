%% This function calculates the area under curve of your selected
% IC-cluster/s backprojected to time*electrodes and its proportion to a complete backprojection of all
% IC-clusters.

function [pvaf_U, pvaf_A]=cluster_auc_lean(STUDY, ALLEEG, EEG, channel,ClusterInclude, ClusterExclude, aucRange, cond1, cond2)
%% usage: [pvaf_U, pvaf_A]=cluster_auc_lean(STUDY, ALLEEG, EEG, 14,[3], [17],[0.1 0.6], 'unamb_stability', 'amb_stability')
% means: show me how much of the grand mean erp at Cz (channel 14) between 
% 100 and 600 ms can be explained by components in cluster 3. 
% Components of individual subjects are backprojected toegether and AUC of
% these components is set in comparison to the subjects mean. 
% 
% Additionally, exclude cluster 17 from the analysis (e.g. because it's eye
% artifacts.
% Outputs:
% [auc_U, auc_A] = array containing explained variance of ICs within the
% backprojected clusters on the individual subjects data. 


global STUDY
global ALLEEG

% arrays of channel x timepoints x no. of Clusters

% How many different subs are in there?
subs=unique(STUDY.cluster(ClusterInclude).sets);
% create output arrays
pvaf_U = NaN(length(ALLEEG),32);
pvaf_A = NaN(length(ALLEEG),32);
U_clust_subMeans = NaN(length(ALLEEG),ALLEEG(1).nbchan, 1400);
A_clust_subMeans = NaN(length(ALLEEG),ALLEEG(1).nbchan, 1400);
cnt=0;

% Loop through subjects in the current cluster
for i = subs
    cnt=cnt+1;
    sub_components = STUDY.cluster(ClusterInclude).comps(find(STUDY.cluster(ClusterInclude).sets==i));
    current_set = pop_loadset('filename', ALLEEG(i).filename, 'filepath', ALLEEG(i).filepath);
    unamb_set = pop_epoch(current_set, {cond1}, [-0.3 1.1]);
    amb_set = pop_epoch(current_set, {cond2}, [-0.3 1.1]);
    % x-axis scaling
    x_scale = unamb_set.xmin:(1/unamb_set.srate):unamb_set.xmax; 
    x_scale = round(x_scale,3);
%     unamb_set = pop_subcomp( unamb_set, [ClusterExclude], 0);
%     unamb_set = eeg_checkset(unamb_set);
%     amb_set = pop_subcomp( amb_set, [ClusterExclude], 0);
%     amb_set = eeg_checkset(amb_set);
    backproj_U = zeros(unamb_set.nbchan, unamb_set.pnts, unamb_set.trials);
    backproj_U = zeros(amb_set.nbchan, amb_set.pnts, amb_set.trials);
    tmp_icaact_U = zeros(size(unamb_set.icaact));
    tmp_icaact_U(sub_components,:,:) = unamb_set.icaact(sub_components,:,:);
    tmp_icaact_A = zeros(size(amb_set.icaact));
    tmp_icaact_A(sub_components,:,:) = amb_set.icaact(sub_components,:,:);
    tmp_winv_U = zeros(size(unamb_set.icawinv));
    tmp_winv_U(:,sub_components) = unamb_set.icawinv(:,sub_components);
    tmp_winv_A = zeros(size(amb_set.icawinv));
    tmp_winv_A(:,sub_components) = amb_set.icawinv(:,sub_components);
    
    % Backprojecting to Unamb...
    for j = 1:unamb_set.trials
        backproj_U(:,:,j) = tmp_winv_U*unamb_set.icaact(:,:,j);
    end
    % and Amb..
    for j = 1:amb_set.trials
        backproj_A(:,:,j) = tmp_winv_U*amb_set.icaact(:,:,j);
    end
    %Baseline correction
    backproj_U = Baseline_corr(backproj_U, [-0.06 0.04], unamb_set.nbchan, unamb_set.trials, x_scale);
    backproj_A = Baseline_corr(backproj_A, [-0.06 0.04], amb_set.nbchan, amb_set.trials, x_scale);
    
    %Subject ERP
    % of full data:
    U_basecorr = Baseline_corr(unamb_set.data, [-0.06 0.04], unamb_set.nbchan, unamb_set.trials, x_scale);
    A_basecorr = Baseline_corr(amb_set.data, [-0.06 0.04], amb_set.nbchan, amb_set.trials, x_scale);
    % of backprojection
    U_clust_subMeans(i,:,:) = squeeze(mean(U_basecorr,3));
    A_clust_subMeans(i,:,:) = squeeze(mean(A_basecorr,3));
    % temporal region of interest indices
    tROI = find(x_scale==aucRange(1)):find(x_scale==aucRange(2));
    %% AUC ratio of backprojected components / subject mean
    % Full Data is taken in absolute values, then averaged, then AUC is
    % calculated in interval given by 'aucRange'

     tROI = find(x_scale==aucRange(1)):find(x_scale==aucRange(2));
%         
%     auc_mean_U = trapz(mean(abs(U_basecorr(channel,tROI)),3));
%     auc_mean_A = trapz(mean(abs(A_basecorr(channel,tROI)),3));
%     auc_ICs_U = trapz(mean(abs(backproj_U(channel,tROI)),3));
%     auc_ICs_A = trapz(mean(abs(backproj_A(channel,tROI)),3));
%     auc_ratio_U(i) = auc_ICs_U/auc_mean_U;
%     auc_ratio_A(i) = auc_ICs_A/auc_mean_A;
    %% Explained variance of backprojected components
    varU = mean(var(U_basecorr(:,tROI,:),0,2),3);
    varA = mean(var(A_basecorr(:,tROI,:),0,2),3);
    var_IC_U = mean(var(backproj_U(:,tROI,:),0,2),3);
    var_IC_A = mean(var(backproj_A(:,tROI,:),0,2),3);
    
    pvaf_U(i,:) = (100-100*(varU-var_IC_U)./varU);
    pvaf_A(i,:) = 100-100*(varA-var_IC_A)./varA;
end

%% Running statistical tests
% These are paired tests 

sig_arr_perm = NaN(1,length(x_scale));
sig_arr_wilc = NaN(1,length(x_scale));
sig_arr_ttest = NaN(1,length(x_scale));

for k = find(x_scale==aucRange(1)):find(x_scale==aucRange(2))
    fprintf('Significance tests running...')
    fprintf('')
    a_tmp = squeeze(A_clust_subMeans(:,channel,k));
    u_tmp = squeeze(U_clust_subMeans(:,channel,k));
    %minTrials=min(length(a_tmp), length(u_tmp));
    [sig_arr_perm(1,k), ~, ~]=permutationTest(a_tmp, u_tmp, 5000 );
    [sig_arr_wilc(1,k)] = signrank(a_tmp,u_tmp);
    [~,sig_arr_ttest(1,k)] = ttest(a_tmp,u_tmp);
end
%% Store in Study Structure
STUDY.etc.cluster_pvaf(ClusterInclude).name = ClusterInclude;
STUDY.etc.cluster_pvaf(ClusterInclude).pvaf_U = pvaf_U;
STUDY.etc.cluster_pvaf(ClusterInclude).pvaf_A = pvaf_A;
STUDY.etc.cluster_pvaf(ClusterInclude).sig_arr_perm = sig_arr_perm;
STUDY.etc.cluster_pvaf(ClusterInclude).sig_arr_wilc = sig_arr_wilc;
STUDY.etc.cluster_pvaf(ClusterInclude).sig_arr_ttest = sig_arr_ttest;
%% save Mean Scalp Distribution of Cluster
cnt=0;
winvArr=[];
for k = STUDY.cluster(i).sets
    cnt=cnt+1;
    winvArr=[winvArr ALLEEG(k).icawinv(:,STUDY.cluster(ClusterInclude).comps(cnt))];
end
STUDY.etc.cluster_pvaf(i).meanWinv = mean(winvArr,2);
end