function erpdata_reref = reref_erpdata(erpdata,chaninds)
    erpdata_reref = erpdata;
    if isempty(chaninds)
        chaninds = 1:32;
    end
    % loop conditions
    for i=1:size(erpdata_reref,1)
        % loop subjects
        for j = 1:size(erpdata_reref{i},3)
            % loop timepoints
            for k = 1:size(erpdata_reref{i},1)
                erpdata_reref{i}(k,:,j) = erpdata_reref{i}(k,:,j)-mean(erpdata_reref{i}(k,chaninds,j));
            end
        end
    end
                

end