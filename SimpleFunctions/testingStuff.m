EEG = pop_chanedit(EEG, 'lookup','/Applications/eeglab14_1_2b/plugins/dipfit2.3/standard_BEM/elec/standard_1005.elc','eval','chans = pop_chancenter( chans, [],[]);');


% Cutoffs: HPF: 1Hz LPF: 120 Hz (both)
EEG = pop_eegfiltnew(EEG, 2, [], [], 0, 0, 0);
EEG = pop_eegfiltnew(EEG, [], 120-(29.62/2), [], 0, 0, 0);
EEG = eeg_checkset( EEG );


%% Parameters for PREP that must be present
params.lineFrequencies = [50:50:450];
params.referenceChannels = 1:32;
params.evaluationChannels = 1:32;
params.rereferencedChannels = 1:32;
params.detrendChannels = [1:32];
params.lineNoiseChannels = 1:32;
params.ignoreBoundaryEvents = true;
params.detrendType = 'high pass';
params.detrendCutoff = 1;
params.referenceType = 'robust';
params.meanEstimateType = 'median';
params.interpolationOrder = 'post-reference';
params.removeInterpolatedChannels = true;
params.keepFiltered = false;
%basenameOut = [basename 'robust_1Hz_post_median_unfiltered'];
originalEEG=EEG;

% PREP
[EEG, params, comp] = prepPipeline(EEG, params);



% ASR
EEG = clean_rawdata(EEG, 5, [-1], 0.8, 4, 10, 0.5);

% Step 8: Interpolate all the removed channels
EEG= pop_interp(EEG, originalEEG_A.chanlocs, 'spherical');

