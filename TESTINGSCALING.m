%% normalization
cond_1 = randn(1,32);
cond_2 = cond_1;
cond_2([1 2]) = abs(cond_2([1 2])*2);
cond_1([14]) = abs(cond_1([14])*5);
figure;
subplot(1,3,1)
topoplot(cond_1,EEG.chanlocs)
title('cond 1')
subplot(1,3,2)
topoplot(cond_2,EEG.chanlocs)
title('cond 2')
subplot(1,3,3)
topoplot(cond_1-cond_2,EEG.chanlocs)
title('cond 1 - cond 2')
colorbar
% imagewise
cond_1_iw = cond_1-mean(cond_1);
cond_2_iw = cond_2-mean(cond_2);
figure;
topoplot(cond_1_iw-cond_2_iw,EEG.chanlocs)
title('imagewise scaled')
colorbar
% subjectwise
tmp = cond_1;
tmp(33:64)= cond_2;
sw_scale = mean(tmp);
cond_1_sw = cond_1-sw_scale;
cond_2_sw = cond_2-sw_scale;
figure;
topoplot(cond_1_sw-cond_2_sw,EEG.chanlocs)
title('subjectwise scaled')
colorbar

%% Real Data
EEG_1 = pop_epoch(EEG,{'unamb_reversal'},[0.300 0.600]);
EEG_2 = pop_epoch(EEG,{'amb_reversal'},[0.300 0.600]);
RE = reshape(EEG_1.data,[32,EEG_1.pnts*EEG_1.trials]);
Unamb_Rev_Avg = mean(RE,2);
RE = reshape(EEG_2.data,[32,EEG_2.pnts*EEG_2.trials]);
Amb_Rev_Avg = mean(RE,2);

cond_1 = Unamb_Rev_Avg.^2;
cond_2 = Amb_Rev_Avg.^2;

% raw data
figure;
subplot(1,3,1)
topoplot(cond_1,EEG.chanlocs)
title('cond 1')
caxis([-max(max(abs([cond_1,cond_2]))) max(max(abs([cond_1,cond_2])))])
subplot(1,3,2)
topoplot(cond_2,EEG.chanlocs)
title('cond 2')
caxis([-max(max(abs([cond_1,cond_2]))) max(max(abs([cond_1,cond_2])))])
subplot(1,3,3)
topoplot(cond_1-cond_2,EEG.chanlocs)
title('cond 1 - cond 2')
colorbar
caxis([-max(max(abs([cond_1,cond_2]))) max(max(abs([cond_1,cond_2])))])
% imagewise
cond_1_iw = cond_1./mean(cond_1);
cond_2_iw = cond_2./mean(cond_2);
figure;
subplot(1,3,1)
topoplot(cond_1_iw,EEG.chanlocs)
title('cond 1 iw')
caxis([-max(max(abs([cond_1_iw,cond_2_iw]))) max(max(abs([cond_1_iw,cond_2_iw])))])
subplot(1,3,2)
topoplot(cond_2_iw,EEG.chanlocs)
title('cond 2 iw')
caxis([-max(max(abs([cond_1_iw,cond_2_iw]))) max(max(abs([cond_1_iw,cond_2_iw])))])
subplot(1,3,3)
topoplot(cond_1_iw-cond_2_iw,EEG.chanlocs)
title('cond 1 iw - cond 2 iw')
colorbar
caxis([-max(max(abs([cond_1_iw,cond_2_iw]))) max(max(abs([cond_1_iw,cond_2_iw])))])
% subjectwise
tmp = cond_1;
tmp(33:64)= cond_2;
sw_scale = mean(tmp);
cond_1_sw = cond_1./sw_scale;
cond_2_sw = cond_2./sw_scale;
figure;
subplot(1,3,1)
topoplot(cond_1_sw,EEG.chanlocs)
title('cond 1 sw')
caxis([-max(max(abs([cond_1_sw,cond_2_sw]))) max(max(abs([cond_1_sw,cond_2_sw])))])
subplot(1,3,2)
topoplot(cond_2_sw,EEG.chanlocs)
title('cond 2 sw')
caxis([-max(max(abs([cond_1_sw,cond_2_sw]))) max(max(abs([cond_1_sw,cond_2_sw])))])
subplot(1,3,3)
topoplot(cond_1_sw-cond_2_sw,EEG.chanlocs)
title('cond 1 sw - cond 2 sw')
colorbar
caxis([-max(max(abs([cond_1_sw,cond_2_sw]))) max(max(abs([cond_1_sw,cond_2_sw])))])

% sw_log
tmp = cond_1;
tmp(33:64)= cond_2;
sw_scale = mean(tmp);
cond_1_sw_log = log(cond_1./sw_scale);
cond_2_sw_log = log(cond_2./sw_scale);

figure;
subplot(1,3,1)
topoplot(cond_1_sw_log,EEG.chanlocs)
title('cond 1 sw_log')
caxis([-max(max(abs([cond_1_sw_log,cond_2_sw_log]))) max(max(abs([cond_1_sw_log,cond_2_sw_log])))])
subplot(1,3,2)
topoplot(cond_2_sw_log,EEG.chanlocs)
title('cond 2 sw_log')
caxis([-max(max(abs([cond_1_sw_log,cond_2_sw_log]))) max(max(abs([cond_1_sw_log,cond_2_sw_log])))])
subplot(1,3,3)
topoplot(cond_1_sw_log-cond_2_sw_log,EEG.chanlocs)
title('cond 1 sw_log - cond 2 sw_log')
colorbar
caxis([-max(max(abs([cond_1_sw_log,cond_2_sw_log]))) max(max(abs([cond_1_sw_log,cond_2_sw_log])))])
% log_sw
tmp = cond_1;
tmp(33:64)= cond_2;
sw_scale = mean(tmp);
cond_1_log_sw = log(cond_1)./sw_scale;
cond_2_log_sw = log(cond_2)./sw_scale;

figure;
subplot(1,3,1)
topoplot(cond_1_log_sw,EEG.chanlocs)
title('cond 1 log_sw')
caxis([-max(max(abs([cond_1_log_sw,cond_2_log_sw]))) max(max(abs([cond_1_log_sw,cond_2_log_sw])))])
subplot(1,3,2)
topoplot(cond_2_log_sw,EEG.chanlocs)
title('cond 2 log_sw')
caxis([-max(max(abs([cond_1_log_sw,cond_2_log_sw]))) max(max(abs([cond_1_log_sw,cond_2_log_sw])))])
subplot(1,3,3)
topoplot(cond_1_log_sw-cond_2_log_sw,EEG.chanlocs)
title('cond 1 log_sw - cond 2 log_sw')
colorbar
caxis([-max(max(abs([cond_1_log_sw,cond_2_log_sw]))) max(max(abs([cond_1_log_sw,cond_2_log_sw])))])


figure;
plot(1:32,cond_1-cond_2)
hold on
plot(1:32,cond_1_sw-cond_2_sw)
hold on
plot(1:32,cond_1_iw-cond_2_iw)
legend('Normal', 'Subwise', 'Imgwise')




